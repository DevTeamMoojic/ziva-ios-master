//
//  SignInViewController.m
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "VerifyOTPViewController.h"
#import "SignInViewController.h"

#define OFFSET_Y 100
#define PARAM_MOBILE @"Mobile"

@interface SignInViewController ()<MBProgressHUDDelegate, APIDelegate, VerifyOTPDelegate>
{
    MBProgressHUD *HUD;
    __weak IBOutlet UIScrollView *mainscrollV;
    
    __weak IBOutlet UIView *mobileNoV;
    __weak IBOutlet UITextField *mobileNoTxtF;
    
    __weak IBOutlet UIButton *closeB;

    __weak IBOutlet UIView *loginV;
    __weak IBOutlet UIView *verifyOTPV;

    __weak VerifyOTPViewController *verifyOTPVC;
    
    AppDelegate *appD;
    NSInteger pageWidth;
    
    UIToolbar* numberToolbar;
}
@end

@implementation SignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self setupLayout];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout

- (void) setupLayout
{
    pageWidth = CGRectGetWidth(self.view.frame);
    verifyOTPV.frame = CGRectOffset(verifyOTPV.frame, pageWidth , 0);
    [mainscrollV setContentSize: CGSizeMake(2 * pageWidth, CGRectGetHeight(self.view.frame))];
    [self setupToolBar];
 
    [mobileNoTxtF setValue:[UIColor whiteColor]forKeyPath:@"_placeholderLabel.textColor"] ;
}

- (void) setupToolBar
{
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth( self.view.frame), 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    numberToolbar.barTintColor = [UIColor lightGrayColor];
    //numberToolbar.tintColor = [UIColor officialMaroonColor];
    [numberToolbar sizeToFit];
    
    mobileNoTxtF.inputAccessoryView = numberToolbar;
}

#pragma mark - Methods

- (void) dismissKeyboard
{
    [UIResponder dismissKeyboard];
    [mainscrollV setContentOffset:CGPointZero animated:YES];
}

- (void) showErrorMessage : (NSString *)message
{
    UIAlertView *errorNotice = [[UIAlertView alloc] initWithTitle:ALERT_TITLE_ERROR message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorNotice show];
}

- (void) callAPI
{
    if ([InternetCheck isOnline]) {
        [self hudShowWithMessage:@"Loading"];
        
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject: mobileNoTxtF.text forKey:PARAM_MOBILE];
        [paramDict setObject:OS_TYPE_IPHONE forKey:PARAM_PLATFORM];
        
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_REGISTER_MOBILE Param:paramDict];
    }
    else
        [self showErrorMessage:NETWORKERROR];
}

- (void) processResponse : (NSString *) userId
{
    NSUserDefaults *standardDef = [NSUserDefaults standardUserDefaults];
    [standardDef setValue:userId forKey:USER_REGISTRATION_ID];
    [standardDef setValue: [appD getAppVersionNo] forKey:PARAM_APP_VERSION];
    [standardDef setValue: [appD getAppEnvironment] forKey:PARAM_APP_ENVIRONMENT];
    [standardDef synchronize];
    
    [mainscrollV setContentOffset:CGPointMake(pageWidth, 0) animated:YES];
    closeB.hidden = YES;
}

#pragma mark - Verify Delegate methods

- (void) dismissOTPScreen
{
    [mainscrollV setContentOffset:CGPointZero animated:YES];
    closeB.hidden = NO;
    [verifyOTPVC resetLayout];
}

#pragma mark - HUD & Delegate methods

- (void)hudShowWithMessage:(NSString *)message
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    HUD.dimBackground = YES;
    HUD.labelText = message;
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
    
    [HUD show:YES];
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
    HUD = nil;
}

#pragma mark - ToolBar Events

-(void)cancelNumberPad
{
    [self dismissKeyboard];
}

-(void)doneWithNumberPad
{
    [self signInBPressed:Nil];
}


#pragma mark - Text Responder

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [self signInBPressed:Nil];
    return NO;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == mobileNoTxtF) {
        if(IS_IPHONE4){
            [mainscrollV setContentOffset:CGPointMake(0,  100) animated:YES];
        }
    }
    else{
        [mainscrollV setContentOffset:CGPointZero animated:YES];
    }
    return YES;
}

#pragma mark - Events

- (IBAction)closeBPressed: (UIButton *)sender
{
    [self dismissKeyboard];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:Nil];
}

- (IBAction)signInBPressed: (UIButton *)sender
{
    [self dismissKeyboard];
    if (![appD isEmptyString:mobileNoTxtF.text]) {
        if([appD NSStringIsValidMobile: mobileNoTxtF.text])
        {
            [self callAPI];
        }
        else{
            [self showErrorMessage:@"Please enter correct mobile number"];
        }
    }
    else{
        [self showErrorMessage:@"Please enter mobile number"];
    }
}



#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"embed_VerifyOTP"])
    {
        verifyOTPVC = (VerifyOTPViewController *) segue.destinationViewController;
        verifyOTPVC.delegate = self;
    }
}

#pragma mark - API

-(void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName
{
    [HUD hide:YES];
    if(![ReadData isValidResponseData:response])
    {
        {
            NSString *message = [ReadData stringValueFromDictionary:response forKey:RESPONSEKEY_ERRORMESSAGE];
            [appD showAPIErrorWithMessage:message andTitle:ALERT_TITLE_ERROR];
            [self.view setUserInteractionEnabled:YES];
        }
        return;
    }
    
    if ([apiName isEqualToString:API_REGISTER_MOBILE]) {
        [self processResponse:[ReadData stringValueFromDictionary:response forKey:RESPONSEKEY_DATA]];
    }
}


@end
