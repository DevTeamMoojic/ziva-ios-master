//
//  LookDetailViewController.m
//  Ziva
//
//  Created by Bharat on 24/09/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "BeautyServicesSelectorViewController.h"
#import "GalleryViewController.h"
#import "LookDetailViewController.h"

@interface LookDetailViewController ()<BeautyServicesSelectorDelegate,MBProgressHUDDelegate>
{
    __weak IBOutlet UIView *optionsV;    
    __weak IBOutlet UILabel *screenTitleL;
    
    __weak IBOutlet UIView *containerView;
    __weak IBOutlet UIView *topImageV;
    __weak IBOutlet UIImageView *mainImgV;
    
    __weak IBOutlet UIScrollView *scrollviewOnTop;
    
    __weak IBOutlet UIScrollView *tabcontentscrollView;
    
    __weak IBOutlet UIScrollView *mainscrollView;
    
    __weak IBOutlet UILabel *titleL;
    __weak IBOutlet UILabel *otherInfoL;
    __weak IBOutlet UILabel *startingPriceL;
    

    __weak IBOutlet UIView *descriptionCV;
    __weak IBOutlet UILabel *descriptionL;
    
    __weak IBOutlet UIView *footerV;
    
    __weak IBOutlet UIView *backV;
    
    __weak IBOutlet UIView *beautyservicesCV;
    __weak BeautyServicesSelectorViewController *beautyservicesVC;
    
    NSArray *containerViews;
    
    CGRect cachedImageViewSize;
    CGFloat yscrollOffset;
    
    BOOL isBackFromOtherView;
    BOOL isContentSizeUpdated;
    BOOL isUpdating;
    
    NSUInteger pageIndex;
    NSInteger pageWidth;
    NSInteger pageHeight;
    
    CGFloat contentHeight;
    
    NSDictionary *dataDict;
    
    AppDelegate *appD;
    MyCartController *cartC;
    
    // changes
    __weak IBOutlet UIScrollView *scrollviewMain;
    __weak IBOutlet UIImageView *storeLogoImgV;
    __weak IBOutlet UILabel *ExclusiveORNewL;
    __weak IBOutlet UIView *looksDetailsV;
 MBProgressHUD *HUD;

    
}

@end

@implementation LookDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    cartC = appD.sessionDelegate.mycartC;
    
     [self setupLayout];
//scrollviewMain.contentSize = self.view.frame.size;
//    [scrollviewMain setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)];
    //[scrollviewMain addSubview:self.view];

    //self.sc = self.contentView.frame.size;
    
    
//    float sizeOfContent = 0;
//    UIView *lLast = [scrollviewMain.subviews lastObject];
//    NSInteger wd = lLast.frame.origin.y;
//    NSInteger ht = lLast.frame.size.height;
//    
//    sizeOfContent = wd+ht+300;
//    
//    scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, sizeOfContent);
//
    scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, topImageV.frame.size.height + looksDetailsV.frame.size.height + descriptionCV.frame.size.height +70);
}
//- (void)viewDidLayoutSubviews
//{
//   // [scrollviewMain setContentSize:CGRectMake(0, -50,320, self.view.frame.size.height*1.5)];
//     CGFloat height = CGRectGetHeight(mainImgV.frame) + contentHeight;
//    [scrollviewMain setContentSize:CGSizeMake(320, height)];
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!isBackFromOtherView)
        cachedImageViewSize = mainImgV.frame;
    else
        isBackFromOtherView = NO;
}

#pragma mark - Public Methods

- (void) loadDetailView : (NSDictionary *) dict
{
    dataDict = dict;
}

#pragma mark - Layout

-(void) resetLayout;
{
    mainscrollView.contentOffset = CGPointZero;
    contentHeight = CGRectGetHeight(self.view.bounds);
    
}
- (void) setupLayout
{
    /*
    containerViews = @[beautyservicesCV];
    
    pageWidth = CGRectGetWidth(self.view.bounds);
    
    [self.view addGestureRecognizer:scrollviewOnTop.panGestureRecognizer];
    [mainscrollView removeGestureRecognizer:mainscrollView.panGestureRecognizer];
    
    NSLog(@"----%f%f%f%f",topImageV.frame.origin.x,topImageV.frame.origin.y,topImageV.frame.size.height,topImageV.frame.size.width);
    
    //Position elements
    {
        topImageV.frame = [UpdateFrame setSizeForView:topImageV usingHeight:[ResizeToFitContent getHeightForDetailViewImage:self.view.frame]];
        NSLog(@"----%f%f%f%f",topImageV.frame.origin.x,topImageV.frame.origin.y,topImageV.frame.size.height,topImageV.frame.size.width);

        yscrollOffset = ceil(CGRectGetHeight(topImageV.frame)* 0.5);
        //NSLog(@"----%@",yscrollOffset);

        mainImgV.frame = topImageV.frame ;
        
        containerView.frame = [UpdateFrame setSizeForView:containerView usingHeight:(CGRectGetHeight(self.view.bounds) + yscrollOffset)];
        
        tabcontentscrollView.frame= [UpdateFrame setPositionForView:tabcontentscrollView usingPositionY: CGRectGetMaxY(topImageV.frame)];
        tabcontentscrollView.frame = [UpdateFrame setSizeForView:tabcontentscrollView usingHeight: CGRectGetHeight(containerView.frame) - CGRectGetMaxY(topImageV.frame)];
    }
    
    
    CGPoint scrollviewOrigin = scrollviewOnTop.frame.origin;
    scrollviewOnTop.scrollIndicatorInsets = UIEdgeInsetsMake(-scrollviewOrigin.y, 0, scrollviewOrigin.y, scrollviewOrigin.x);
    
   */
    
    [self setData];
}

#pragma mark - Methods

- (NSMutableAttributedString *) getStartingPrice : (NSString *) price withExclusiveSalon :(NSString *) salonName
{
    NSMutableAttributedString *aAttrString ;
    NSDictionary *bFontDict = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[UIFont boldFontOfSize:14],[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1],Nil] forKeys:[NSArray arrayWithObjects:NSFontAttributeName,NSForegroundColorAttributeName,Nil]];
    NSDictionary *gFontDict = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[UIFont boldFontOfSize:14],[UIColor colorWithRed:86.0/255.0 green:190.0/255.0 blue:159.0/255.0 alpha:1],Nil] forKeys:[NSArray arrayWithObjects:NSFontAttributeName,NSForegroundColorAttributeName,Nil]];
    
    if([salonName length] > 0){
        
        aAttrString = [[NSMutableAttributedString alloc] initWithString:@"" attributes: bFontDict];
        //[aAttrString appendAttributedString:  [[NSMutableAttributedString alloc] initWithString:salonName   attributes: gFontDict]];
        [aAttrString appendAttributedString:  [[NSMutableAttributedString alloc] initWithString:@""   attributes: gFontDict]];

        [aAttrString appendAttributedString:  [[NSMutableAttributedString alloc] initWithString:@"Starting price from @ "   attributes: bFontDict]];
        [aAttrString appendAttributedString:  [[NSMutableAttributedString alloc] initWithString:price   attributes: bFontDict]];
        
    }
    else{
        aAttrString = [[NSMutableAttributedString alloc] initWithString:@"Starting @ " attributes: bFontDict];
        [aAttrString appendAttributedString:  [[NSMutableAttributedString alloc] initWithString:price   attributes: bFontDict]];
        
    }
    return  aAttrString;
}

- (void) setData
{
    contentHeight = 0;
    if(dataDict)
    {
        NSString *recordId = [ReadData recordIdFromDictionary:dataDict forKey:KEY_ID];
        [appD.sessionDelegate.mytrackingC trackLookById:recordId];
        
        titleL.text = [ReadData stringValueFromDictionary:dataDict forKey:KEY_NAME];
        otherInfoL.text = [ReadData stringValueFromDictionary:dataDict forKey:KEY_NAME];

       // otherInfoL.text = [ReadData stringValueFromDictionary:dataDict forKey:KEY_LOOK_SUBTITLE];
        
        NSString *isNewStr = [ReadData stringValueFromDictionary:dataDict forKey:KEY_LOOK_ISNEW];
        
    
        NSString *salonName = @"";
        if(![ReadData IsnulldictionaryFromDictionary:dataDict forKey:KEY_LOOK_SALON]){
            NSDictionary *storesDict = [ReadData dictionaryFromDictionary:dataDict forKey:KEY_LOOK_SALON];
            salonName = [ReadData stringValueFromDictionary:storesDict forKey:KEY_NAME];
            
            
            // set the store vlaue eithor exclusive or new.
            ExclusiveORNewL.text = @"EXCLUSIVE";
            
            // Set store logo imges.
            // ImageView
            NSString *url = [ReadData stringValueFromDictionary:storesDict forKey:KEY_LOGO_URL];
            if (![url isEqualToString:@""])
            {
                NSURL *imageURL = [NSURL URLWithString:url];
                NSURLRequest *urlRequest = [NSURLRequest requestWithURL:imageURL];
                [ storeLogoImgV setImageWithURLRequest:urlRequest
                                 placeholderImage:Nil
                                          success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
                 {
                     //mainImgV.contentMode = UIViewContentModeScaleAspectFill;
                     [ storeLogoImgV setImage:image];
                     [UIView animateWithDuration:IMAGE_VIEW_TOGGLE
                                           delay:0 options:UIViewAnimationOptionCurveLinear
                                      animations:^(void)
                      {
                          
                          [ storeLogoImgV setAlpha:1 ];
                      }
                                      completion:^(BOOL finished)
                      {
                      }];
                     
                     
                     
                 }
                                          failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
                 {
                     
                 }];
            }
             
        }
        else // if the store is new.
        {
            // set the store vlaue  new.
            if ([isNewStr isEqualToString:@"1"]) {
                ExclusiveORNewL.text = @"NEW";
            }
            
        }
        
        // new code.
        
        ////
        startingPriceL.text = [NSString stringWithFormat: @"Starting price from %@",[ReadData amountInRsFromDictionary:dataDict forKey:KEY_LOOK_SILVERPRICE]];
        
//        
//         CGFloat tmpWidth = [ResizeToFitContent getWidthForText:startingPriceL.text usingFontType:FONT_SEMIBOLD FontOfSize:11 andMaxWidth:100];
//        startingPriceL.frame = [UpdateFrame setSizeForView:startingPriceL usingWidth:(tmpWidth+10)];

        
        
               ////

         //startingPriceL.attributedText = [self getStartingPrice:[ReadData amountInRsFromDictionary:dataDict forKey:KEY_LOOK_SILVERPRICE] withExclusiveSalon:salonName];
        
        descriptionL.text = [[ReadData arrayFromDictionary:dataDict forKey:KEY_LOOK_NOTES] componentsJoinedByString:@"\n\n"];
       // @"asdfasdf asd fa sdf as df as dfasdfasd fasdfasdfasdfasdf asdfa sdf a sdf asdfasdf qwerqwer sdfewrasdfqwerqwer qwer qwer qwerq werqwerq werq wer qwe rq wer qw erqw er qw er qwer q werqwerqw er qwer qwerqwerq wer qwer qwe rq werqwer q wer qwer q werqwerq werqwe r qwer q wer qwer q wer q werqwe rq wer qwer q werqwerqwer qwer qwer qwer qwe r qwer qwer q wer qwer q wer qwer q wer qwe rqwer q wer qwer  qwer  qerw qwer q wer qwer qwe r q rq werqwer qw erq wer q wer qwe r qwe rq wer qwer qwer q wer qwe r qwer qwer qwerq wer qw er qwer qwer  qwerqwe r qwer qwerqwer qwer q wer qw er qwer qwerqwer qwr qwerqwerqwer  qwer";
        //[[ReadData arrayFromDictionary:dataDict forKey:KEY_LOOK_NOTES] componentsJoinedByString:@"\n\n"];
        CGFloat tmpHeight = [ResizeToFitContent getHeightForText:descriptionL.text usingFontType:FONT_SEMIBOLD FontOfSize:12 andMaxWidth:CGRectGetWidth(descriptionL.frame)];
        descriptionL.frame = [UpdateFrame setSizeForView:descriptionL usingHeight:tmpHeight];
        //descriptionL.backgroundColor = [UIColor yellowColor];
        
        descriptionCV.frame =  [UpdateFrame setSizeForView:descriptionCV usingHeight:tmpHeight + 50];
        //descriptionCV.backgroundColor = [UIColor lightGrayColor];
        
        // new frame setting for button.
        footerV.frame = CGRectMake(footerV.frame.origin.x, tmpHeight+550, footerV.frame.size.width, footerV.frame.size.height);
        
        // ImageView
        NSString *url = [ReadData stringValueFromDictionary:dataDict forKey:KEY_IMAGE1_URL];
        if (![url isEqualToString:@""])
        {
            [self hudShowWithMessage:@"Loading"];
            NSURL *imageURL = [NSURL URLWithString:url];
            NSURLRequest *urlRequest = [NSURLRequest requestWithURL:imageURL];
            [ mainImgV setImageWithURLRequest:urlRequest
                             placeholderImage:Nil
                                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
             {
                 mainImgV.contentMode = UIViewContentModeScaleAspectFill;

                 [ mainImgV setImage:image];
                 [UIView animateWithDuration:IMAGE_VIEW_TOGGLE
                                       delay:0 options:UIViewAnimationOptionCurveLinear
                                  animations:^(void)
                  {
                      [HUD hide:YES];
                      [ mainImgV setAlpha:1 ];
                  }
                                  completion:^(BOOL finished)
                  {
                  }];
                 
                 
                 
             }
                                      failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
             {
                 
             }];
        }
        
        
        contentHeight = CGRectGetMaxY(descriptionCV.frame) + CGRectGetHeight(footerV.frame);
    }
    
    if(contentHeight <= CGRectGetHeight(mainscrollView.frame)){
        contentHeight = CGRectGetHeight(mainscrollView.bounds);
        mainscrollView.scrollEnabled = NO;
    }
    
    [self tabChanged];
}

- (void) tabChanged
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        CGPoint offset = [self getScrollContentOffset];
        offset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:offset animated:NO];
    }
    isContentSizeUpdated = NO;
}

- (void) startBookingWithDeliveryType : (NSString *) type{
    [cartC initForLooksWithDeliveryType:type];
    [cartC setLatitude:[appD getUserCurrentLocationLatitude] andLongitude:[appD getUserCurrentLocationLongitude]];
    [cartC addLooks:dataDict];
    [self performSegueWithIdentifier:@"showlocationdate_looks" sender:Nil];
}

#pragma mark - Showing/Hiding Views - Generic

- (void)showView:(UIView *)view andGreyView:(BOOL)showGreyView
{
    // Shrink WebView
    view.transform = CGAffineTransformMakeScale(0.8, 0.8);
    
    // Ready Grey View
    if (showGreyView)
    {
        backV.alpha = 0;
        [self.view bringSubviewToFront:backV];
    }
    
    // Ready WebView
    view.alpha = 0;
    [self.view bringSubviewToFront:view];
    
    // DONT CHANGE THE ORDER
    
    // 1 - Dismiss keyboard
    [UIResponder dismissKeyboard];
    
    // 2 - Disable User Interaction
    [self.view setUserInteractionEnabled:NO];
    
    // 3 - Animate
    [UIView animateWithDuration:TIME_VIEW_TOGGLE
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         // Animate Grow and Fade In
         view.transform = CGAffineTransformMakeScale(1.0, 1.0);
         view.alpha = 1;
         
         if (showGreyView)
             backV.alpha = 1;
     }
                     completion:^(BOOL finished)
     {
         // Enable User Interaction
         [self.view setUserInteractionEnabled:YES];
     }];
}

- (void)hideView:(UIView *)view andGreyView:(BOOL)hideGreyView
{
    // DONT CHANGE THE ORDER
    
    // 1 - Dismiss keyboard
    [UIResponder dismissKeyboard];
    
    
    // 2 - Disable User Interaction
    [self.view setUserInteractionEnabled:NO];
    
    // 3 - Animate
    [UIView animateWithDuration:TIME_VIEW_TOGGLE
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         // Animate Shrink & Fade Out
         view.transform = CGAffineTransformMakeScale(0.8, 0.8);
         view.alpha = 0;
         
         if(hideGreyView)
             backV.alpha = 0;
     }
                     completion:^(BOOL finished)
     {
         
         // Send Views to Back
         [self.view sendSubviewToBack:view];
         
         if(hideGreyView)
             [self.view sendSubviewToBack:backV];
         
         // Enable User Interaction
         [self.view setUserInteractionEnabled:YES];
     }];
}

- (IBAction)backVClicked:(id)sender
{
    [self hideAllViews];
}

- (void)hideAllViews
{
    // DONT CHANGE THE ORDER
    
    // 1 - Dismiss keyboard
    [UIResponder dismissKeyboard];
    
    
    // 2 - Disable User Interaction
    [self.view setUserInteractionEnabled:NO];
    
    
    // 3 - Animate
    [UIView animateWithDuration:TIME_VIEW_TOGGLE
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         for (UIView *view in containerViews)
         {
             view.transform = CGAffineTransformMakeScale(0.8, 0.8);
             view.alpha = 0;
         }
         
         backV.alpha = 0;
         
     }
                     completion:^(BOOL finished)
     {
         
         for (UIView *view in containerViews)
             [self.view sendSubviewToBack:view];
         
         [self.view sendSubviewToBack:backV];
         
         // Enable User Interaction
         [self.view setUserInteractionEnabled:YES];
         
     }];
}

#pragma mark - Delegates

- (void) homeServicesSelected
{
    [self hideView:beautyservicesCV andGreyView:YES];
    [self startBookingWithDeliveryType:DELIVERY_TYPE_HOME];
}

- (void) salonServicesSelected
{
    [self hideView:beautyservicesCV andGreyView:YES];
    [self startBookingWithDeliveryType:DELIVERY_TYPE_SALON];
}

#pragma mark - Events

- (IBAction)backBPressed:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)galleryBPressed:(UIButton *)sender{
    [self performSegueWithIdentifier:@"showgallery_look" sender:Nil];
}

- (IBAction)bookBPressed:(UIButton *)sender{
    //[self showView:beautyservicesCV andGreyView:YES];
    [self startBookingWithDeliveryType:DELIVERY_TYPE_SALON];
}


#pragma mark - Expand on Scroll animation

- (CGSize) setContentSize
{
    CGFloat height = CGRectGetHeight(mainImgV.frame) + contentHeight;
    return CGSizeMake(pageWidth, height);
}

- (void) setScrollContentOffset: (CGPoint) offset
{
    NSLog(@"----%f%f", offset.x,offset.y);
    mainscrollView.contentOffset = offset;
}

- (CGPoint) getScrollContentOffset
{
    return [mainscrollView contentOffset];
}

- (void) resetOffsetAllTabs
{
    CGPoint offset = CGPointZero;
    [self setScrollContentOffset : offset];
}

-(void) updateContentSize : (CGPoint) contentOffset;
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        contentOffset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    }
    [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    
    isContentSizeUpdated = NO;
}

#pragma mark - Scroll Events

- (void)scrollViewDidScroll:(UIScrollView *)scrollView

{
    NSLog(@"scrollviewOnTop ---- %@", scrollviewOnTop);
    
    if (scrollView == scrollviewOnTop)
    {
        {
            if (isContentSizeUpdated)  return;
            
           
            CGRect scrolledBoundsForContainerView = containerView.bounds;
            if (scrollView.contentOffset.y <= yscrollOffset)
            {
                
                scrolledBoundsForContainerView.origin.y = scrollView.contentOffset.y ;
                containerView.bounds = scrolledBoundsForContainerView;
                
                //Reset offset for all tabs
                if (scrollView.contentOffset.y <= 0) [self resetOffsetAllTabs];
                
                
                CGFloat y = -scrollView.contentOffset.y;
//                CGFloat alphaLevel = 1;
//                CGFloat BLUR_MAX_Y = IMAGE_SCROLL_OFFSET_Y + CGRectGetHeight(topImageV.frame) - CGRectGetMinY(optionsV.frame);
//                if (fabs(y) < BLUR_MAX_Y)
//                {
//                    alphaLevel = (BLUR_MAX_Y - fabs(y))/(BLUR_MAX_Y);
//                }
//                else
//                {
//                    alphaLevel = 0;
//                }
//                
               //[screenTitleL setAlpha:alphaLevel];
                if (y > 0)
                {
                    mainImgV.frame = CGRectInset(cachedImageViewSize, 0, -y/2);
                    mainImgV.center = CGPointMake(mainImgV.center.x, mainImgV.center.y + y/2);
                    NSLog(@"----%f%f",mainImgV.center.x,mainImgV.center.y);
                }
                else
                {
                    mainImgV.frame = [UpdateFrame setPositionForView:mainImgV usingPositionY:y];
                }
                return;
            }
            
            scrolledBoundsForContainerView.origin.y = yscrollOffset ;
            containerView.bounds = scrolledBoundsForContainerView;
            
            [self setScrollContentOffset:CGPointMake(0, scrollView.contentOffset.y - yscrollOffset)  ];
        }
    }
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"showgallery_look"]){
        GalleryViewController *galleryVC = (GalleryViewController *)segue.destinationViewController;
        [galleryVC setImageForFullScreen:mainImgV.image];
    }
    else if([segue.identifier isEqualToString:@"showlocationdate_looks"]){
    }
    else if([segue.identifier isEqualToString:@"showservicesselector_look"]){
        beautyservicesVC = (BeautyServicesSelectorViewController *)segue.destinationViewController;
        beautyservicesVC.delegate = self;
    }
}

#pragma mark - HUD & Delegate methods

- (void)hudShowWithMessage:(NSString *)message
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    HUD.dimBackground = YES;
    HUD.labelText = message;
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
    
    [HUD show:YES];
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
    HUD = nil;
}


@end
