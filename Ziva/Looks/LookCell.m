//
//  LookCell.m
//  Ziva
//
//  Created by Bharat on 23/09/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "LookCell.h"

@implementation LookCell

- (void)awakeFromNib {
    // Initialization code
    
    [super awakeFromNib];
    
    // Fix : Autoresizing issues for iOS 7 using Xcode 6
    self.contentView.frame = self.bounds;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //    Fix : For right aligned view with autoresizing
    UIView *backV = [[self.contentView subviews] objectAtIndex:0];
    backV.frame = [UpdateFrame setSizeForView:backV usingSize:self.bounds.size];
}

- (NSMutableAttributedString *) getStartingPrice : (NSString *) price withExclusiveSalon :(NSString *) salonName
{
    NSMutableAttributedString *aAttrString ;
    NSDictionary *bFontDict = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[UIFont semiboldFontOfSize:14],[UIColor colorWithRed:154.0/255.0 green:154.0/255.0 blue:154.0/255.0 alpha:1],Nil] forKeys:[NSArray arrayWithObjects:NSFontAttributeName,NSForegroundColorAttributeName,Nil]];
    NSDictionary *gFontDict = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[UIFont semiboldFontOfSize:14],[UIColor colorWithRed:86.0/255.0 green:190.0/255.0 blue:159.0/255.0 alpha:1],Nil] forKeys:[NSArray arrayWithObjects:NSFontAttributeName,NSForegroundColorAttributeName,Nil]];
    
    if([salonName length] > 0){
    
        aAttrString = [[NSMutableAttributedString alloc] initWithString:@"Complete look at " attributes: bFontDict];
        [aAttrString appendAttributedString:  [[NSMutableAttributedString alloc] initWithString:salonName   attributes: gFontDict]];
        [aAttrString appendAttributedString:  [[NSMutableAttributedString alloc] initWithString:@". Starting @ "   attributes: bFontDict]];
        [aAttrString appendAttributedString:  [[NSMutableAttributedString alloc] initWithString:price   attributes: bFontDict]];
        
    }
    else{
        aAttrString = [[NSMutableAttributedString alloc] initWithString:@"Starting @ " attributes: bFontDict];
        [aAttrString appendAttributedString:  [[NSMutableAttributedString alloc] initWithString:price   attributes: bFontDict]];
        
    }
    return  aAttrString;
}

- (void) configureDataForCell : (NSDictionary *) item
{
    self.mainImgV.image = Nil;
    self.placeholderImgV.alpha=1;
    
    self.titleL.text = [ReadData stringValueFromDictionary:item forKey:KEY_NAME];
    self.otherInfoL.text = [ReadData stringValueFromDictionary:item forKey:KEY_LOOK_SUBTITLE];
    
    NSString *salonName = @"";    
    if(![ReadData IsnulldictionaryFromDictionary:item forKey:KEY_LOOK_SALON]){
        NSDictionary *storesDict = [ReadData dictionaryFromDictionary:item forKey:KEY_LOOK_SALON];
        salonName = [ReadData stringValueFromDictionary:storesDict forKey:KEY_NAME];
    }
    
    self.startingPriceL.attributedText = [self getStartingPrice:[ReadData amountInRsFromDictionary:item forKey:KEY_LOOK_SILVERPRICE] withExclusiveSalon:salonName];
    
    // ImageView
    NSString *url = [ReadData stringValueFromDictionary:item forKey:KEY_BANNER_URL];
    if (![url isEqualToString:@""])
    {
        NSURL *imageURL = [NSURL URLWithString:url];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:imageURL];
        [self.mainImgV setImageWithURLRequest:urlRequest
                             placeholderImage:Nil
                                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {             
             [self.mainImgV setImage:image];
             
             [UIView animateWithDuration:IMAGE_VIEW_TOGGLE
                                   delay:0 options:UIViewAnimationOptionCurveLinear
                              animations:^(void)
              {
                  
                  [self.placeholderImgV setAlpha:0 ];
                  [self.mainImgV setAlpha:1 ];
              }
                              completion:^(BOOL finished)
              {
              }];
             
         }
                                      failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
         {
             
         }];
    }
}

@end
