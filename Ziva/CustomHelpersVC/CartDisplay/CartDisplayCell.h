//
//  CartDisplayCell.h
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CartDisplayCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *mainV;

@property (weak, nonatomic) IBOutlet UIView *serviceHeadingV;
@property (weak, nonatomic) IBOutlet UILabel *servicetitleL;
@property (weak, nonatomic) IBOutlet UILabel *servicepriceL;

@property (weak, nonatomic) IBOutlet UIView *productHeadingV;
@property (weak, nonatomic) IBOutlet UILabel *producttitleL;
@property (weak, nonatomic) IBOutlet UILabel *productquantityL;
@property (weak, nonatomic) IBOutlet UILabel *productpriceL;

@property (weak, nonatomic) IBOutlet UIButton *removeB;

- (void) configureDataForCell : (NSDictionary *) item ;

@end
