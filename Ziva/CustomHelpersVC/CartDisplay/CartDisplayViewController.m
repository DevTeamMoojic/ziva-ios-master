//
//  CartDisplayViewController.m
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "CartDisplayCell.h"
#import "CartDisplayViewController.h"

@interface CartDisplayViewController ()
{
    __weak IBOutlet UIView *optionsV;    
    __weak IBOutlet UILabel *screenTitleL;
    
    __weak IBOutlet UIView *containerView;
    __weak IBOutlet UIView *topImageV;
    __weak IBOutlet UIImageView *mainImgV;
    
    __weak IBOutlet UIScrollView *scrollviewOnTop;
    
    __weak IBOutlet UIView *cartTotalV;
    __weak IBOutlet UILabel *totalAmountL;
    __weak IBOutlet UIScrollView *tabcontentscrollView;
    
    __weak IBOutlet UIScrollView *mainscrollView;
    __weak IBOutlet UITableView *listTblV;

    __weak IBOutlet UIView *footerV;
    __weak IBOutlet UIButton *bookB;
    
    CGRect cachedImageViewSize;
    CGFloat yscrollOffset;
    
    BOOL isBackFromOtherView;
    BOOL isContentSizeUpdated;
    BOOL isUpdating;
    
    NSUInteger pageIndex;
    NSInteger pageWidth;
    NSInteger pageHeight;
    
    AppDelegate *appD;
    MyCartController *cartC;
    
    CGFloat contentHeight;
}
@end

@implementation CartDisplayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    cartC = appD.sessionDelegate.mycartC;
    
    [self setupLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!isBackFromOtherView)
        cachedImageViewSize = mainImgV.frame;
    else
        isBackFromOtherView = NO;
}


#pragma mark - Layout

-(void) resetLayout;
{
    mainscrollView.contentOffset = CGPointZero;
    contentHeight = CGRectGetHeight(self.view.bounds);
    
}
- (void) setupLayout
{
    pageWidth = CGRectGetWidth(self.view.bounds);
    
    if([cartC.cartType isEqualToString:CART_TYPE_SERVICES] || [cartC.cartType isEqualToString:CART_TYPE_LOOKS]){
        [bookB setTitle:@"BOOK" forState:UIControlStateNormal];
    }
    else{
        [bookB setTitle:@"PURCHASE" forState:UIControlStateNormal];        
    }
    
    [self.view addGestureRecognizer:scrollviewOnTop.panGestureRecognizer];
    [mainscrollView removeGestureRecognizer:mainscrollView.panGestureRecognizer];
    
    //Position elements
    {
        topImageV.frame = [UpdateFrame setSizeForView:topImageV usingHeight:[ResizeToFitContent getHeightForParallaxImage:self.view.frame]];
        
        yscrollOffset = CGRectGetHeight(topImageV.frame) - IMAGE_SCROLL_OFFSET_Y - CGRectGetHeight(optionsV.frame);
        mainImgV.frame = topImageV.frame ;
        
        containerView.frame = [UpdateFrame setSizeForView:containerView usingHeight:(CGRectGetHeight(self.view.bounds) + yscrollOffset)];
     
        cartTotalV.frame = [UpdateFrame setPositionForView:cartTotalV usingPositionY: CGRectGetMaxY(topImageV.frame)];
        
        tabcontentscrollView.frame= [UpdateFrame setPositionForView:tabcontentscrollView usingPositionY: CGRectGetMaxY(cartTotalV.frame)];
        tabcontentscrollView.frame = [UpdateFrame setSizeForView:tabcontentscrollView usingHeight: CGRectGetHeight(containerView.frame) - CGRectGetMaxY(cartTotalV.frame)];
    }
    
    
    CGPoint scrollviewOrigin = scrollviewOnTop.frame.origin;
    scrollviewOnTop.scrollIndicatorInsets = UIEdgeInsetsMake(-scrollviewOrigin.y, 0, scrollviewOrigin.y, scrollviewOrigin.x);
    
    [self loadData];
}

#pragma mark - Methods

-(CGFloat) calculateHeightOfList
{
    CGFloat height = 0.0f;
    for (NSInteger ctr =0; ctr < [[cartC cartItems] count]; ctr++) {
        height+= [self calculateheightForRow:ctr];
    }
    return height;
}


- (void) loadData
{
    totalAmountL.text = [cartC display_TotalValue];
    [listTblV reloadData];
    
    listTblV.frame = [UpdateFrame setSizeForView:listTblV usingHeight:[self calculateHeightOfList]];
    
    contentHeight = CGRectGetMaxY(listTblV.frame) + CGRectGetHeight(footerV.frame);
    
    if (contentHeight <= CGRectGetHeight(mainscrollView.bounds))
    {
        contentHeight = CGRectGetHeight(mainscrollView.bounds);
        mainscrollView.scrollEnabled = NO;
    }
    
    footerV.hidden = ([cartC.cartItems count] == 0);
    
    [self tabChanged];
}

- (void) tabChanged
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        CGPoint offset = [self getScrollContentOffset];
        offset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:offset animated:NO];
    }
    isContentSizeUpdated = NO;
}

#pragma mark - Table view data source

-(CGFloat) calculateheightForRow:(NSInteger)row{
    CGFloat height = 56.0f;
    //NSDictionary *item = list[indexPath.row];
    return height;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[cartC cartItems] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self calculateheightForRow:indexPath.row];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CartDisplayCell *cell = (CartDisplayCell *)[tableView dequeueReusableCellWithIdentifier:@"cartdisplaycell" forIndexPath:indexPath];
    NSDictionary *item = [cartC cartItems][indexPath.row];
    [cell configureDataForCell :item];
    
    cell.productHeadingV.hidden = ![cartC.cartType isEqualToString:CART_TYPE_PRODUCTS];
    cell.serviceHeadingV.hidden = !cell.productHeadingV.hidden;
    
    [cell.removeB addTarget:self action:@selector(removeFromCartBPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

#pragma mark - Events

-(void)removeFromCartBPressed:(id)sender{
    UIButton *btn = (UIButton *)sender;
    [cartC removeFromCart:[NSString stringWithFormat:@"%ld",(long)btn.tag]];
    [self loadData];
    if([cartC.cartItems count] == 0){
        UIViewController *vc = [self.navigationController.viewControllers objectAtIndex: (self.navigationController.viewControllers.count - 2) - 1] ;
        [self.navigationController popToViewController:vc animated:YES];
    }
}

- (IBAction)backBPressed:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)bookBPressed:(UIButton *)sender{
    isBackFromOtherView = YES;
    if(![appD isProfileCompleted]){
        [self performSegueWithIdentifier:@"showupdateprofile" sender:Nil];
    }
    else{
        [self performSegueWithIdentifier:@"showpaymentsummary" sender:Nil];
    }
    
    
}

#pragma mark - Expand on Scroll animation

- (CGSize) setContentSize
{
    CGFloat height = CGRectGetHeight(mainImgV.frame) + CGRectGetHeight(cartTotalV.frame) +  contentHeight;
    return CGSizeMake(pageWidth, height);
}

- (void) setScrollContentOffset: (CGPoint) offset
{
    mainscrollView.contentOffset = offset;
}

- (CGPoint) getScrollContentOffset
{
    return [mainscrollView contentOffset];
}

- (void) resetOffsetAllTabs
{
    CGPoint offset = CGPointZero;
    [self setScrollContentOffset : offset];
}

-(void) updateContentSize : (CGPoint) contentOffset;
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        contentOffset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    }
    [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    
    isContentSizeUpdated = NO;
}

#pragma mark - Scroll Events

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == scrollviewOnTop)
    {
        {
            if (isContentSizeUpdated)  return;
            
            CGRect scrolledBoundsForContainerView = containerView.bounds;
            if (scrollView.contentOffset.y <= yscrollOffset)
            {
                
                scrolledBoundsForContainerView.origin.y = scrollView.contentOffset.y ;
                containerView.bounds = scrolledBoundsForContainerView;
                
                //Reset offset for all tabs
                if (scrollView.contentOffset.y <= 0) [self resetOffsetAllTabs];
                
                
                CGFloat y = -scrollView.contentOffset.y;
                CGFloat alphaLevel = 1;
                CGFloat BLUR_MAX_Y = IMAGE_SCROLL_OFFSET_Y + CGRectGetHeight(topImageV.frame) - CGRectGetMinY(optionsV.frame);
                if (fabs(y) < BLUR_MAX_Y)
                {
                    alphaLevel = (BLUR_MAX_Y - fabs(y))/(BLUR_MAX_Y);
                }
                else
                {
                    alphaLevel = 0;
                }
                
                //[screenTitleL setAlpha:alphaLevel];
                if (y > 0)
                {
                    mainImgV.frame = CGRectInset(cachedImageViewSize, 0, -y/2);
                    mainImgV.center = CGPointMake(mainImgV.center.x, mainImgV.center.y + y/2);
                }
                else
                {
                    mainImgV.frame = [UpdateFrame setPositionForView:mainImgV usingPositionY:y];
                }
                return;
            }
            
            scrolledBoundsForContainerView.origin.y = yscrollOffset ;
            containerView.bounds = scrolledBoundsForContainerView;
            
            [self setScrollContentOffset:CGPointMake(0, scrollView.contentOffset.y - yscrollOffset)  ];
        }
    }
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
}

@end
