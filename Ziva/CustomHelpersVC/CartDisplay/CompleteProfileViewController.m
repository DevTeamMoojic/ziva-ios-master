//
//  CompleteProfileViewController.m
//  Ziva
//
//  Created by Bharat on 14/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "CompleteProfileViewController.h"

@interface CompleteProfileViewController ()<APIDelegate,MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
    
    __weak IBOutlet UIScrollView *mainscrollV;
    
    __weak IBOutlet UIView *firstNameV;
    __weak IBOutlet UITextField *firstNameTxtF;
    
    __weak IBOutlet UIView *lastNameV;
    __weak IBOutlet UITextField *lastNameTxtF;
    
    __weak IBOutlet UIView *emailV;
    __weak IBOutlet UITextField *emailTxtF;
    
    __weak IBOutlet UIView *mobileV;
    __weak IBOutlet UITextField *mobileTxtF;
    
    AppDelegate *appD;
    
}
@end

@implementation CompleteProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self setupLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Methods

- (void) setupLayout
{
    firstNameTxtF.text = [appD getLoggedInUserFirstName];
    lastNameTxtF.text = [appD getLoggedInUserLastName];
    emailTxtF.text = [appD getLoggedInUserEmailAddress];
    mobileTxtF.text = [appD getLoggedInUserMobileNumber];
}

- (void) dismissKeyboard
{
    [UIResponder dismissKeyboard];
    [mainscrollV setContentOffset:CGPointZero animated:YES];
}

- (void) showErrorMessage : (int)errorCode
{
    NSString *message = @"";
    switch (errorCode) {
        case NETWORKERRORCODE: {
            message = NETWORKERROR;
        }
            break;
        case FIRSTNAMEEMPTY: {
            message = @"First Name cannot be blank.";
        }
            break;
        case LASTNAMEEMPTY: {
            message = @"Last Name cannot be blank.";
        }
            break;
        case EMAILEMPTY: {
            message = @"Email cannot be blank.";
        }
            break;
        case EMAILWRONG: {
            message = @"Enter valid email address.";
        }
            break;
        case DOBEMPTY: {
            message = @"Date of birth cannot be blank.";
        }
            break;
        case GENDEREMPTY: {
            message = @"Gender cannot be blank.";
        }
            break;
    }
    
    UIAlertView *errorNotice = [[UIAlertView alloc] initWithTitle:ALERT_TITLE_ERROR message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorNotice show];
}

- (BOOL) isValid
{
    [UIResponder dismissKeyboard];
    [mainscrollV setContentOffset:CGPointZero animated:YES];
    
    if ([appD isEmptyString:firstNameTxtF.text])
    {
        [self showErrorMessage:FIRSTNAMEEMPTY];
        return  NO;
    }
    if ([appD isEmptyString:lastNameTxtF.text])
    {
        [self showErrorMessage:LASTNAMEEMPTY];
        return  NO;
    }
    if ([appD isEmptyString:emailTxtF.text])
    {
        [self showErrorMessage:EMAILEMPTY];
        return  NO;
    }
    if(![appD NSStringIsValidEmail:emailTxtF.text])
    {
        [self showErrorMessage:EMAILWRONG];
        return  NO;
    }
    return YES;
}

- (void) updateProfile
{
    if ([InternetCheck isOnline]){
        
        [self hudShowWithMessage:@"Loading"];
        
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject:[appD getLoggedInUserId] forKey:KEY_ID];
        [paramDict setObject:firstNameTxtF.text forKey:KEY_USER_FIRSTNAME];
        [paramDict setObject:lastNameTxtF.text forKey:KEY_USER_LASTNAME];
        [paramDict setObject:emailTxtF.text forKey:KEY_USER_EMAIL];
        
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_UPDATE_PROFILE Param:paramDict];
    }
}


- (void) processResponse : (NSDictionary *) resultsDict
{
    NSUserDefaults *standardDef = [NSUserDefaults standardUserDefaults];
    [standardDef setValue: [ReadData stringValueFromDictionary:resultsDict forKey:KEY_USER_EMAIL] forKey:USER_EMAIL];
    [standardDef setValue: [ReadData stringValueFromDictionary:resultsDict forKey:KEY_USER_LASTNAME] forKey:USER_LASTNAME];
    [standardDef setValue: [ReadData stringValueFromDictionary:resultsDict forKey:KEY_USER_FIRSTNAME] forKey:USER_FIRSTNAME];
    [standardDef setValue: [ReadData genderIdFromDictionary:resultsDict forKey:KEY_USER_GENDER] forKey:USER_GENDER];
    [standardDef setValue: [ReadData stringValueFromDictionary:resultsDict forKey:KEY_USER_IMAGE_URL] forKey:USER_PROFILE_URL];
    [standardDef setValue: [ReadData stringValueFromDictionary:resultsDict forKey:KEY_USER_DATEOFBIRTH] forKey:USER_BIRTHDATE];
    
    [standardDef synchronize];
    
    [appD profileUpdated];
    
    [self.presentingViewController dismissViewControllerAnimated:YES completion:Nil];
}

#pragma mark - HUD & Delegate methods

- (void)hudShowWithMessage:(NSString *)message
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    HUD.dimBackground = YES;
    HUD.labelText = message;
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
    
    [HUD show:YES];
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
    HUD = nil;
}

#pragma mark - Events

- (IBAction)closeBPressed:(UIButton *)sender{
    [self dismissKeyboard];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:Nil];
}

- (IBAction)updateBPressed:(UIButton *)sender{
    [self dismissKeyboard];
    if([self isValid]){
        [self updateProfile];
    }
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if (textField == emailTxtF) {
        [self updateBPressed:Nil];
    }
    return NO;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    BOOL isEditable = YES;
    if (textField == emailTxtF) {
        if(IS_IPHONE4){
            [mainscrollV setContentOffset:CGPointMake(0,  100) animated:YES];
        }
    }
    else if(textField == mobileTxtF){
        isEditable = NO;
    }
    else{
        [mainscrollV setContentOffset:CGPointZero animated:YES];
    }
    return isEditable;
}

#pragma mark - API

-(void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName
{
    [HUD hide:YES];
    if(![ReadData isValidResponseData:response])
    {
        {
            NSString *message = [ReadData stringValueFromDictionary:response forKey:RESPONSEKEY_ERRORMESSAGE];
            [appD showAPIErrorWithMessage:message andTitle:ALERT_TITLE_ERROR];
            [self.view setUserInteractionEnabled:YES];
            [HUD hide:YES];
        }
        return;
    }
    
    if ([apiName isEqualToString:API_UPDATE_PROFILE]) {
        [self processResponse:[ReadData dictionaryFromDictionary:response forKey:RESPONSEKEY_DATA]];
    }
}


@end
