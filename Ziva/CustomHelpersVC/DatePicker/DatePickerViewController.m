//
//  DatePickerViewController.m
//  Ziva
//
//  Created by Bharat on 17/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//
#import "DatePickerViewController.h"

@interface DatePickerViewController ()
{
    __weak IBOutlet UIView *containerV;
    __weak IBOutlet UIDatePicker *datePickerV;
    
    __weak IBOutlet UIButton *backB;
}
@end

@implementation DatePickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [datePickerV setDate:[NSDate date]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public Methods

- (void) setMinimumDate :(NSDate  *) value
{
    [datePickerV setMinimumDate:value];
}

- (void) setMaximumDate :(NSDate  *) value
{
    [datePickerV setMaximumDate:value];
}

- (void) setDate :(NSDate  *) value
{
    [datePickerV setDate:value];
}

- (NSDate *) selectedDate
{
    return datePickerV.date;
}

#pragma mark - Delegate methods

-(IBAction)doneWithDatePicker
{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(dateSelected)])
    {
        [self.delegate dateSelected] ;
    }
}

- (IBAction)backBPressed: (UIButton *)sender
{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(hideDatePicker)])
    {
        [self.delegate hideDatePicker];
    }
}

@end
