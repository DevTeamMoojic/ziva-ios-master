//
//  InappBrowserViewController.m
//  Ziva
//
//  Created by Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Ltd. All rights reserved.
//

#import "InappBrowserViewController.h"

@interface InappBrowserViewController ()
{
    __weak IBOutlet UIActivityIndicatorView *loadingAIV;
    
    __weak IBOutlet UIWebView *webView;
    __weak IBOutlet UILabel *titleL;
    
    AppDelegate *appD ;
    
    NSDictionary *infoDict;
    NSURL *promotionURL;
    
}

@end

@implementation InappBrowserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [webView setDelegate:nil];
    [webView stopLoading];
}

#pragma mark - Methods

- (void) loadURL : (NSURL *) url;
{
    promotionURL = url;
}


- (void)loadData
{
    self.navigationController.toolbar.hidden = NO;
    
    NSURLRequest* request = [NSURLRequest requestWithURL:promotionURL];
    [webView loadRequest:request];
}

#pragma mark - Events

- (IBAction)closeClicked:(id)sender;
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - WebView

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [loadingAIV startAnimating];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [loadingAIV stopAnimating];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [loadingAIV stopAnimating];
}

#pragma mark - Alerts

- (void)showOfflineAlert
{
    UIAlertView *offlineAlert = [[UIAlertView alloc] initWithTitle:TOAST_BROWSER_OFFLINE_TITLE
                                                           message:TOAST_BROWSER_OFFLINE_MESSAGE
                                                          delegate:self
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
    [offlineAlert show];
}


@end

