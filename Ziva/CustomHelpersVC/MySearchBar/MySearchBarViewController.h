//
//  MySearchBarViewController.h
//  Ziva
//
//  Created by Bharat on 27/10/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MySearchBarDelegate <NSObject>

- (void) searchCancelled;
- (void) searchTextChanged : (NSString *) searchText;

@end


@interface MySearchBarViewController : UIViewController

@property (nonatomic, weak) id<MySearchBarDelegate> delegate;

- (void) initSearchBar ;
- (void) startSearching ;

@end
