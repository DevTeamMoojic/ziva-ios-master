//
//  TimeCell.h
//  Ziva
//
//  Created by Bharat on 28/09/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *mainV;

@property (weak, nonatomic) IBOutlet UIView *timeV;
@property (weak, nonatomic) IBOutlet UILabel *slotDescL;

@property (weak, nonatomic) IBOutlet UIButton *slotSelectedB;

- (void) configureDataForCell : (NSDictionary *) item ;

- (void) itemSelected : (BOOL) selected ;

@end
