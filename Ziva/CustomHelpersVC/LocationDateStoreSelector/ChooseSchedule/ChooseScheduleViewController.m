//
//  ChooseScheduleViewController.m
//  Ziva
//
//  Created by Bharat on 28/09/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "DateCell.h"
#import "TimeCell.h"
#import "ChooseScheduleViewController.h"

#define NO_OF_DAYS 35 // display schedule for next 5 weeks
#define SELECTOR_PAGE_SIZE 7
#define START_PAGE_INDEX 3

#define DATEEMPTY 11
#define TIMEEMPTY 12
#define NOSTORESFOUND 190

@interface ChooseScheduleViewController ()<MBProgressHUDDelegate, UICollectionViewDelegateFlowLayout, MyCartDelegate>
{
    MBProgressHUD *HUD;
    
    CGSize layoutsize;
    CGSize layoutsize_slots;
    
    NSMutableArray *lstDates;
    NSMutableArray *lstSlots;
    
    __weak IBOutlet UIView *dateCV;
    __weak IBOutlet UIView *dateSelectorV;
    __weak IBOutlet UILabel *selectDateMonthL;
    __weak IBOutlet UICollectionView *datelistV;
    
    __weak IBOutlet UILabel *scheduleTimeHeadingL;
    __weak IBOutlet UIView *zenotiCV;
    __weak IBOutlet UICollectionView *zenotiSlotlistV;
    __weak IBOutlet UILabel *noAvailableSlotsL;
    
    __weak IBOutlet UIView *nonzenotiCV;
    __weak IBOutlet UIDatePicker *timePickerV;
    
    __weak IBOutlet UIView * footerV;
    
    NSInteger pageIndex;
    NSInteger slotIndex;
    
    NSDateFormatter *formatter;
    
    AppDelegate *appD;
    MyCartController *cartC;
    
    NSDate* firstAvailableDate;
    NSString *firstAvailableTimeSlot24Hr ;
    NSString *lastAvailableTimeSlot24Hr ;
    
    BOOL isBackFromOtherView;
}
@end

@implementation ChooseScheduleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    isBackFromOtherView = NO;
    
    formatter = [[NSDateFormatter alloc] init];
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    cartC = appD.sessionDelegate.mycartC;
    [cartC addDelegate:self];
    
    [self setupLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!isBackFromOtherView){
        
    }
    else{
        isBackFromOtherView = NO;
    }
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if(!isBackFromOtherView){
        
        [cartC removeDelegate:self];
    }
}

#pragma mark - Public Methods

- (void) updateBackFromOtherViewStatus : (BOOL) status{
    isBackFromOtherView = status;
}


#pragma mark - Methods

- (void) loadDates
{
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSMutableDictionary *item;
    for(NSInteger ctr = START_PAGE_INDEX; ctr > 0;ctr--){
        NSDate *newDate = [self getDateByAddingDays:-1*ctr ToDate:firstAvailableDate];
        
        item = [NSMutableDictionary dictionary];
        [item setObject:[formatter stringFromDate:newDate] forKey:KEY_SCHEDULE_DATE];
        [item setObject:@YES forKey:KEY_SCHEDULE_LOCKED];
        [lstDates addObject:item];
    }
    
    for(NSInteger ctr = 0; ctr < NO_OF_DAYS;ctr++){
        NSDate *newDate = [self getDateByAddingDays: ctr ToDate:firstAvailableDate];
        
        item = [NSMutableDictionary dictionary];
        [item setObject:[formatter stringFromDate:newDate] forKey:KEY_SCHEDULE_DATE];
        [item setObject:@NO forKey:KEY_SCHEDULE_LOCKED];
        [lstDates addObject:item];
    }
    
    for(NSInteger ctr = NO_OF_DAYS; ctr < (NO_OF_DAYS + START_PAGE_INDEX);ctr++){
        NSDate *newDate = [self getDateByAddingDays: ctr ToDate:firstAvailableDate];
        
        item = [NSMutableDictionary dictionary];
        [item setObject:[formatter stringFromDate:newDate] forKey:KEY_SCHEDULE_DATE];
        [item setObject:@YES forKey:KEY_SCHEDULE_LOCKED];
        [lstDates addObject:item];
    }

    [datelistV reloadData];
    
    pageIndex = START_PAGE_INDEX;
    [self selectDate];
}

- (void) setupLayout
{
    lstDates = [NSMutableArray array];
    lstSlots = [NSMutableArray array];
    
    firstAvailableDate = [self getFirstAvailableDate];
    firstAvailableTimeSlot24Hr = @"09:30:00";
    lastAvailableTimeSlot24Hr = @"21:30:00";
    
    layoutsize_slots = CGSizeMake(71, 40);
    
    layoutsize.width = floor(CGRectGetWidth(datelistV.frame)/SELECTOR_PAGE_SIZE);
    layoutsize.height = CGRectGetHeight(datelistV.frame);
    
    datelistV.frame = [UpdateFrame setSizeForView:datelistV usingWidth:(layoutsize.width * SELECTOR_PAGE_SIZE)];
    datelistV.frame = [UpdateFrame setPositionForView:datelistV usingPositionX:(CGRectGetWidth(self.view.frame) - CGRectGetWidth(datelistV.frame))/2];
    
    dateSelectorV.frame=[UpdateFrame setSizeForView:dateSelectorV usingSize:layoutsize];
    dateSelectorV.center = datelistV.center;
    
    if(cartC.isZenotiEnabled){
        scheduleTimeHeadingL.text = [@"Availability Time Schedule" uppercaseString];
        zenotiCV.hidden = NO;
        nonzenotiCV.hidden= !zenotiCV.hidden;
        noAvailableSlotsL.hidden = YES;
        zenotiSlotlistV.hidden = YES;
    }
    else{
        scheduleTimeHeadingL.text = [@"Choose suitable time" uppercaseString];
        zenotiCV.hidden = YES;
        nonzenotiCV.hidden= !zenotiCV.hidden;
    }
    
    [self loadDates];
}

- (BOOL) isValid{
    BOOL status = YES;
    if ([appD isEmptyString:cartC.bookDate])
    {
        [self showErrorMessage:DATEEMPTY];
        status = NO;
    }
    if ([appD isEmptyString:cartC.bookTimeSlot])
    {
        [self showErrorMessage:TIMEEMPTY];
        status = NO;
    }
    return status;
}

- (void) showErrorMessage : (int)errorCode
{
    NSString *message = @"";
    switch (errorCode) {
        case DATEEMPTY: {
            message = @"Choose date";
        }
            break;
        case TIMEEMPTY: {
            message = @"Choose time";
        }
            break;
        case NOSTORESFOUND: {
            message = @"No stores found";
        }
            break;
        default:
            break;
    }
    UIAlertView *errorNotice = [[UIAlertView alloc] initWithTitle:ALERT_TITLE_ERROR message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorNotice show];
}


#pragma mark - Cart Delegate

- (void) onResponseError{
    [HUD hide:YES];
}

- (void) onRequestBookingGenerated
{
    if(cartC.isZenotiEnabled){
        noAvailableSlotsL.hidden = YES;
        zenotiSlotlistV.hidden = YES;
        [cartC getZenotiAvailability:cartC.storeId];
    }
    else{
        [HUD hide:YES];
        if ([ReadData integerValueFromDictionary:[cartC fetchRequestBookingInfo] forKey:@"StoresFound"] > 0){
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(onsuccess_Schedule)])
            {
                [self.delegate onsuccess_Schedule] ;
            }
        }
        else{
            [self showErrorMessage:NOSTORESFOUND];
        }
    }
}

- (void) onAvailabilityGenerated
{
    NSArray *arrSlots = [cartC fetchAvailibilitySlots];
    if([lstSlots count] > 0) [lstSlots removeAllObjects];
    NSDate *firstSlot = [self getFirstAvailableSlot];
    NSDate *lastSlot = [self getLastAvailableSlot];
    for(NSDictionary *item in arrSlots){
        NSString *slotId = [ReadData stringValueFromDictionary:item forKey:KEY_SLOT_ID];
        [formatter setDateFormat:FORMAT_TIME_24];
        if(([slotId compare:[formatter stringFromDate:firstSlot]] != NSOrderedAscending) && ([slotId compare:[formatter stringFromDate:lastSlot]] != NSOrderedDescending))
        {
            NSMutableDictionary *slot = [NSMutableDictionary dictionary];
            [slot setObject:slotId forKey:KEY_SLOT_ID];
            [slot setObject:[self get24HrTimeSlotIn12Hr:[ReadData stringValueFromDictionary:item forKey:KEY_SLOT_START_TIME]] forKey:KEY_SLOT_START_TIME];
            [slot setObject:[self get24HrTimeSlotIn12Hr:[ReadData stringValueFromDictionary:item forKey:KEY_SLOT_END_TIME]] forKey:KEY_SLOT_END_TIME];
            [lstSlots addObject:slot];
        }
    }
    
    slotIndex = -1;
    noAvailableSlotsL.hidden = ([lstSlots count] > 0);
    [zenotiSlotlistV setContentOffset:CGPointZero animated:NO];
    zenotiSlotlistV.hidden = !noAvailableSlotsL.hidden;
    [zenotiSlotlistV reloadData];
    [HUD hide:YES];
}

- (void) onUpdateZenotiSlot{
    if ([cartC fetchAvailibilityStatus]) {
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(onsuccess_Schedule)])
        {
            [self.delegate onsuccess_Schedule] ;
        }
    }
}

#pragma mark - Date Selector Methods

-(NSInteger) getPageFromOffset : (CGPoint) offset
{
    NSInteger page = START_PAGE_INDEX;
    NSInteger offsetX = (NSInteger)datelistV.contentOffset.x;
    NSInteger cellWidth = (NSInteger)layoutsize.width;
    page += floor((offsetX/cellWidth)) ;
    page+= (((offsetX % cellWidth) > cellWidth *0.5) ? 1 : 0);
    return page;
}

- (void) jumpToItem : (NSInteger) index
{
    pageIndex = index;
    [datelistV scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:pageIndex inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    [self selectDate];
}

-(void) selectDate
{
    NSDictionary *item = [lstDates objectAtIndex:pageIndex];
    cartC.bookDate = [ReadData stringValueFromDictionary:item forKey:KEY_SCHEDULE_DATE];
    cartC.bookTimeSlot = @"";
    for(NSInteger ctr =0; ctr < [lstDates count]; ctr++){
        DateCell *cell = (DateCell *)[datelistV cellForItemAtIndexPath:[NSIndexPath indexPathForRow:ctr inSection:0]];
        [cell itemSelected:(ctr == pageIndex)];
    }
    
    [formatter setDateFormat:FORMAT_DATE];
    NSDate *selectedDate = [formatter dateFromString:cartC.bookDate];
    [formatter setDateFormat:@"MMMM yyyy"];
    selectDateMonthL.text = [formatter stringFromDate:selectedDate];
    
    if(cartC.isZenotiEnabled){
        [self hudShowWithMessage:@""];
        [cartC generateRequestBookingId];
    }
    else{
        NSDate *minDate = [self getFirstAvailableSlot];
        NSDate *maxDate = [self getLastAvailableSlot];
        [timePickerV setMinimumDate:minDate];
        [timePickerV setMaximumDate:maxDate];
        [timePickerV setDate:minDate];
        
        [formatter setDateFormat:FORMAT_TIME_24];
        cartC.bookTimeSlot = [formatter stringFromDate:[timePickerV date]];
    }
}

#pragma mark - Date Helper Methods

- (NSDate *) getDateByAddingDays : (NSInteger) days ToDate : (NSDate *) startDate
{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];

    [dateComponents setDay:days];
    return [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:startDate options:0];
}

- (NSDate *) getFirstAvailableDate
{
    NSDate *retDate = [NSDate date];
    
    [formatter setDateFormat:FORMAT_TIME_24];
    NSString *currentTime = [formatter stringFromDate:retDate];
    if ([lastAvailableTimeSlot24Hr compare:currentTime] == NSOrderedDescending)
    {
        retDate = [self getDateByAddingDays:1 ToDate:retDate];
    }
    return retDate;
}

- (NSDate *) getFirstAvailableSlot
{
    [formatter setDateFormat:FORMAT_DATE_TIME];
    NSDate *retDate =  [formatter dateFromString:[NSString stringWithFormat:@"%@ %@", cartC.bookDate, firstAvailableTimeSlot24Hr]];
    [formatter setDateFormat:FORMAT_DATE];
    if([cartC.bookDate isEqualToString:[formatter stringFromDate:[NSDate date]]]){
        NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
        [dateComponents setMinute:45];
        NSDate *newDate = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:[NSDate date] options:0];
        [formatter setDateFormat:FORMAT_TIME_24];
        NSString *currentTime = [formatter stringFromDate:newDate];
        if ([firstAvailableTimeSlot24Hr compare:currentTime] == NSOrderedAscending)
        {
            [formatter setDateFormat:FORMAT_DATE_TIME];
            retDate =  [formatter dateFromString:[NSString stringWithFormat:@"%@ %@", cartC.bookDate, currentTime]];
        }
    }
    return retDate;
}

- (NSDate *) getLastAvailableSlot
{
    [formatter setDateFormat:FORMAT_DATE_TIME];
    return [formatter dateFromString:[NSString stringWithFormat:@"%@ %@", cartC.bookDate, lastAvailableTimeSlot24Hr]];
}

- (NSString *) get24HrTimeSlotIn12Hr : (NSString *) slot24Hr
{
    [formatter setDateFormat:FORMAT_TIME_24];
    NSDate *tmpDate = [formatter dateFromString:slot24Hr];
    [formatter setDateFormat:FORMAT_TIME_12];
    return [formatter stringFromDate:tmpDate];
}


#pragma mark - HUD & Delegate methods

- (void)hudShowWithMessage:(NSString *)message
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    HUD.dimBackground = YES;
    HUD.labelText = message;
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
    
    [HUD show:YES];
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
    HUD = nil;
}

#pragma mark - Collection View

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return  1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if([collectionView isEqual:datelistV]){
        return [lstDates count];
    }
    else{
        return [lstSlots count];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if([collectionView isEqual:datelistV]){
        return layoutsize;
    }
    else{
        return layoutsize_slots;
    }

}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if([collectionView isEqual:datelistV]){
        // set cell
        DateCell *cell = (DateCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"datecell" forIndexPath:indexPath];

        //Reposition elements
        {
            cell.mainV.frame = [UpdateFrame setSizeForView:cell.mainV usingSize:layoutsize];
        }
        
        cell.dateSelectedB.tag = indexPath.row;
        [cell.dateSelectedB addTarget:self action:@selector(dateselectedBPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        NSDictionary *item = lstDates[indexPath.row];
        [cell configureDataForCell:item usingFormatter:formatter];
        [cell itemSelected:(indexPath.row == pageIndex)];
        
        return cell;
    }
    else{
        TimeCell *cell = (TimeCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"timecell" forIndexPath:indexPath];
        
        //Reposition elements
        {
            cell.mainV.frame = [UpdateFrame setSizeForView:cell.mainV usingSize:layoutsize_slots];
        }
        
        cell.slotSelectedB.tag = indexPath.row;
        [cell.slotSelectedB addTarget:self action:@selector(slotselectedBPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        NSDictionary *item = lstSlots[indexPath.row];
        [cell configureDataForCell:item];
        [cell itemSelected:(indexPath.row == slotIndex)];
        return cell;
    }
}


#pragma mark - Zenoti Slot Selector View Delegate Methods

-(void)slotselectedBPressed:(UIButton *)sender{
    slotIndex = sender.tag;
    NSDictionary *item = lstSlots[slotIndex];
    cartC.bookTimeSlot = [ReadData stringValueFromDictionary:item forKey:KEY_SLOT_ID];
    for(NSInteger ctr =0; ctr < [lstSlots count]; ctr++){
        TimeCell *cell = (TimeCell *)[zenotiSlotlistV cellForItemAtIndexPath:[NSIndexPath indexPathForRow:ctr inSection:0]];
        [cell itemSelected:(ctr == slotIndex)];
    }
}

#pragma mark - Date Selector View Delegate Methods

-(void)dateselectedBPressed:(UIButton *)sender{
    NSInteger page = sender.tag;
    if([ReadData boolValueFromDictionary:[lstDates objectAtIndex:page] forKey:KEY_SCHEDULE_LOCKED]) return;
    if(page != pageIndex)
    {
        [self jumpToItem:page];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if(!decelerate){
        if([scrollView isEqual:datelistV]){
            [self jumpToItem:[self getPageFromOffset:datelistV.contentOffset]];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if([scrollView isEqual:datelistV]){
        [self jumpToItem:[self getPageFromOffset:datelistV.contentOffset]];
    }
}

#pragma mark - Events

- (IBAction)backBPressed:(UIButton *)sender{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(oncancel_Schedule)])
    {
        [self.delegate oncancel_Schedule] ;
    }
}

- (IBAction)proceedBPressed:(UIButton *)sender{
    if([self isValid]){
        if(cartC.isZenotiEnabled){
            [cartC updateZenotiSlot];
        }
        else{
            [self hudShowWithMessage:@""];
            [cartC generateRequestBookingId];
        }
    }
}

- (IBAction)timeSelectionChanged:(UIDatePicker *)sender{
    [formatter setDateFormat:FORMAT_TIME_24];
    cartC.bookTimeSlot = [formatter stringFromDate:[timePickerV date]];
}

@end
