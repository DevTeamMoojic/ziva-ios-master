//
//  DateCell.m
//  Ziva
//
//  Created by Bharat on 28/09/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "DateCell.h"

@implementation DateCell

- (void)awakeFromNib {
    // Initialization code
    
    [super awakeFromNib];
    
    // Fix : Autoresizing issues for iOS 7 using Xcode 6
    self.contentView.frame = self.bounds;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //    Fix : For right aligned view with autoresizing
    UIView *backV = [[self.contentView subviews] objectAtIndex:0];
    backV.frame = [UpdateFrame setSizeForView:backV usingSize:self.bounds.size];
}

- (void) itemSelected : (BOOL) selected  {
    UIColor *selectedColor = [UIColor whiteColor];
    UIColor *notselectedColor= [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.4];
    
    self.dateSelectedB.selected = selected;
    self.dayPartL.textColor = selected ? selectedColor : notselectedColor;
    self.datePartL.textColor = selected ? selectedColor : notselectedColor;
}

- (NSString *) getDayOfWeekPart : (NSDate *) recordDate usingFormatter : (NSDateFormatter *) formatter
{
    formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [formatter setDateFormat:FORMAT_WEEKDAY];
    
    return [formatter stringFromDate:recordDate];
}

- (NSString *) getDatePart : (NSDate *) recordDate usingFormatter : (NSDateFormatter *) formatter
{
    formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [formatter setDateFormat:@"dd"];
    
    return [formatter stringFromDate:recordDate];
}

- (void) configureDataForCell : (NSDictionary *) item usingFormatter : (NSDateFormatter *) formatter
{
    [formatter setDateFormat:FORMAT_DATE];
    NSDate *recordDate =[formatter dateFromString: [ReadData stringValueFromDictionary:item forKey:KEY_SCHEDULE_DATE]] ;
    
    self.dayPartL.text = [self getDayOfWeekPart:recordDate usingFormatter:formatter];
    self.datePartL.text = [self getDatePart:recordDate usingFormatter:formatter];
    
    self.dateSelectedB.selected = NO;
}

@end

