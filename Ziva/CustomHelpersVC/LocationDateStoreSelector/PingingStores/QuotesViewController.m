//
//  QuotesViewController.m
//  Ziva
//
//  Created by Bharat on 14/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "QuotesViewController.h"

@interface QuotesViewController ()
{
    __weak IBOutlet UIScrollView *mainscrollV;
    __weak IBOutlet UIView *dummyView;
    
    NSMutableArray *lstQuotes;
    
    CGFloat pageWidth;
    NSInteger pageIndex;
    
    AppDelegate *appD;
    
    NSInteger timerElapsed;
    NSInteger ticksToChange;
}
@end

@implementation QuotesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self setupQuotes];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public Methods

- (void) startTimer : (NSInteger) totalTimeInSeconds;
{
    pageIndex = 0;
    timerElapsed = 0;
    [mainscrollV setContentOffset:CGPointZero animated:NO];
    ticksToChange = totalTimeInSeconds/[lstQuotes count];
}

- (void) updateTicks
{
    timerElapsed++;
    if(timerElapsed % ticksToChange == 0)
    {
        pageIndex++;
        if(pageIndex < [lstQuotes count]){
            [mainscrollV setContentOffset:CGPointMake(pageWidth*pageIndex, 0) animated:YES];
        }
    }
}

#pragma mark - Methods

- (void) setupQuotes
{
    pageWidth = CGRectGetWidth(mainscrollV.frame);
    lstQuotes = [NSMutableArray array];
    if(![[appD getLoggedInUserGender] isEqualToString: GENDER_MALE]){
        [lstQuotes addObject:[self generateQuote:@"A well-tied tie is the first serious step in life." byAuthor: @"Oscar Wilde"]];
        
        [lstQuotes addObject:[self generateQuote:@"The Scruffier your beard, the sharper you need to dress." byAuthor: @"Ashton Kutcher"]];
        
        [lstQuotes addObject:[self generateQuote:@"Clothes and manners do not make the man; but when he is made, they greatly improve his appearance." byAuthor: @"Arthur Ashe, Professional Tennis Player"]];
        
        [lstQuotes addObject:[self generateQuote:@"To achieve the nonchalance which is absolutely necessary for a man, one article at least must not match." byAuthor: @"Hardy Amies"]];
        
        [lstQuotes addObject:[self generateQuote:@"Putting on a beautifully designed suit elevates my spirit, extols my sense of self, and helps define me as a man to whom details matter." byAuthor: @"Gay Talese"]];
        
        [lstQuotes addObject:[self generateQuote:@"Clothes don't make a man, but clothes have got many a man a good job." byAuthor: @"Herbert Harold Vreeland"]];
        
        [lstQuotes addObject:[self generateQuote:@"Looking good isn’t self-importance; it’s self-respect."byAuthor: @"Charles Hix"]];
        
        [lstQuotes addObject:[self generateQuote:@"The well-dressed man is he whose clothes you never notice."byAuthor: @"William Somerset Maugham"]];
    }
    else{
        [lstQuotes addObject:[self generateQuote:@"A girl should be two things: classy and fabulous." byAuthor: @"Coco Chanel"]];
        
        [lstQuotes addObject:[self generateQuote:@"You are imperfect, permanently and inevitably flawed. And you are beautiful." byAuthor: @"Amy Bloom"]];
        
        [lstQuotes addObject:[self generateQuote:@"Everything has beauty, but not everyone sees it." byAuthor: @"Confucius"]];
        
        [lstQuotes addObject:[self generateQuote:@"Crying is for plain women. Pretty women go shopping." byAuthor: @"Oscar Wilde"]];
        
        [lstQuotes addObject:[self generateQuote:@"The absence of flaw in beauty is itself a flaw." byAuthor: @"Havelock Ellis"]];
        
        [lstQuotes addObject:[self generateQuote:@"I find beauty in unusual things, like hanging your head out the window or sitting on a fire escape." byAuthor: @"Scarlett Johansson"]];
        
        [lstQuotes addObject:[self generateQuote:@"It is better to be beautiful than to be good. But it is better to be good than to be ugly." byAuthor: @"Oscar Wilde"]];
        
        [lstQuotes addObject:[self generateQuote:@"There are various orders of beauty, causing men to make fools of themselves in various styles." byAuthor: @"George Eliot"]];
        
        [lstQuotes addObject:[self generateQuote:@"I found a money back guarantee on a beauty cream. Rushed down to the store. They took one look at me and paid me in advance." byAuthor: @"Phyllis Diller"]];
        
        [lstQuotes addObject:[self generateQuote:@"Fashion fades away, only style remains the same." byAuthor: @"Coco Chanel"]];
        
        [lstQuotes addObject:[self generateQuote:@"Style isn't just about what you wear, it's about how you live." byAuthor: @"Lilly Pulitzer"]];
        
        [lstQuotes addObject:[self generateQuote:@"Fashion can be bought. Style one must possess." byAuthor: @"Edna Woolman Chase"]];
        
        [lstQuotes addObject:[self generateQuote:@"The Best things in life are Free. The Second-Best are very expensive." byAuthor: @"Coco Chanel"]];
        
        [lstQuotes addObject:[self generateQuote:@"Dressing well is a form of good manners." byAuthor: @"Tom Ford"]];
        
//        [lstQuotes addObject:[self generateQuote:@"Fashion says me too, Style says Me Only." byAuthor: @"Anonymous"]];
        
        [lstQuotes addObject:[self generateQuote:@"Personality Begins where Comparison Ends." byAuthor: @"Karl Lagerfeld"]];
        
        [lstQuotes addObject:[self generateQuote:@"I would hate for someone to look at my shoes and say, 'Oh my god, that looks so comfortable!'" byAuthor: @"Christian Louboutin"]];
        
        [lstQuotes addObject:[self generateQuote:@"Looking good isnt self Importance; it's self respect." byAuthor: @"Charles Hix"]];
        
        [lstQuotes addObject:[self generateQuote:@"Beauty doesnt last forever but a Beautiful personality does."byAuthor: @"Anonymous"]];
        
        [lstQuotes addObject:[self generateQuote:@"I love the Confidence that Makeup gives me." byAuthor: @"Tyra Banks"]];
        
        [lstQuotes addObject:[self generateQuote:@"The best thing is to look natural, but it takes makeup to look Natural." byAuthor: @"Calvin Klein"]];
        
    }
    
    
    for (UIView* v in mainscrollV.subviews)
    {
        if(v.tag >= TAG_START_INDEX)
            [v removeFromSuperview];
    }
    
    CGFloat offsetX = 0;
    NSInteger ctr = TAG_START_INDEX;
    for(NSDictionary *item in lstQuotes)
    {
        NSData *tempArchive = [NSKeyedArchiver archivedDataWithRootObject:dummyView];
        UIView *quoteV = (UIView *)[NSKeyedUnarchiver unarchiveObjectWithData:tempArchive];
        quoteV.tag = ctr;
        quoteV.hidden = NO;
        quoteV.frame = [UpdateFrame setPositionForView:quoteV usingPositionX:offsetX andPositionY:0];
        quoteV.frame = [UpdateFrame setSizeForView:quoteV usingWidth:pageWidth];
        
        UILabel *lbl = [quoteV viewWithTag:200];
        lbl.attributedText = [self getFormattedQuoteText:item];
        
        offsetX+=CGRectGetWidth(quoteV.frame);
        ctr++;
        
        [mainscrollV addSubview:quoteV];
    }
    [mainscrollV setContentSize:CGSizeMake(offsetX, CGRectGetHeight(mainscrollV.frame))];
    
}

- (NSMutableDictionary *) generateQuote : (NSString *) title byAuthor : (NSString *) authorName
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:title forKey:KEY_QUOTED_TEXT];
    [dict setObject:authorName forKey:KEY_QUOTED_BY];
    return dict;
}

- (NSMutableAttributedString *)getFormattedQuoteText : (NSDictionary *) dict
{
    NSMutableAttributedString *aAttrString ;
    NSDictionary *tFontDict = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[UIFont getFontType:FONT_REGULAR andSize:12],[UIColor blackColor],Nil] forKeys:[NSArray arrayWithObjects:NSFontAttributeName,NSForegroundColorAttributeName,Nil]];
    NSDictionary *iFontDict = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[UIFont getFontType:FONT_ITALIC_BOLD andSize:12],[UIColor blackColor],Nil] forKeys:[NSArray arrayWithObjects:NSFontAttributeName,NSForegroundColorAttributeName,Nil]];
    
    aAttrString = [[NSMutableAttributedString alloc] initWithString: [ReadData stringValueFromDictionary:dict forKey:KEY_QUOTED_TEXT]  attributes: tFontDict];
    [aAttrString appendAttributedString:  [[NSMutableAttributedString alloc] initWithString:@"\n" attributes: tFontDict]];
    [aAttrString appendAttributedString:  [[NSMutableAttributedString alloc] initWithString: @" - " attributes: iFontDict]];
    [aAttrString appendAttributedString:  [[NSMutableAttributedString alloc] initWithString:[ReadData stringValueFromDictionary:dict forKey:KEY_QUOTED_BY] attributes: iFontDict]];
    return  aAttrString;
}

@end
