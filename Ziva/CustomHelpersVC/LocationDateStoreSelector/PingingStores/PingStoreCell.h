//
//  PingStoreCell.h
//  Ziva
//
//  Created by Bharat on 03/10/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PingStoreCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *mainV;

@property (weak, nonatomic) IBOutlet UIView *outerCircleV;
@property (weak, nonatomic) IBOutlet UIView *innerCircleV;
@property (weak, nonatomic) IBOutlet UILabel *priceL;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingAIV;

@property (weak, nonatomic) IBOutlet UILabel *storeNameL;
@property (weak, nonatomic) IBOutlet UILabel *addressL;

@property (weak, nonatomic) IBOutlet UILabel *bookingslotL;
@property (weak, nonatomic) IBOutlet UILabel *distanceL;


- (void) configureDataForCell : (NSDictionary *) item ;

@end
