//
//  QuotesViewController.h
//  Ziva
//
//  Created by Bharat on 14/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuotesViewController : UIViewController

- (void) startTimer : (NSInteger) totalTimeInSeconds;

- (void) updateTicks;

@end
