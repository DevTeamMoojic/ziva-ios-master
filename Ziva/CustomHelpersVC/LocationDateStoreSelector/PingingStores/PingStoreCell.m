//
//  PingStoreCell.m
//  Ziva
//
//  Created by Bharat on 03/10/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "PingStoreCell.h"

@implementation PingStoreCell

- (void)awakeFromNib {
    // Initialization code
    
    [super awakeFromNib];
    
    // Fix : Autoresizing issues for iOS 7 using Xcode 6
    self.contentView.frame = self.bounds;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //    Fix : For right aligned view with autoresizing
    UIView *backV = [[self.contentView subviews] objectAtIndex:0];
    backV.frame = [UpdateFrame setSizeForView:backV usingSize:self.bounds.size];
}


- (void) configureDataForCell : (NSDictionary *) item
{
    self.outerCircleV.layer.cornerRadius = CGRectGetWidth(self.outerCircleV.frame) *0.5;
    self.innerCircleV.layer.cornerRadius = CGRectGetWidth(self.innerCircleV.frame) *0.5;
    
    self.priceL.text = [ReadData amountInRsFromDictionary:item forKey:KEY_PING_PRICE];
    self.storeNameL.text = [ReadData stringValueFromDictionary:item forKey:KEY_PING_STORENAME];
    self.addressL.text = [ReadData stringValueFromDictionary:item forKey:KEY_PING_STOREADDRESS];
    
    self.bookingslotL.text = [ReadData stringValueFromDictionary:item forKey:KEY_PING_DISPLAY_SLOT];
    self.distanceL.text = [ReadData distanceInKmFromDictionary:item forKey:KEY_PING_DISTANCE];
}

@end


