//
//  OrderLineItemsViewController.h
//  Ziva
//
//  Created by Bharat on 12/10/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderLineItemsViewController : UIViewController

- (void) loadData : (NSArray *) dataArray withHeading :(NSString *) cartType ;

@end
