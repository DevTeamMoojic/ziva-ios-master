//
//  CartSummaryViewController.h
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CartSummaryDelegate <NSObject>

- (void) takeActionOnCart;

@end

@interface CartSummaryViewController : UIViewController

@property (nonatomic, weak) id<CartSummaryDelegate> delegate;

- (void) setActionOnCart : (NSString *) keyName;

- (void) updateSummary ;

@end
