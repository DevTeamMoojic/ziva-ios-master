//
//  UpgradeAppViewController.m
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "UpgradeAppViewController.h"

@interface UpgradeAppViewController (){
 
    BOOL isUpdateNotification;
}
@end

@implementation UpgradeAppViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    isUpdateNotification = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Events

- (IBAction)updateBPressed:(id)sender;
{
    NSString *iTunesLink = @"itms://itunes.apple.com/app/id1031579277";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
}

- (IBAction)closeBPressed:(id)sender;
{
    if (isUpdateNotification)
        [self.presentingViewController dismissViewControllerAnimated:YES completion:Nil];
}



@end
