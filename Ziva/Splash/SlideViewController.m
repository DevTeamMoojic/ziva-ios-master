//
//  SlideViewController.m
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "SlideViewController.h"

@interface SlideViewController (){
    
    __weak IBOutlet UIImageView *slideImgV;
    NSInteger slideNumber;
    
}
@end

@implementation SlideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma - Methods

-(void) showSlideNumber : (NSInteger) index{
    slideNumber = index;
    [self setUpSlideImage];
}

-(void) setUpSlideImage{
    
    if(IS_IPHONE4){
        slideImgV.image = [UIImage imageNamed:[NSString stringWithFormat:@"%02ld_screen_4s", (long)slideNumber]];
    }
    else{
        slideImgV.image = [UIImage imageNamed:[NSString stringWithFormat:@"%02ld_screen", (long)slideNumber]];
    }
}

@end
