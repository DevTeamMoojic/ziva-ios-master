//
//  SlideShowViewController.m
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "SlideViewController.h"
#import "SlideShowViewController.h"

#define SLIDESHOW_LENGTH 6
#define TOTAL_SLIDES 8

@interface SlideShowViewController (){
 
    
    __weak IBOutlet UIScrollView *mainscrollV;
    __weak IBOutlet UIPageControl *pagecontrol;
    
    NSUInteger pageIndex;
    NSInteger pageWidth;
    NSInteger pageHeight;
    
    AppDelegate *appD;
}
@end

@implementation SlideShowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self setupSlides];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma - Methods

- (void) setupSlides{
    pageWidth = CGRectGetWidth(mainscrollV.frame);
    pageHeight = CGRectGetHeight(mainscrollV.frame);
    mainscrollV.contentSize = CGSizeMake(pageWidth * TOTAL_SLIDES, CGRectGetHeight(mainscrollV.frame));
    //tabcontentscrollView.scrollsToTop = NO;
    
    SlideViewController  *controller;
    for (NSInteger ctr = 0; ctr < TOTAL_SLIDES; ctr++)
    {
        controller = [self.storyboard instantiateViewControllerWithIdentifier:@"slideVC"];
        // add the controller's view to the scroll view
        if (controller.view.superview == nil)
        {
            controller.view.frame = [UpdateFrame setPositionForView:mainscrollV usingPositionX:pageWidth * ctr andPositionY:0];
            
            [self addChildViewController:controller];
            [mainscrollV addSubview:controller.view];
            [controller didMoveToParentViewController:self];
        }
        if(ctr == 0){
            [controller showSlideNumber:SLIDESHOW_LENGTH];
        }
        else if (ctr > SLIDESHOW_LENGTH){
            [controller showSlideNumber:1];
        }
        else{
            [controller showSlideNumber:ctr];
        }
    }
    
    [mainscrollV scrollRectToVisible:CGRectMake(pageWidth,0,pageWidth,pageHeight) animated:NO];
    
    NSTimer *imageChangeTimer = [NSTimer timerWithTimeInterval:SLIDE_VIEW_TOGGLE
                                                        target:self
                                                      selector:@selector(imageChange)
                                                      userInfo:nil
                                                       repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:imageChangeTimer forMode:NSDefaultRunLoopMode];
    
    
    pagecontrol.numberOfPages = SLIDESHOW_LENGTH;
    pagecontrol.currentPage = 0;
    pageIndex = 1;
}


- (void)imageChange
{
    pageIndex++;
    [mainscrollV setContentOffset:CGPointMake(pageWidth*pageIndex, 0) animated:YES];
    pagecontrol.currentPage = pageIndex-1;
    
}

#pragma mark - Events

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
//     The key is repositioning without animation
    if (scrollView.contentOffset.x == 0) {
        // user is scrolling to the left from image 1 to image n
        // reposition offset to show image n that is on the right in the scroll view
        [scrollView scrollRectToVisible:CGRectMake(pageWidth*SLIDESHOW_LENGTH,0,pageWidth,pageHeight) animated:NO];
    }
    else if (scrollView.contentOffset.x == pageWidth*(SLIDESHOW_LENGTH + 1)) {
        // user is scrolling to the right from image n to image 1
        // reposition offset to show image 1 that is on the left in the scroll view
        [scrollView scrollRectToVisible:CGRectMake(pageWidth,0,pageWidth,pageHeight) animated:NO];
        pageIndex = 1;
        pagecontrol.currentPage =0 ;
    }
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
}


@end
