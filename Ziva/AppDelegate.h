//
//  AppDelegate.h
//  Ziva
//
//  Created by Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "SessionDelegate.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) SessionDelegate *sessionDelegate;

- (BOOL)isFirstRun;
- (BOOL)isLoggedIn;
- (BOOL)isVersionUpgrade;

- (NSString *) getAppEnvironment;
- (NSString *) getAppVersionNo;

- (void) pushProfile : (BOOL) newSesion;
- (void) loginUser;
- (void) logoutUser;

- (void) showSlideshowVC;
- (void) showSigninVC;
- (void) showHomeVC : (BOOL) newsSession;
- (void) showChooseGenderVC;

- (void) forceAppUpdate;

- (void) showAPIErrorWithMessage : (NSString *) message andTitle : (NSString *) title;

- (void) registerForRemoteNotification;

- (void) recordEvent : (NSString *) eventName withProperties : (NSDictionary *)dict;

- (void) startLocationServices;
- (CLLocation *) getUserCurrentLocation ;
- (NSString *) getUserCurrentLocationLatitude ;
- (NSString *) getUserCurrentLocationLongitude ;
- (NSString *) getUserCurrentLocationAddress ;

- (BOOL) isEmptyString : (NSString *) str;
- (BOOL) NSStringIsValidMobile:(NSString *)str ;
- (BOOL) NSStringIsValidEmail:(NSString *) str;

- (NSString *) getLoggedInUserId ;
- (NSString *) getLoggedInUserGender ;
- (NSString *) getLoggedInUserDisplayName;
- (NSString *) getLoggedInUserFirstName ;
- (NSString *) getLoggedInUserLastName ;
- (NSString *) getLoggedInUserMobileNumber;
- (NSString *) getLoggedInUserEmailAddress;
- (NSString *) getLoggedInUserProfileURL;
- (NSString *) getLoggedInUserBirthDate;
- (BOOL) isProfileCompleted;

- (NSArray *) excludeShareItems;

- (void) startChatSupport : (NSString *) message;
- (void) startSupportCall ;

- (void) profileUpdated;
- (void) locationUpdated;
- (void) backToHome ;


@end

