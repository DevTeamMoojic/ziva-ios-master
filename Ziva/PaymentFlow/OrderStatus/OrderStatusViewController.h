//
//  OrderStatusViewController.h
//  Ziva
//
//  Created by Bharat on 14/10/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OrderStatusDelegate <NSObject>

@optional

- (void) onRetry_Booking;

- (void) onCompleted_Booking;

- (void) onView_Appointment:(NSString *) orderId;

@end


@interface OrderStatusViewController : UIViewController

@property (nonatomic, weak) id<OrderStatusDelegate> delegate;

- (void) showPaymentConfirmMsg;

- (void) showPaymentFailureMsg;

@end
