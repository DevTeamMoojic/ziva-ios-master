//
//  CashPaymentViewController.h
//  Ziva
//
//  Created by Bharat on 14/10/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CashPaymentDelegate <NSObject>

@optional

- (void) oncancel_CashPayment;

- (void) oncompleted_CashTransaction : (BOOL) status;

@end


@interface CashPaymentViewController : UIViewController

@property (nonatomic, weak) id<CashPaymentDelegate> delegate;

- (void) initiatePayment;

@end
