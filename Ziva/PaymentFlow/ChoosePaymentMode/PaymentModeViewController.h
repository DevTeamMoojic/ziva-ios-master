//
//  PaymentModeViewController.h
//  Ziva
//
//  Created by Bharat on 14/10/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PaymentModeDelegate <NSObject>

@optional

- (void) oncancel_PaymentMode;

- (void) onapply_PaymentMode : (NSString *) mode;

@end


@interface PaymentModeViewController : UIViewController

@property (nonatomic, weak) id<PaymentModeDelegate> delegate;

@end
