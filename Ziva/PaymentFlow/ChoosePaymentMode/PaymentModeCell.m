//
//  PaymentModeCell.m
//  Ziva
//
//  Created by Bharat on 14/10/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "PaymentModeCell.h"

@implementation PaymentModeCell

- (void)awakeFromNib {
    // Initialization code
    
    [super awakeFromNib];
    
    // Fix : Autoresizing issues for iOS 7 using Xcode 6
    self.contentView.frame = self.bounds;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //    Fix : For right aligned view with autoresizing
    UIView *backV = [[self.contentView subviews] objectAtIndex:0];
    backV.frame = [UpdateFrame setSizeForView:backV usingSize:self.bounds.size];
}

- (NSMutableAttributedString *) getAttributedString_Title:(NSString *) title andDescription : (NSString *) subTitle
{
    NSMutableAttributedString *aAttrString ;
    NSDictionary *tFontDict = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[UIFont semiboldFontOfSize:15],[UIColor blackColor],Nil] forKeys:[NSArray arrayWithObjects:NSFontAttributeName,NSForegroundColorAttributeName,Nil]];
    aAttrString = [[NSMutableAttributedString alloc] initWithString:title attributes: tFontDict];
    
    if([subTitle length] > 0)
    {
        NSDictionary *sFontDict = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[UIFont lightFontOfSize:13],[UIColor greyTextDisplayColor],Nil] forKeys:[NSArray arrayWithObjects:NSFontAttributeName,NSForegroundColorAttributeName,Nil]];
        [aAttrString appendAttributedString:  [[NSMutableAttributedString alloc] initWithString:@"\n" attributes: sFontDict]];
        [aAttrString appendAttributedString:  [[NSMutableAttributedString alloc] initWithString:subTitle attributes: sFontDict]];
    }
    return  aAttrString;
}



- (void) configureDataForCell : (NSDictionary *) item
{
    self.iconImgV.image = [UIImage imageNamed:[ReadData stringValueFromDictionary:item forKey:KEY_PAYMODE_ICON]];
    NSString *heading = [ReadData stringValueFromDictionary:item forKey:KEY_PAYMODE_TITLE];
    NSString *subHeading = [ReadData stringValueFromDictionary:item forKey:KEY_PAYMODE_SUBTITLE];
    self.titleL.attributedText = [ self getAttributedString_Title:heading andDescription:subHeading];    
}

@end
