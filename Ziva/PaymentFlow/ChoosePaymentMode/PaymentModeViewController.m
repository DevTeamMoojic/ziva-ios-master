//
//  PaymentModeViewController.m
//  Ziva
//
//  Created by Bharat on 14/10/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "PaymentModeCell.h"
#import "PaymentModeViewController.h"

#define ROW_HEIGHT 60

@interface PaymentModeViewController ()
{
    NSMutableArray *list;
    
    __weak IBOutlet UIView *optionsV;    
    __weak IBOutlet UILabel *screenTitleL;
    
    __weak IBOutlet UIView *containerView;
    __weak IBOutlet UIView *topImageV;
    __weak IBOutlet UIImageView *mainImgV;
    
    __weak IBOutlet UIScrollView *scrollviewOnTop;
    
    __weak IBOutlet UIScrollView *tabcontentscrollView;
    
    __weak IBOutlet UITableView *listTblV;
    
    CGRect cachedImageViewSize;
    CGFloat yscrollOffset;
    
    BOOL isBackFromOtherView;
    BOOL isContentSizeUpdated;
    
    NSInteger pageWidth;
    
    AppDelegate *appD;
    MyCartController *cartC;
    
    CGFloat contentHeight;
    
}
@end

@implementation PaymentModeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    cartC = appD.sessionDelegate.mycartC;
    
    [self setupLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

#pragma mark - Layout

- (void) loadPaymentOptions
{
    list = [NSMutableArray array];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    // minnarao add new code.
    [dict setObject:PAYMENT_MODE_CARDSSAVED forKey:KEY_PAYMODE_TITLE];
    [dict setObject:@"Saved Visa, Master, Amex, Diners & Discover Cards" forKey:KEY_PAYMODE_SUBTITLE];
    [dict setObject:@"icon_option_card" forKey:KEY_PAYMODE_ICON];
    [list addObject:dict];
    //***
    dict = [NSMutableDictionary dictionary];
    [dict setObject:PAYMENT_MODE_CREDITCARD forKey:KEY_PAYMODE_TITLE];
    [dict setObject:@"Visa, Master, Amex, Diners & Discover" forKey:KEY_PAYMODE_SUBTITLE];
    [dict setObject:@"icon_option_card" forKey:KEY_PAYMODE_ICON];
    [list addObject:dict];
    
    dict = [NSMutableDictionary dictionary];
    [dict setObject:PAYMENT_MODE_DEBITCARD forKey:KEY_PAYMODE_TITLE];
    [dict setObject:@"Visa, Master, Maestro & Rupay" forKey:KEY_PAYMODE_SUBTITLE];
    [dict setObject:@"icon_option_card" forKey:KEY_PAYMODE_ICON];
    [list addObject:dict];

    dict = [NSMutableDictionary dictionary];
    [dict setObject:PAYMENT_MODE_NETBANKING forKey:KEY_PAYMODE_TITLE];
    [dict setObject:@"All Major banks are supported" forKey:KEY_PAYMODE_SUBTITLE];
    [dict setObject:@"icon_option_netbanking" forKey:KEY_PAYMODE_ICON];
    [list addObject:dict];

//    dict = [NSMutableDictionary dictionary];
//    [dict setObject:PAYMENT_MODE_MOBILEWALLET forKey:KEY_PAYMODE_TITLE];
//    [dict setObject:@"PayTM, PayUMoney, MobiKwik & others" forKey:KEY_PAYMODE_SUBTITLE];
//    [dict setObject:@"icon_option_mobilewallet" forKey:KEY_PAYMODE_ICON];
//    [list addObject:dict];

    if(cartC.isCashPaymentAllowed){
        dict = [NSMutableDictionary dictionary];
        [dict setObject:PAYMENT_MODE_CASH forKey:KEY_PAYMODE_TITLE];
        [dict setObject:@"" forKey:KEY_PAYMODE_SUBTITLE];
        [dict setObject:@"icon_option_cash" forKey:KEY_PAYMODE_ICON];
        [list addObject:dict];
    }
}

- (void) setupLayout
{
    [self loadPaymentOptions];
    
    pageWidth = CGRectGetWidth(self.view.bounds);
    
    [self.view addGestureRecognizer:scrollviewOnTop.panGestureRecognizer];
    [listTblV removeGestureRecognizer:listTblV.panGestureRecognizer];
    
    //Position elements
    {
        topImageV.frame = [UpdateFrame setSizeForView:topImageV usingHeight:[ResizeToFitContent getHeightForParallaxImage:self.view.frame]];
        
        yscrollOffset = CGRectGetHeight(topImageV.frame) - IMAGE_SCROLL_OFFSET_Y - CGRectGetHeight(optionsV.frame);
        mainImgV.frame = topImageV.frame ;
        
        containerView.frame = [UpdateFrame setSizeForView:containerView usingHeight:(CGRectGetHeight(self.view.bounds) + yscrollOffset)];
        
        tabcontentscrollView.frame= [UpdateFrame setPositionForView:tabcontentscrollView usingPositionY: CGRectGetMaxY(topImageV.frame)];
        tabcontentscrollView.frame = [UpdateFrame setSizeForView:tabcontentscrollView usingHeight: CGRectGetHeight(containerView.frame) - CGRectGetMaxY(topImageV.frame)];
    }
    
    
    CGPoint scrollviewOrigin = scrollviewOnTop.frame.origin;
    scrollviewOnTop.scrollIndicatorInsets = UIEdgeInsetsMake(-scrollviewOrigin.y, 0, scrollviewOrigin.y, scrollviewOrigin.x);
    
    [self reloadList];
}

#pragma mark - Methods

- (void) reloadList
{
    [listTblV reloadData];
    [listTblV setContentOffset:CGPointZero animated:NO];
    
    contentHeight = CGRectGetMinY(listTblV.frame) + [self calculateHeightOfList];
    if(contentHeight <= CGRectGetHeight(tabcontentscrollView.bounds)){
        contentHeight = CGRectGetHeight(tabcontentscrollView.bounds);
        listTblV.scrollEnabled = NO;
    }
    listTblV.frame = [UpdateFrame setSizeForView:listTblV usingHeight:[self calculateHeightOfList]];
    
    listTblV.hidden = ([list count] == 0);
    
    [self tabChanged];
    
}

-(CGFloat) calculateHeightOfList
{
    return ([list count]  * ROW_HEIGHT);
}

- (void) tabChanged
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        CGPoint offset = [self getScrollContentOffset];
        offset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:offset animated:NO];
    }
    isContentSizeUpdated = NO;
}

#pragma mark - Expand on Scroll animation

- (CGSize) setContentSize
{
    CGFloat height = CGRectGetHeight(mainImgV.frame) + contentHeight;
    return CGSizeMake(pageWidth, height);
}

- (void) setScrollContentOffset: (CGPoint) offset
{
    listTblV.contentOffset = offset;
}

- (CGPoint) getScrollContentOffset
{
    return [listTblV contentOffset];
}

- (void) resetOffsetAllTabs
{
    CGPoint offset = CGPointZero;
    [self setScrollContentOffset : offset];
}

-(void) updateContentSize : (CGPoint) contentOffset;
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        contentOffset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    }
    [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    
    isContentSizeUpdated = NO;
}

#pragma mark - Table view data source

-(CGFloat) calculateheightForRow:(NSInteger)row{
    return ROW_HEIGHT;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [list count] ;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self calculateheightForRow:indexPath.row];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PaymentModeCell *cell = (PaymentModeCell *)[tableView dequeueReusableCellWithIdentifier:@"paymentmodecell" forIndexPath:indexPath];
    NSDictionary *item = list[indexPath.row];
    [cell configureDataForCell :item];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSDictionary *item = list[indexPath.row];
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(onapply_PaymentMode:)])
    {
        [self.delegate onapply_PaymentMode : [ReadData stringValueFromDictionary:item forKey:KEY_PAYMODE_TITLE]] ;
    }    
}



#pragma mark - Scroll Events

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == scrollviewOnTop)
    {
        {
            if (isContentSizeUpdated)  return;
            
            CGRect scrolledBoundsForContainerView = containerView.bounds;
            if (scrollView.contentOffset.y <= yscrollOffset)
            {
                
                scrolledBoundsForContainerView.origin.y = scrollView.contentOffset.y ;
                containerView.bounds = scrolledBoundsForContainerView;
                
                //Reset offset for all tabs
                if (scrollView.contentOffset.y <= 0) [self resetOffsetAllTabs];
                
                
                CGFloat y = -scrollView.contentOffset.y;
                CGFloat alphaLevel = 1;
                CGFloat BLUR_MAX_Y = IMAGE_SCROLL_OFFSET_Y + CGRectGetHeight(topImageV.frame) - CGRectGetMinY(optionsV.frame);
                if (fabs(y) < BLUR_MAX_Y)
                {
                    alphaLevel = (BLUR_MAX_Y - fabs(y))/(BLUR_MAX_Y);
                }
                else
                {
                    alphaLevel = 0;
                }
                
                //[screenTitleL setAlpha:alphaLevel];
                if (y > 0)
                {
                    mainImgV.frame = CGRectInset(cachedImageViewSize, 0, -y/2);
                    mainImgV.center = CGPointMake(mainImgV.center.x, mainImgV.center.y + y/2);
                }
                else
                {
                    mainImgV.frame = [UpdateFrame setPositionForView:mainImgV usingPositionY:y];
                }
                return;
            }
            
            scrolledBoundsForContainerView.origin.y = yscrollOffset ;
            containerView.bounds = scrolledBoundsForContainerView;
            
            [self setScrollContentOffset:CGPointMake(0, scrollView.contentOffset.y - yscrollOffset)  ];
        }
    }
}


#pragma mark - Events

- (IBAction)closeBPressed:(UIButton *)sender
{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(oncancel_PaymentMode)])
    {
        [self.delegate oncancel_PaymentMode] ;
    }
}

@end
