//
//  ChooseCouponViewController.h
//  Ziva
//
//  Created by Bharat on 03/10/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChooseCouponDelegate <NSObject>

@optional

- (void) oncancel_Coupon;

- (void) onapply_Coupon : (NSString *) code;

@end

@interface ChooseCouponViewController : UIViewController

@property (nonatomic, weak) id<ChooseCouponDelegate> delegate;


- (void) showCouponsListing ;

@end
