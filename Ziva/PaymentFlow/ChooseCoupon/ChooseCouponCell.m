//
//  ChooseCouponCell.m
//  Ziva
//
//  Created by Bharat on 13/10/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "ChooseCouponCell.h"

@implementation ChooseCouponCell

- (void)awakeFromNib {
    // Initialization code
    
    [super awakeFromNib];
    
    // Fix : Autoresizing issues for iOS 7 using Xcode 6
    self.contentView.frame = self.bounds;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //    Fix : For right aligned view with autoresizing
    UIView *backV = [[self.contentView subviews] objectAtIndex:0];
    backV.frame = [UpdateFrame setSizeForView:backV usingSize:self.bounds.size];
}

- (void) itemSelected : (BOOL) selected  {
    self.iconB.selected = selected;
        
    self.mainV.backgroundColor = selected ? [UIColor brandOrangeDisplayColor] : [UIColor greyBackgroundDisplayColor];
    self.titleL.textColor = selected ? [UIColor blackColor]  : [UIColor greyTextDisplayColor];
}

- (void) configureDataForCell : (NSDictionary *) item
{
    self.titleL.text = [ReadData stringValueFromDictionary:item forKey:KEY_COUPON_CODE];
    
    self.couponSelectedB.selected = NO;
    self.iconB.selected = self.couponSelectedB.selected;
}

@end
