//
//  ChooseCouponCell.h
//  Ziva
//
//  Created by Bharat on 13/10/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseCouponCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *mainV;

@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UIButton *iconB;

@property (weak, nonatomic) IBOutlet UIButton *couponSelectedB;

- (void) configureDataForCell : (NSDictionary *) item ;

- (void) itemSelected : (BOOL) selected ;


@end
