//
//  ChooseBankViewController.m
//  Ziva
//
//  Created by Bharat on 12/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "NetBankingCell.h"
#import "ChooseBankViewController.h"
#import "GatewayPaymentViewController.h"

@interface ChooseBankViewController ()
{
    __weak IBOutlet UITableView *listTblV;
    NSArray *list;
    NSInteger selectedIndex;
}
@end

@implementation ChooseBankViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    selectedIndex = -1;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public Methods

- (void) resetSelection
{
    selectedIndex = -1;
    NSIndexPath *indexPath = listTblV.indexPathForSelectedRow;
    if (indexPath) {
        NetBankingCell *cell = (NetBankingCell *)[listTblV cellForRowAtIndexPath:indexPath];
        [cell toggleSelectOption:NO];
        [listTblV deselectRowAtIndexPath:indexPath animated:NO];
    }
}

- (void) loadlistForOtherBanks : (NSArray *) otherBanks
{
    list = otherBanks;
    [listTblV reloadData];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [list count] ;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NetBankingCell *cell = (NetBankingCell *)[tableView dequeueReusableCellWithIdentifier:@"netbankingcell" forIndexPath:indexPath];
    NSDictionary *item = list[indexPath.row];
    [cell configureDataForCell :item];
    [cell toggleSelectOption:(indexPath.row == selectedIndex)];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NetBankingCell *cell = (NetBankingCell *)[tableView cellForRowAtIndexPath:indexPath];
    [cell toggleSelectOption:YES];
    selectedIndex = indexPath.row;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NetBankingCell *cell = (NetBankingCell *)[tableView cellForRowAtIndexPath:indexPath];
    [cell toggleSelectOption:NO];
}


#pragma mark - Events

- (IBAction)backBPressed:(UIButton *)sender
{
    GatewayPaymentViewController *parentVC = (GatewayPaymentViewController *)self.parentViewController;
    [parentVC dismissBankSelector];
}

- (IBAction)doneBPressed:(UIButton *)sender
{
    GatewayPaymentViewController *parentVC = (GatewayPaymentViewController *)self.parentViewController;
    [parentVC otherBankSelected:selectedIndex];
}

@end
