//
//  NetBankingCell.h
//  Ziva
//
//  Created by Bharat on 12/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NetBankingCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *mainV;

@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UIImageView *selectedImgV;

- (void) configureDataForCell : (NSDictionary *) item;
- (void) toggleSelectOption : (BOOL) selected;
@end
