//
//  PaymentSummaryViewController.h
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PaymentSummaryDelegate <NSObject>

@optional

- (void) oncancel_Summary;

- (void) onconfirm_Booking ;

- (void) ondisplay_Coupon;

@end


@interface PaymentSummaryViewController : UIViewController

@property (nonatomic, weak) id<PaymentSummaryDelegate> delegate;


- (void) applyCoupon :(NSString *) couponcode;

@end
