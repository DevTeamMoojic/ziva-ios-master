//
//  PaymentSummaryViewController.m
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "OrderLineItemsViewController.h"
#import "PaymentSummaryViewController.h"

#define KEY_PMT_SUMMARY_ZIVA_PRICE @"ZivaPrice"
#define KEY_PMT_SUMMARY_ACTUAL_PRICE @"ActualPrice"
#define KEY_PMT_SUMMARY_SAVINGS @"Savings"
#define KEY_PMT_SUMMARY_DISCOUNT @"Discount"
#define KEY_PMT_SUMMARY_CREDITSAVAILABLE @"CreditsAvailable"
#define KEY_PMT_SUMMARY_CREDITSAPPLIED @"CreditsApplied"
#define KEY_PMT_SUMMARY_SURCHARGES @"Surcharges"
#define KEY_PMT_SUMMARY_TAX @"Tax"
#define KEY_PMT_SUMMARY_TOTAL @"Total"
#define KEY_PMT_SUMMARY_PAYABLE_AMT @"PayableAmount"
#define KEY_PMT_SUMMARY_CODPAYABLE_AMT @"CODPayableAmount"

@interface PaymentSummaryViewController ()<MyCartDelegate,MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
    
    __weak IBOutlet UIView *optionsV;    
    __weak IBOutlet UILabel *screenTitleL;
    
    __weak IBOutlet UIView *containerView;
    __weak IBOutlet UIView *topImageV;
    __weak IBOutlet UIImageView *mainImgV;
    
    __weak IBOutlet UIScrollView *scrollviewOnTop;
    
    __weak IBOutlet UIView *cartTotalV;
    __weak IBOutlet UILabel *cartItemsL;
    __weak IBOutlet UILabel *cartAmountL;
    __weak IBOutlet UIScrollView *tabcontentscrollView;
    
    __weak IBOutlet UIScrollView *mainscrollView;
    
    __weak IBOutlet UIView *productsTotalV;
    __weak IBOutlet UILabel *productsTotalL;
    __weak IBOutlet UIButton *expandcoolapseB;
    
    __weak IBOutlet UIView *lineItemsCV;
    __weak OrderLineItemsViewController *lineItemsVC;

    __weak IBOutlet UIView *discountsV;
    __weak IBOutlet UILabel *discountsL;

    __weak IBOutlet UIView *walletcashV;
    __weak IBOutlet UILabel *walletcashL;
    __weak IBOutlet UIButton *applycreditsB;    

    __weak IBOutlet UIView *taxV;
    __weak IBOutlet UILabel *taxL;
    
    __weak IBOutlet UIView *totalAmountV;
    __weak IBOutlet UILabel *totalAmountL;

    __weak IBOutlet UIView *showCouponsV;
    
    __weak IBOutlet UIView *addressV;
    __weak IBOutlet UILabel *storenameL;
    __weak IBOutlet UILabel *addressL;

    __weak IBOutlet UIView *scheduleV;
    __weak IBOutlet UILabel *scheduleDateL;
    __weak IBOutlet UILabel *scheduleTimeL;

    __weak IBOutlet UIView *footerV;
    
    NSDateFormatter *formatter;
    
    CGRect cachedImageViewSize;
    CGFloat yscrollOffset;
    
    CGFloat height_Of_LineItemsCV;

    BOOL isContentSizeUpdated;
    BOOL isUpdating;
    
    NSUInteger pageIndex;
    NSInteger pageWidth;
    NSInteger pageHeight;
    
    CGFloat payableAmount;
    
    AppDelegate *appD;
    MyCartController *cartC;
    
    CGFloat contentHeight;
    
    BOOL isBackFromOtherView;
}
@end

@implementation PaymentSummaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    cartC = appD.sessionDelegate.mycartC;
    [cartC addDelegate:self];
    
    formatter = [[NSDateFormatter alloc] init];
    
    [self setupLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if(!isBackFromOtherView){
        
        [cartC removeDelegate:self];
    }
}

#pragma mark - Public Methods

- (void) applyCoupon :(NSString *) couponcode
{
    cartC.promoCode = couponcode;
    [self generatePaymentSummary];
}


#pragma mark - Layout

-(void) resetLayout;
{
    mainscrollView.contentOffset = CGPointZero;
    contentHeight = CGRectGetHeight(self.view.bounds);
    
}
- (void) setupLayout
{
    [cartC getCoupons];
    
    lineItemsCV.frame = [UpdateFrame setSizeForView:lineItemsCV usingHeight:0];
    [lineItemsVC loadData:cartC.cartItems withHeading:cartC.cartType];
    
    height_Of_LineItemsCV = 30 + [cartC.cartItems count]*46.0f;
    
    applycreditsB.selected = YES;
    cartC.promoCode = @"";
    cartC.isCreditsApplied = applycreditsB.selected;
    
    pageWidth = CGRectGetWidth(self.view.bounds);
    
    [self.view addGestureRecognizer:scrollviewOnTop.panGestureRecognizer];
    [mainscrollView removeGestureRecognizer:mainscrollView.panGestureRecognizer];
    
    //Position elements
    {
        topImageV.frame = [UpdateFrame setSizeForView:topImageV usingHeight:[ResizeToFitContent getHeightForParallaxImage:self.view.frame]];
        
        yscrollOffset = CGRectGetHeight(topImageV.frame) - IMAGE_SCROLL_OFFSET_Y - CGRectGetHeight(optionsV.frame);
        mainImgV.frame = topImageV.frame ;
        
        containerView.frame = [UpdateFrame setSizeForView:containerView usingHeight:(CGRectGetHeight(self.view.bounds) + yscrollOffset)];
        
        cartTotalV.frame = [UpdateFrame setPositionForView:cartTotalV usingPositionY: CGRectGetMaxY(topImageV.frame)];
        
        tabcontentscrollView.frame= [UpdateFrame setPositionForView:tabcontentscrollView usingPositionY: CGRectGetMaxY(cartTotalV.frame)];
        tabcontentscrollView.frame = [UpdateFrame setSizeForView:tabcontentscrollView usingHeight: CGRectGetHeight(containerView.frame) - CGRectGetMaxY(cartTotalV.frame)];
    }
    
    
    CGPoint scrollviewOrigin = scrollviewOnTop.frame.origin;
    scrollviewOnTop.scrollIndicatorInsets = UIEdgeInsetsMake(-scrollviewOrigin.y, 0, scrollviewOrigin.y, scrollviewOrigin.x);
    
    [self loadData];
}

#pragma mark - Methods

- (void) generatePaymentSummary
{
    [self hudShowWithMessage:@"Loading"];
    [cartC getPaymentSummary];
    
}


- (void) loadData
{
    cartItemsL.text = [cartC display_CartTypeInfo];
    cartAmountL.text = [cartC display_TotalValue];
    
    if ([cartC.cartType isEqualToString:CART_TYPE_SERVICES] || [cartC.cartType isEqualToString:CART_TYPE_LOOKS] ){

        [formatter setDateFormat:FORMAT_DATE];
        NSDate *scheduleDate = [formatter dateFromString:cartC.bookDate];
        [formatter setDateFormat:@"d-M-yyyy, EEEE"];
        scheduleDateL.text = [formatter stringFromDate:scheduleDate];
        
        [formatter setDateFormat:FORMAT_TIME_24];
        NSDate *scheduleTime = [formatter dateFromString:cartC.bookTimeSlot];
        [formatter setDateFormat:FORMAT_TIME_12];
        scheduleTimeL.text = [formatter stringFromDate:scheduleTime];

        storenameL.text = cartC.storeName;
        addressL.text = cartC.storeAddress;
    }
    else{
        addressV.frame = [UpdateFrame setSizeForView:addressV usingHeight:0];
        scheduleV.frame = [UpdateFrame setSizeForView:scheduleV usingHeight:0];
    }
    
    scheduleV.frame = [UpdateFrame setPositionForView:scheduleV usingPositionY:CGRectGetMaxY(addressV.frame) + 10];

    [self generatePaymentSummary];
    contentHeight = CGRectGetMaxY(scheduleV.frame) + CGRectGetHeight(footerV.frame);
    if (contentHeight <= CGRectGetHeight(mainscrollView.bounds))
    {
        contentHeight = CGRectGetHeight(mainscrollView.bounds);
        mainscrollView.scrollEnabled = NO;
    }
    
    [self tabChanged];
}

- (void) tabChanged
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        CGPoint offset = [self getScrollContentOffset];
        offset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:offset animated:NO];
    }
    isContentSizeUpdated = NO;
}

#pragma mark - Cart Delegate Methods

- (void) onResponseError{
    [HUD hide:YES];
}

- (void) onPaymentSummaryGenerated{
    NSDictionary *item = [cartC fetchPaymentSummaryInfo];
    productsTotalL.text = [ReadData amountInRsFromDictionary:item forKey:KEY_PMT_SUMMARY_ZIVA_PRICE];
    discountsL.text = [ReadData amountInRsFromDictionary:item forKey:KEY_PMT_SUMMARY_DISCOUNT];
    walletcashL.text = [ReadData amountInRsFromDictionary:item forKey:KEY_PMT_SUMMARY_CREDITSAPPLIED];
    applycreditsB.hidden = !([ReadData floatValueFromDictionary:item forKey: KEY_PMT_SUMMARY_CREDITSAPPLIED] > 0.0f);
    taxL.text = [ReadData amountInRsFromDictionary:item forKey:KEY_PMT_SUMMARY_TAX];
    payableAmount = [ReadData floatValueFromDictionary:item forKey:KEY_PMT_SUMMARY_PAYABLE_AMT];
    totalAmountL.text= [ReadData amountInRsFromDictionary:item forKey:KEY_PMT_SUMMARY_PAYABLE_AMT];
    [HUD hide:YES];
}


#pragma mark - HUD & Delegate methods

- (void)hudShowWithMessage:(NSString *)message
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    HUD.dimBackground = YES;
    HUD.labelText = message;
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
    
    [HUD show:YES];
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
    HUD = nil;
}

#pragma mark - Expand on Scroll animation

- (CGSize) setContentSize
{
    CGFloat height = CGRectGetHeight(mainImgV.frame) + CGRectGetHeight(cartTotalV.frame) +  contentHeight;
    return CGSizeMake(pageWidth, height);
}

- (void) setScrollContentOffset: (CGPoint) offset
{
    mainscrollView.contentOffset = offset;
}

- (CGPoint) getScrollContentOffset
{
    return [mainscrollView contentOffset];
}

- (void) resetOffsetAllTabs
{
    CGPoint offset = CGPointZero;
    [self setScrollContentOffset : offset];
}

-(void) updateContentSize : (CGPoint) contentOffset;
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        contentOffset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    }
    [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    
    isContentSizeUpdated = NO;
}

#pragma mark - Scroll Events

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == scrollviewOnTop)
    {
        {
            if (isContentSizeUpdated)  return;
            
            CGRect scrolledBoundsForContainerView = containerView.bounds;
            if (scrollView.contentOffset.y <= yscrollOffset)
            {
                
                scrolledBoundsForContainerView.origin.y = scrollView.contentOffset.y ;
                containerView.bounds = scrolledBoundsForContainerView;
                
                //Reset offset for all tabs
                if (scrollView.contentOffset.y <= 0) [self resetOffsetAllTabs];
                
                
                CGFloat y = -scrollView.contentOffset.y;
                CGFloat alphaLevel = 1;
                CGFloat BLUR_MAX_Y = IMAGE_SCROLL_OFFSET_Y + CGRectGetHeight(topImageV.frame) - CGRectGetMinY(optionsV.frame);
                if (fabs(y) < BLUR_MAX_Y)
                {
                    alphaLevel = (BLUR_MAX_Y - fabs(y))/(BLUR_MAX_Y);
                }
                else
                {
                    alphaLevel = 0;
                }
                
                //[screenTitleL setAlpha:alphaLevel];
                if (y > 0)
                {
                    mainImgV.frame = CGRectInset(cachedImageViewSize, 0, -y/2);
                    mainImgV.center = CGPointMake(mainImgV.center.x, mainImgV.center.y + y/2);
                }
                else
                {
                    mainImgV.frame = [UpdateFrame setPositionForView:mainImgV usingPositionY:y];
                }
                return;
            }
            
            scrolledBoundsForContainerView.origin.y = yscrollOffset ;
            containerView.bounds = scrolledBoundsForContainerView;
            
            [self setScrollContentOffset:CGPointMake(0, scrollView.contentOffset.y - yscrollOffset)  ];
        }
    }
}

#pragma mark - Translate View

- (void)translateView:(UIView *)view
{
    // Shrink View
    view.frame = [UpdateFrame setSizeForView:view usingHeight:0];
    view.alpha = 0;
    
    // DONT CHANGE THE ORDER
    
    // 1 - Dismiss keyboard
    [UIResponder dismissKeyboard];
    
    // 2 - Disable User Interaction
    [self.view setUserInteractionEnabled:NO];
    
    // 3 - Animate
    [UIView animateWithDuration:TIME_VIEW_TOGGLE
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         // Animate Grow and Fade In
         view.frame = [UpdateFrame setSizeForView:view usingHeight:height_Of_LineItemsCV];
         view.alpha = 1;
         discountsV.frame = [UpdateFrame setPositionForView:discountsV usingOffsetY:height_Of_LineItemsCV];
         walletcashV.frame = [UpdateFrame setPositionForView:walletcashV usingOffsetY:height_Of_LineItemsCV];
         taxV.frame = [UpdateFrame setPositionForView:taxV usingOffsetY:height_Of_LineItemsCV];
         totalAmountV.frame = [UpdateFrame setPositionForView:totalAmountV usingOffsetY:height_Of_LineItemsCV];
         showCouponsV.frame = [UpdateFrame setPositionForView:showCouponsV usingOffsetY:height_Of_LineItemsCV];
         addressV.frame = [UpdateFrame setPositionForView:addressV usingOffsetY:height_Of_LineItemsCV];
         scheduleV.frame = [UpdateFrame setPositionForView:scheduleV usingOffsetY:height_Of_LineItemsCV];
         
     }
                     completion:^(BOOL finished)
     {
         // Enable User Interaction
         [self.view setUserInteractionEnabled:YES];
     }];
}

- (void)resetTranslateView:(UIView *)view
{
    // DONT CHANGE THE ORDER
    
    // 1 - Dismiss keyboard
    [UIResponder dismissKeyboard];
    
    
    // 2 - Disable User Interaction
    [self.view setUserInteractionEnabled:NO];
    
    // 3 - Animate
    [UIView animateWithDuration:TIME_VIEW_TOGGLE
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         // Animate Shrink & Fade Out
         view.frame = [UpdateFrame setSizeForView:view usingHeight:0];;
         view.alpha = 0;
         discountsV.frame = [UpdateFrame setPositionForView:discountsV usingOffsetY:-height_Of_LineItemsCV];
         walletcashV.frame = [UpdateFrame setPositionForView:walletcashV usingOffsetY:-height_Of_LineItemsCV];
         taxV.frame = [UpdateFrame setPositionForView:taxV usingOffsetY:-height_Of_LineItemsCV];
         totalAmountV.frame = [UpdateFrame setPositionForView:totalAmountV usingOffsetY:-height_Of_LineItemsCV];
         showCouponsV.frame = [UpdateFrame setPositionForView:showCouponsV usingOffsetY:-height_Of_LineItemsCV];
         addressV.frame = [UpdateFrame setPositionForView:addressV usingOffsetY:-height_Of_LineItemsCV];
         scheduleV.frame = [UpdateFrame setPositionForView:scheduleV usingOffsetY:-height_Of_LineItemsCV];
         
     }
                     completion:^(BOOL finished)
     {
         
         // Send Views to Back
         [self.view sendSubviewToBack:view];
         
         // Enable User Interaction
         [self.view setUserInteractionEnabled:YES];
     }];
}

#pragma mark - Events

- (IBAction)sectionExpandCollapsedBPressed:(UIButton *)sender
{
    expandcoolapseB.selected = !expandcoolapseB.selected;
    if(expandcoolapseB.selected){
        [self translateView:lineItemsCV];
    }
    else{
        [self resetTranslateView:lineItemsCV];
    }
}

- (IBAction)applyCreditsBPressed:(UIButton *)sender
{
    sender.selected = !sender.selected;
    cartC.isCreditsApplied = sender.selected;
    [self generatePaymentSummary];
}

- (IBAction)showCouponsBPressed:(UIButton *)sender
{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(ondisplay_Coupon)])
    {
        [self.delegate ondisplay_Coupon] ;
    }
    
}

- (IBAction)backBPressed:(UIButton *)sender{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(oncancel_Summary)])
    {
        [self.delegate oncancel_Summary] ;
    }
}

- (IBAction)confirmBPressed:(UIButton *)sender{
    cartC.payableAmount = payableAmount;
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(onconfirm_Booking)])
    {
        [self.delegate onconfirm_Booking] ;
    }
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"embed_paymentsummary_lineitems"]){
        lineItemsVC = (OrderLineItemsViewController *)segue.destinationViewController;
    }
}


@end
