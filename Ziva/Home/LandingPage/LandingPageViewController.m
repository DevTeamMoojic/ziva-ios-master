//
//  LandingPageViewController.m
//  Ziva
//
//  Created by Bharat on 30/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "HomeViewController.h"
#import "InappBrowserViewController.h"
#import "WalletViewController.h"
#import "BlogsViewController.h"
#import "OffersViewController.h"
#import "LandingPageViewController.h"

@interface LandingPageViewController ()
{
    __weak IBOutlet UIScrollView *mainscrollView;
    
    __weak IBOutlet UIView *backgroundImgV;
    __weak IBOutlet UIView *top_offersCV;
    __weak IBOutlet UIView *walletCV;
    
    __weak IBOutlet UIView *first_otherOptionsCV;
    __weak IBOutlet UILabel *first_otherOption_1L;
    __weak IBOutlet UILabel *first_otherOption_2L;
    
    __weak IBOutlet UIView *middle_offersCV;
    
    __weak IBOutlet UIView *second_otherOptionsCV;
    __weak IBOutlet UILabel *second_otherOption_1L;
    __weak IBOutlet UILabel *second_otherOption_2L;
    
    __weak IBOutlet UIView *blogCV;
    
    __weak OffersViewController *top_offersVC;
    __weak OffersViewController *middle_offersVC;
    __weak BlogsViewController *blogsVC;
    __weak WalletViewController *walletVC;
    
    CGFloat contentHeight;
    
    AppDelegate *appD;    
}
@end

@implementation LandingPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [mainscrollView removeGestureRecognizer:mainscrollView.panGestureRecognizer];
    [self resetLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public Methods

- (void) updateLayout {
    [self setupLayout];
}

- (void) launchDetailView{
    HomeViewController *homeVc = (HomeViewController *) self.parentViewController;
    [homeVc launchDetailView];
}

#pragma mark - Methods

- (CGSize) getSizeForOfferCell_Top
{
    CGFloat designWidth = 320;
    CGFloat designHeight = 160;
    
    CGFloat translatedWidth = CGRectGetWidth(self.view.bounds);
    CGFloat translatedHeight =(int)ceil(translatedWidth * designHeight/designWidth);
    
    return CGSizeMake(translatedWidth, translatedHeight);
}

- (CGSize) getSizeForOfferCell_Middle
{
    CGFloat designWidth = 300;
    CGFloat designHeight = 150;
    
    CGFloat translatedWidth = (CGRectGetWidth(self.view.bounds) - 20);
    CGFloat translatedHeight =(int)ceil(translatedWidth * designHeight/designWidth);
    
    return CGSizeMake(translatedWidth, translatedHeight);
}

- (CGSize) getSizeForBlogCell
{
    CGFloat designWidth = 260;
    CGFloat designHeight = 130;
    
    CGFloat translatedWidth = (int)ceil((CGRectGetWidth(self.view.bounds) - 20)*designWidth/300);
    CGFloat translatedHeight =(int)ceil(translatedWidth * designHeight/designWidth);
    
    return CGSizeMake(translatedWidth, translatedHeight + 17);
}

-(void) resetLayout
{
    CGFloat tmpHeight = 15;
    
    tmpHeight =[ResizeToFitContent getHeightForText:first_otherOption_1L.text usingFontType:FONT_REGULAR FontOfSize:14 andMaxWidth:CGRectGetWidth(first_otherOption_1L.frame)];
    first_otherOption_1L.frame = [UpdateFrame setSizeForView:first_otherOption_1L usingHeight:tmpHeight];

    tmpHeight =[ResizeToFitContent getHeightForText:first_otherOption_2L.text usingFontType:FONT_REGULAR FontOfSize:14 andMaxWidth:CGRectGetWidth(first_otherOption_2L.frame)];
    first_otherOption_2L.frame = [UpdateFrame setSizeForView:first_otherOption_2L usingHeight:tmpHeight];

    tmpHeight =[ResizeToFitContent getHeightForText:second_otherOption_1L.text usingFontType:FONT_REGULAR FontOfSize:14 andMaxWidth:CGRectGetWidth(second_otherOption_1L.frame)];
    second_otherOption_1L.frame = [UpdateFrame setSizeForView:second_otherOption_1L usingHeight:tmpHeight];

    tmpHeight =[ResizeToFitContent getHeightForText:second_otherOption_2L.text usingFontType:FONT_REGULAR FontOfSize:14 andMaxWidth:CGRectGetWidth(second_otherOption_2L.frame)];
    second_otherOption_2L.frame = [UpdateFrame setSizeForView:second_otherOption_2L usingHeight:tmpHeight];
    
    mainscrollView.contentOffset = CGPointZero;
    contentHeight = CGRectGetHeight(self.view.bounds);
}

- (void) setupLayout
{    
    CGSize cellcontentSz;
    cellcontentSz = [self getSizeForOfferCell_Top];
    top_offersCV.frame = [UpdateFrame setSizeForView:top_offersCV usingSize:cellcontentSz];
    [top_offersVC setCellContentSize: cellcontentSz andPosition:BANNER_POSITION_TOP];
    
    cellcontentSz = [self getSizeForOfferCell_Middle];
    middle_offersCV.frame = [UpdateFrame setSizeForView:middle_offersCV  usingWidth:(cellcontentSz.width + 20) andHeight:cellcontentSz.height];
    [middle_offersVC setCellContentSize: cellcontentSz andPosition:BANNER_POSITION_MIDDLE];

    cellcontentSz = [self getSizeForBlogCell];
    blogCV.frame = [UpdateFrame setSizeForView:blogCV  usingHeight:cellcontentSz.height + 58];
    [blogsVC setCellContentSize: cellcontentSz];

    walletCV.frame = [UpdateFrame setPositionForView:walletCV usingPositionY:CGRectGetMaxY(top_offersCV.frame)];
    first_otherOptionsCV.frame = [UpdateFrame setPositionForView:first_otherOptionsCV usingPositionY:CGRectGetMaxY(walletCV.frame)];
    middle_offersCV.frame = [UpdateFrame setPositionForView:middle_offersCV usingPositionY:CGRectGetMaxY(first_otherOptionsCV.frame)];
    second_otherOptionsCV.frame = [UpdateFrame setPositionForView:second_otherOptionsCV usingPositionY:CGRectGetMaxY(middle_offersCV.frame)];
    blogCV.frame = [UpdateFrame setPositionForView:blogCV usingPositionY:CGRectGetMaxY(second_otherOptionsCV.frame)];
    
    contentHeight = CGRectGetMaxY(blogCV.frame);    
    backgroundImgV.frame = [UpdateFrame setSizeForView:backgroundImgV usingHeight:contentHeight];
    
    if (contentHeight <= CGRectGetHeight(mainscrollView.bounds))
    {
        contentHeight = CGRectGetHeight(mainscrollView.bounds);
        mainscrollView.scrollEnabled = NO;
    }
    [mainscrollView setContentSize:CGSizeMake(CGRectGetWidth(mainscrollView.frame), contentHeight)];
    
    [self callAPI];
}

- (void) callAPI
{
    [top_offersVC reloadList];
    [walletVC reloadList];
    [middle_offersVC reloadList];
    [blogsVC reloadList];
}

#pragma mark - Scroll content

- (CGFloat) getContentHeight;
{
    return contentHeight;
}

- (void) scrollContent : (CGPoint) offset
{
    mainscrollView.contentOffset = offset;
}

- (CGPoint) getScrollContent
{
    return mainscrollView.contentOffset;
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"embed_offers_top"])
    {
        top_offersVC = (OffersViewController *) segue.destinationViewController;

    }
    else if ([segue.identifier isEqualToString:@"embed_offers_middle"])
    {
        middle_offersVC = (OffersViewController *) segue.destinationViewController;
        
    }
    else if ([segue.identifier isEqualToString:@"embed_blogs"])
    {
        blogsVC = (BlogsViewController *) segue.destinationViewController;
        
    }
    else if ([segue.identifier isEqualToString:@"embed_wallets"])
    {
        walletVC = (WalletViewController *) segue.destinationViewController;
    }
}

#pragma mark - Events

- (IBAction)first_otherOption_1BPressed:(UIButton *)sender{
    [self performSegueWithIdentifier:@"showlist_salons" sender:Nil];
    [self launchDetailView];
}

- (IBAction)first_otherOption_2BPressed:(UIButton *)sender{
    [self performSegueWithIdentifier:@"showlist_looks" sender:Nil];
    [self launchDetailView];
}

- (IBAction)second_otherOption_1BPressed:(UIButton *)sender{
    [self performSegueWithIdentifier:@"showlist_stylists" sender:Nil];
    [self launchDetailView];
}

- (IBAction)second_otherOption_2BPressed:(UIButton *)sender{
    [self performSegueWithIdentifier:@"showlist_products" sender:Nil];
    [self launchDetailView];
}


@end
