//
//  WalletViewController.m
//  Ziva
//
//  Created by Bharat on 30/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "WalletViewController.h"

@interface WalletViewController ()<APIDelegate>
{
    __weak IBOutlet UILabel *balanceL;
    __weak IBOutlet UILabel *expiryDateL;
    
    __weak IBOutlet UIActivityIndicatorView *sectionloaderAIV;
    
    AppDelegate *appD;
}
@end

@implementation WalletViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public Methods

- (void) reloadList
{
    if ([InternetCheck isOnline]){
        [sectionloaderAIV startAnimating];
        
        NSMutableArray *replacementArray = [NSMutableArray array];
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject:@"USER_ID" forKey:@"key"];
        [paramDict setObject: [appD getLoggedInUserId] forKey:@"value"];
        
        [replacementArray addObject:paramDict];
        
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_MY_WALLET Param:NULL replacementStrings:replacementArray];
    }
}

#pragma mark - Methods

- (void) processResponse : (NSDictionary *) dict
{
    NSInteger totalValue = 0;
    NSArray *tmpArray = [ReadData arrayFromDictionary:dict forKey:RESPONSEKEY_DATA];
    for(NSDictionary *item in tmpArray){
        totalValue += [ReadData integerValueFromDictionary:item forKey:KEY_AMOUNT];
    }
   
    balanceL.text = [NSString stringWithFormat:@"₹ %ld", (long)totalValue];
    
    [sectionloaderAIV stopAnimating];
}


#pragma mark - API

-(void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName
{
    if(![ReadData isValidResponseData:response])
    {
        {
            NSString *message = [ReadData stringValueFromDictionary:response forKey:RESPONSEKEY_ERRORMESSAGE];
            [appD showAPIErrorWithMessage:message andTitle:ALERT_TITLE_ERROR];
            [self.view setUserInteractionEnabled:YES];
            [sectionloaderAIV stopAnimating];
        }
        return;
    }
    
    if ([apiName isEqualToString:API_GET_MY_WALLET]) {
        [self processResponse:response];
    }
}

@end
