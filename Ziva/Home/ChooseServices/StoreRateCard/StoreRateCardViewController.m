//
//  StoreRateCardViewController.m
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "MySearchBarViewController.h"
#import "GenderSelectorViewController.h"
#import "CartSummaryViewController.h"
#import "StoreCategoryListViewController.h"
#import "StoreRateCardViewController.h"


@interface StoreRateCardViewController ()<APIDelegate,MBProgressHUDDelegate,GenderSelectorDelegate,CartSummaryDelegate,MySearchBarDelegate>
{
    MBProgressHUD *HUD;
    
    NSArray *categoriesArray;
    NSArray *servicesArray;
    NSArray *homesalonservicesArray;
    
    NSMutableArray *list;
    NSMutableArray *viewcontrollers;
    
    __weak IBOutlet UIView *optionsV;
    __weak IBOutlet UILabel *screenTitleL;
    __weak IBOutlet UIButton *searchB;
    
    __weak IBOutlet UIView *containerView;
    __weak IBOutlet UIView *topImageV;
    __weak IBOutlet UIImageView *mainImgV;
    
    __weak IBOutlet UIScrollView *scrollviewOnTop;

    __weak IBOutlet UIView *genderselectorCV;
    __weak GenderSelectorViewController *genderselectorVC;
    
    __weak IBOutlet UIView *serviceCategoriesCV;
    __weak IBOutlet UILabel *noresultsL;
    __weak IBOutlet UIScrollView *tabscrollView;
    __weak IBOutlet UIScrollView *tabcontentscrollView;
    
    __weak IBOutlet UIView *tabSelectorV;
    
    __weak IBOutlet UIButton *dummyButton;
    
    
    __weak IBOutlet UIView *cartsummaryCV;
    __weak CartSummaryViewController *cartsummaryVC;
    
    __weak IBOutlet UIView *searchbarCV;
    __weak MySearchBarViewController *searchbarVC;
    
    __weak IBOutlet UIView *backV;

    NSString *selectedGenderId;
    NSDictionary *selectedStoreDict;
    BOOL isStoresRateCard;
    
    CGRect cachedImageViewSize;
    CGFloat yscrollOffset;
    
    BOOL isBackFromOtherView;
    BOOL isContentSizeUpdated;
    
    BOOL isUpdating;
    BOOL isUpdating_MenuOptions;
    
    NSUInteger pages;
    NSUInteger pageIndex;
    NSInteger pageWidth;
    NSInteger pageHeight;
    
    CGFloat offset_menuOptions_x;
    
    NSInteger apiCtr;
    
    AppDelegate *appD;

    MyCartController *cartC;
    
    NSString *deliveryType;
}
@end

@implementation StoreRateCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    cartC = appD.sessionDelegate.mycartC;
    [cartC initForServicesWithDeliveryType:deliveryType];
    [self setupLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!isBackFromOtherView)
        cachedImageViewSize = mainImgV.frame;
    else
        isBackFromOtherView = NO;
    
    [self reloadData];
    [self updateQuantiyHeading];
}

#pragma mark - Public Methods

- (void) loadForSalonStore : (NSDictionary *)dict Gender:(NSString *) genderId  andDeliveryType : (NSString *) dlvryType;
{
    selectedGenderId = genderId;
    deliveryType = dlvryType;
    isStoresRateCard = YES;
    selectedStoreDict = dict;
    [self loadRateCard];
}


- (void) loadRateCard{
    if ([InternetCheck isOnline])
    {
        [self hudShowWithMessage:@"Loading"];
        apiCtr = 2;
        
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_SERVICES_CATEGORIES Param:nil];
        
        NSMutableArray *replacementArray = [NSMutableArray array];
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject:@"RECORD_ID" forKey:@"key"];
        [paramDict setObject:[ReadData recordIdFromDictionary:selectedStoreDict forKey:KEY_ID] forKey:@"value"];
        [replacementArray addObject:paramDict];
        
        paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject:@"GENDER_ID" forKey:@"key"];
        [paramDict setObject:selectedGenderId forKey:@"value"];
        [replacementArray addObject:paramDict];
        
        
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_STORE_RATE_CARD Param:nil replacementStrings:replacementArray];
        
        noresultsL.hidden = YES;
        tabcontentscrollView.hidden = YES;
        tabscrollView.hidden = YES;        
    }
}

- (void) addItemToCart : (NSDictionary *) item{
    [cartC addServices:item];
    [self updateQuantiyHeading];
}

- (void) removeItemFromCart : (NSDictionary *) item{
    [cartC lessServices:item];
    [self updateQuantiyHeading];
}

- (void) reloadData
{
    for(NSInteger ctr = 0 ; ctr < [viewcontrollers count]; ctr++){
        [[self getCategoryListVC:ctr] reloadData];
    }
}

#pragma mark - Methods

- (CGFloat) calculateImageHeight
{
    CGFloat designScreenHeight = 667;
    CGFloat designHeight = 230;
    
    CGFloat translatedHeight = ceil(designHeight * CGRectGetHeight(self.view.frame)/designScreenHeight);
    
    return (int)translatedHeight;
}

- (StoreCategoryListViewController *) getCategoryListVC :(NSInteger) index{
    StoreCategoryListViewController *obj  = (StoreCategoryListViewController *)[viewcontrollers objectAtIndex:index];
    return obj;
}

- (void) setupLayout
{
    list = [NSMutableArray array];
    viewcontrollers = [NSMutableArray array];
    
    [genderselectorVC setGender:selectedGenderId];
    
    [cartsummaryVC setActionOnCart:CART_ACTION_PROCEED];
    [self updateQuantiyHeading];
    
    
    pageWidth = CGRectGetWidth(self.view.bounds);
    
    [self.view addGestureRecognizer:scrollviewOnTop.panGestureRecognizer];
    
    //Position elements
    {
        
        topImageV.frame = [UpdateFrame setSizeForView:topImageV usingHeight:[ResizeToFitContent getHeightForParallaxImage:self.view.frame]];
        
        yscrollOffset = CGRectGetHeight(topImageV.frame) - IMAGE_SCROLL_OFFSET_Y - CGRectGetHeight(optionsV.frame);
        mainImgV.frame = topImageV.frame ;
        
        containerView.frame = [UpdateFrame setSizeForView:containerView usingHeight:(CGRectGetHeight(self.view.bounds) + yscrollOffset)];
        
        genderselectorCV.frame= [UpdateFrame setPositionForView:genderselectorCV usingPositionY:CGRectGetMaxY(topImageV.frame)];
        serviceCategoriesCV.frame= [UpdateFrame setPositionForView:serviceCategoriesCV usingPositionY:CGRectGetMaxY(genderselectorCV.frame)];
        serviceCategoriesCV.frame = [UpdateFrame setSizeForView:serviceCategoriesCV usingHeight: CGRectGetHeight(containerView.frame) - CGRectGetMaxY(genderselectorCV.frame)];
        
        
        tabscrollView.frame = [UpdateFrame setPositionForView:tabscrollView usingPositionY:0];
        
        tabcontentscrollView.frame= [UpdateFrame setPositionForView:tabcontentscrollView usingPositionY: CGRectGetMaxY(tabscrollView.frame)];
        tabcontentscrollView.frame = [UpdateFrame setSizeForView:tabcontentscrollView usingHeight: CGRectGetHeight(serviceCategoriesCV.frame) - CGRectGetMaxY(tabscrollView.frame)];
    }
    
    CGPoint scrollviewOrigin = scrollviewOnTop.frame.origin;
    scrollviewOnTop.scrollIndicatorInsets = UIEdgeInsetsMake(-scrollviewOrigin.y, 0, scrollviewOrigin.y, scrollviewOrigin.x);
    
    searchbarCV.frame = [UpdateFrame setPositionForView:searchbarCV usingOffsetX:pageWidth];
}


- (void) processResponse_Categories : (NSDictionary *)dict
{
    categoriesArray = [ReadData arrayFromDictionary:dict forKey:RESPONSEKEY_DATA];
}

- (void) processResponse_Services : (NSDictionary *)dict
{
    servicesArray = [ReadData arrayFromDictionary:dict forKey:RESPONSEKEY_DATA];
}

- (void) processResponse_Services_HomeSalon : (NSDictionary *)dict
{
    homesalonservicesArray = [ReadData arrayFromDictionary:dict forKey:RESPONSEKEY_DATA];
}

- (void) generateServicesCard{
    if([list count] > 0 ) [list removeAllObjects];
    if([servicesArray count] == 0){
        noresultsL.hidden = NO;
        tabcontentscrollView.hidden = YES;
        tabscrollView.hidden = YES;
    }
    else{
        for(NSDictionary *category in categoriesArray){
            NSString *categoryId = [ReadData recordIdFromDictionary:category forKey:KEY_ID];
            NSMutableArray *tmpArray = [NSMutableArray array];
            {
                for(NSDictionary *service in servicesArray){
                    NSString *topologyId = [ReadData recordIdFromDictionary:service forKey:KEY_SERVICE_TOPOLOGY_ID];
                    if([topologyId isEqualToString:categoryId]){
                        [tmpArray addObject:service];
                    }
                }
            }
            if(!isStoresRateCard)
            {
                NSMutableArray *tmpArray = [NSMutableArray array];
                for(NSDictionary *service in homesalonservicesArray){
                    NSString *topologyId = [ReadData recordIdFromDictionary:service forKey:KEY_SERVICE_TOPOLOGY_ID];
                    if([topologyId isEqualToString:categoryId]){
                        [tmpArray addObject:service];
                    }
                }
            }
            if([tmpArray count] > 0){
                NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                [dict setObject:categoryId forKey:KEY_ID];
                [dict setObject:[ReadData stringValueFromDictionary:category forKey:KEY_NAME] forKey:KEY_NAME];
                [dict setObject:tmpArray forKey:KEY_LIST_ARRAY];
                [list addObject:dict];
            }
        }
        noresultsL.hidden = YES;
        tabcontentscrollView.hidden = NO;
        tabscrollView.hidden = NO;
    }
    [self loadsubmenusScrollView];
}

- (void) updateQuantiyHeading
{
    cartsummaryCV.hidden = ([cartC.cartItems count] == 0);
    [cartsummaryVC updateSummary];
}

#pragma mark - Setup Tab Containers

- (NSInteger) calculateWidthForSubMenu : (NSString *) title
{
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, CGRectGetHeight(tabscrollView.bounds))];
    lbl.font = [UIFont semiboldFontOfSize:13];
    lbl.text = title;
    [lbl sizeToFit];
    return (CGRectGetWidth(lbl.frame) + 50);
}

-(void)loadsubmenusScrollView
{
    for (UIView* v in tabscrollView.subviews)
    {
        if(v.tag >= TAG_START_INDEX)
            [v removeFromSuperview];
    }
    
    CGFloat offsetX = 0;
    NSInteger ctr = TAG_START_INDEX;
    pages = [list count];
    for(NSDictionary *item in list)
    {
        NSData *tempArchive = [NSKeyedArchiver archivedDataWithRootObject:dummyButton];
        UIButton *categoryB = (UIButton *)[NSKeyedUnarchiver unarchiveObjectWithData:tempArchive];
        NSString *heading = [ReadData stringValueFromDictionary:item forKey:KEY_NAME];
        [categoryB setTitle:heading forState:UIControlStateNormal];
        categoryB.tag = ctr;
        categoryB.hidden = NO;
        [categoryB addTarget:self action:@selector(optionBPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        categoryB.frame = [UpdateFrame setPositionForView:categoryB usingPositionX:offsetX andPositionY:0];
        categoryB.frame = [UpdateFrame setSizeForView:categoryB usingWidth:[self calculateWidthForSubMenu:heading]];
        
        offsetX+=CGRectGetWidth(categoryB.frame);
        ctr++;
        
        [tabscrollView addSubview:categoryB];
    }
    tabSelectorV.hidden = (pages == 0);
    [tabscrollView bringSubviewToFront:tabSelectorV];
    [tabscrollView setContentSize:CGSizeMake(offsetX, CGRectGetHeight(tabscrollView.frame))];
    
    offset_menuOptions_x = tabscrollView.contentSize.width - CGRectGetWidth(self.view.bounds);
    if (offset_menuOptions_x > 0) {
        tabscrollView.scrollEnabled = YES;
    }
    else{
        tabscrollView.scrollEnabled = NO;
        offset_menuOptions_x = 0;
    }
    
    [self loadcontentScrollView];
    [HUD hide:YES];
}

- (void) loadcontentScrollView
{
    if([viewcontrollers count] > 0)[viewcontrollers removeAllObjects];
    
    [tabcontentscrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    //Submenus tab content scroll view
    isUpdating = YES;
    pageWidth = CGRectGetWidth(tabcontentscrollView.frame);
    tabcontentscrollView.contentSize =
    CGSizeMake(pageWidth * pages , CGRectGetHeight(tabcontentscrollView.frame));
    [tabcontentscrollView setContentOffset:CGPointZero animated:NO];
    
    
    UIViewController *controller;
    for (NSInteger ctr = 0; ctr < pages; ctr++)
    {
        controller = [self.storyboard instantiateViewControllerWithIdentifier:@"categorylistingVC"];
        
        // add the controller's view to the scroll view
        if (controller.view.superview == nil)
        {
            controller.view.frame = [UpdateFrame setPositionForView:tabcontentscrollView usingPositionX:pageWidth * ctr andPositionY:0];
            
            [self addChildViewController:controller];
            [tabcontentscrollView addSubview:controller.view];
            [controller didMoveToParentViewController:self];
        }
        
        [viewcontrollers addObject:controller];
    }
    [self searchServices:@""];
    isUpdating = NO;
    pageIndex = 0;
    if(pages > 0){
        [self tabChanged:pageIndex];
    }
}

- (void) tabChanged:(NSInteger)index
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize : index];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        CGPoint offset = [self getScrollContentOffset:index];
        offset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:offset animated:NO];
    }
    isContentSizeUpdated = NO;
    
    [self setSelectedOption:index];
}

-(void)setSelectedOption:(NSInteger)index
{
    UIButton *button = (UIButton *)[tabscrollView viewWithTag:TAG_START_INDEX+index];
    CGFloat sliderPosition = CGRectGetMinX(button.frame);
    CGFloat width = CGRectGetWidth(button.frame);
    
    
    [UIView animateWithDuration:TIME_VIEW_TOGGLE
                          delay:0 options:UIViewAnimationOptionCurveLinear
                     animations:^(void)
     {
         
         tabSelectorV.frame = [UpdateFrame setPositionForView:tabSelectorV usingPositionX:sliderPosition];
         tabSelectorV.frame = [UpdateFrame setSizeForView:tabSelectorV usingWidth:width];
         
     }
                     completion:^(BOOL finished)
     {
         for (int i = 0; i < pages; i++)
         {
             UIButton *btn = (UIButton *)[tabscrollView viewWithTag:i + TAG_START_INDEX];
             if (i != index  )
             {
                 if (btn.selected) btn.selected = NO;
             }
             else
                 btn.selected = YES;
         }
     }];
}

#pragma mark - Translate View

- (void)translateView:(UIView *)view andGreyView:(BOOL)showGreyView
{
    // Shrink View
    view.frame = [UpdateFrame setSizeForView:view usingHeight:0];
    
    // Ready Grey View
    if (showGreyView)
    {
        backV.alpha = 0;
        [self.view bringSubviewToFront:backV];
    }
    
    // Ready View
    view.alpha = 0;
    [self.view bringSubviewToFront:view];
    
    // DONT CHANGE THE ORDER
    
    // 1 - Dismiss keyboard
    [UIResponder dismissKeyboard];
    
    // 2 - Disable User Interaction
    [self.view setUserInteractionEnabled:NO];
    
    // 3 - Animate
    [UIView animateWithDuration:TIME_VIEW_TOGGLE
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         // Animate Grow and Fade In
         view.frame = [UpdateFrame setSizeForView:view usingHeight:TRANSFORMED_HEIGHT];
         view.alpha = 1;
         
         if (showGreyView)
             backV.alpha = 1;
     }
                     completion:^(BOOL finished)
     {
         // Enable User Interaction
         [self.view setUserInteractionEnabled:YES];
     }];
}

- (void)resetTranslateView:(UIView *)view andGreyView:(BOOL)hideGreyView
{
    // DONT CHANGE THE ORDER
    
    // 1 - Dismiss keyboard
    [UIResponder dismissKeyboard];
    
    
    // 2 - Disable User Interaction
    [self.view setUserInteractionEnabled:NO];
    
    // 3 - Animate
    [UIView animateWithDuration:TIME_VIEW_TOGGLE
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         // Animate Shrink & Fade Out
         view.frame = [UpdateFrame setSizeForView:view usingHeight:0];;
         view.alpha = 0;
         
         if(hideGreyView)
             backV.alpha = 0;
     }
                     completion:^(BOOL finished)
     {
         
         // Send Views to Back
         [self.view sendSubviewToBack:view];
         
         if(hideGreyView)
             [self.view sendSubviewToBack:backV];
         
         // Enable User Interaction
         [self.view setUserInteractionEnabled:YES];
     }];
}

#pragma mark - MySearch Delegate Methods

- (void) searchCancelled
{
    [searchbarVC initSearchBar];
    [self searchServices:@""];
    UIView *view = searchbarCV;
    
    [self.view bringSubviewToFront:view];
    
    // DONT CHANGE THE ORDER
    
    // 1 - Dismiss keyboard
    [UIResponder dismissKeyboard];
    
    // 2 - Disable User Interaction
    [self.view setUserInteractionEnabled:NO];
    
    // 3 - Animate
    [UIView animateWithDuration:TIME_VIEW_TOGGLE
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         // Animate Grow and Fade In
         //view.frame = [UpdateFrame setSizeForView:view usingWidth:0];
         view.frame = [UpdateFrame setPositionForView:view usingPositionX:pageWidth];
         screenTitleL.alpha = 1;
         searchB.alpha = 1;
         view.alpha = 0;
         
     }
                     completion:^(BOOL finished)
     {
         // Enable User Interaction
         [self.view setUserInteractionEnabled:YES];
     }];
}

- (void) searchTextChanged : (NSString *) searchText
{
    [self searchServices:searchText];
}

#pragma mark - Gender Selector & Delegate Methods

- (void) valueChanged{
    [self searchCancelled];
    selectedGenderId = [genderselectorVC getGender];
    [self loadRateCard];
}

#pragma mark - Cart Summary Delegate

- (void) takeActionOnCart{
    [self performSegueWithIdentifier:@"showlocationdate_services" sender:Nil];
}

#pragma mark - HUD & Delegate methods

- (void)hudShowWithMessage:(NSString *)message
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    HUD.dimBackground = YES;
    HUD.labelText = message;
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
    
    [HUD show:YES];
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
    HUD = nil;
}

#pragma mark - Expand on Scroll animation

- (CGSize) setContentSize :(NSInteger)index
{
    CGFloat height = CGRectGetHeight(mainImgV.frame) + CGRectGetHeight(tabscrollView.frame);
    height += [[self getCategoryListVC:index] getContentHeight];
    return CGSizeMake(pageWidth, height);
}

- (void) setScrollContentOffset: (CGPoint) offset forPage:(NSInteger)index
{
    [[self getCategoryListVC:index] scrollContent:offset];
}

- (CGPoint) getScrollContentOffset :(NSInteger)index
{
    CGPoint offset;
    [[self getCategoryListVC:index] getScrollContent];
    return offset;
}

- (void) resetOffsetAllTabs
{
    CGPoint offset = CGPointZero;
    for(NSInteger ctr = 0; ctr < [viewcontrollers count]; ctr++){
        [[self getCategoryListVC:ctr] scrollContent:offset];
    }
}

-(void) updateContentSize : (CGPoint) contentOffset;
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize : pageIndex];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        contentOffset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    }
    [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    
    isContentSizeUpdated = NO;
}

#pragma mark - Scroll Events


- (void) repositionMenuOptionsAfterScrolling
{
    NSUInteger page = 0;
    if (tabscrollView.contentOffset.x >= offset_menuOptions_x) {
        page = pages - 1 ;
    }
    else{
        page = 0;
    }
    
    UIButton *btn = (UIButton *)[tabscrollView viewWithTag:TAG_START_INDEX+page];
    [self optionBPressed:btn];
}

- (void) updateMenuOptionsScroll
{
    if (tabscrollView.scrollEnabled) {
        UIButton *btn = (UIButton *)[tabscrollView viewWithTag:TAG_START_INDEX+pageIndex];
        isUpdating_MenuOptions = YES;
        if (btn.frame.origin.x > offset_menuOptions_x) {
            [tabscrollView setContentOffset:CGPointMake(offset_menuOptions_x, 0) animated:YES];
        }
        else{
            [tabscrollView setContentOffset:CGPointZero animated:YES];
        }
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == scrollviewOnTop)
    {
        {
            if (isContentSizeUpdated)  return;
            
            CGRect scrolledBoundsForContainerView = containerView.bounds;
            if (scrollView.contentOffset.y <= yscrollOffset)
            {
                
                scrolledBoundsForContainerView.origin.y = scrollView.contentOffset.y ;
                containerView.bounds = scrolledBoundsForContainerView;
                
                //Reset offset for all tabs
                if (scrollView.contentOffset.y <= 0) [self resetOffsetAllTabs];
                
                
                CGFloat y = -scrollView.contentOffset.y;
                CGFloat alphaLevel = 1;
                CGFloat BLUR_MAX_Y = IMAGE_SCROLL_OFFSET_Y + CGRectGetHeight(topImageV.frame) - CGRectGetMinY(optionsV.frame);
                if (fabs(y) < BLUR_MAX_Y)
                {
                    alphaLevel = (BLUR_MAX_Y - fabs(y))/(BLUR_MAX_Y);
                }
                else
                {
                    alphaLevel = 0;
                }
                
                //[optionsV setAlpha:alphaLevel];
                if (y > 0)
                {
                    mainImgV.frame = CGRectInset(cachedImageViewSize, 0, -y/2);
                    mainImgV.center = CGPointMake(mainImgV.center.x, mainImgV.center.y + y/2);
                }
                else
                {
                    mainImgV.frame = [UpdateFrame setPositionForView:mainImgV usingPositionY:y];
                }
                return;
            }
            
            scrolledBoundsForContainerView.origin.y = yscrollOffset ;
            containerView.bounds = scrolledBoundsForContainerView;
            
            [self setScrollContentOffset:CGPointMake(0, scrollView.contentOffset.y - yscrollOffset) forPage:pageIndex ];
        }
    }
    else if(scrollView == tabcontentscrollView)
    {
        if (isUpdating) return;
        NSUInteger page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        if (pageIndex != page)
        {
            pageIndex = page;
            [self tabChanged:pageIndex];
            [self updateMenuOptionsScroll];
        }
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if(scrollView == tabcontentscrollView)
    {
        if (isUpdating) isUpdating = NO;
    }
    else if(scrollView == tabscrollView)
    {
        if (isUpdating_MenuOptions) isUpdating_MenuOptions = NO;
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate) {
        if(scrollView == tabscrollView)
        {
            [self repositionMenuOptionsAfterScrolling];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(scrollView == tabscrollView)
    {
        [self repositionMenuOptionsAfterScrolling];
    }
}


#pragma mark - Events

- (IBAction)backBPressed:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)optionBPressed: (UIButton *)sender
{
    NSUInteger page = sender.tag - TAG_START_INDEX;
    if (pageIndex != page)
    {
        isUpdating = YES;
        pageIndex = page;
        [tabcontentscrollView setContentOffset:CGPointMake(pageIndex*pageWidth, 0) animated:YES];
        [self tabChanged:pageIndex];
    }
    [self updateMenuOptionsScroll];    
}

- (IBAction)searchOptionsBPressed:(UIButton *)sender
{
    [searchbarVC initSearchBar];
    UIView *view = searchbarCV;
    
    view.frame = [UpdateFrame setSizeForView:view usingWidth:0];
    
    // Ready View
    view.alpha = 0;
    [self.view bringSubviewToFront:view];
    
    // DONT CHANGE THE ORDER
    
    // 1 - Dismiss keyboard
    [UIResponder dismissKeyboard];
    
    // 2 - Disable User Interaction
    [self.view setUserInteractionEnabled:NO];
    
    // 3 - Animate
    [UIView animateWithDuration:TIME_VIEW_TOGGLE
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         // Animate Grow and Fade In
         view.frame = [UpdateFrame setSizeForView:view usingWidth:pageWidth];
         view.frame = [UpdateFrame setPositionForView:view usingPositionX:0];
         screenTitleL.alpha = 0;
         searchB.alpha = 0;
         view.alpha = 1;
         
     }
                     completion:^(BOOL finished)
     {
         // Enable User Interaction
         [searchbarVC startSearching];
         [self.view setUserInteractionEnabled:YES];
     }];

}

#pragma mark - Search Bar

-(void) searchServices: (NSString *) queryText
{
    for (NSInteger ctr = 0; ctr < pages; ctr++)
    {
        NSDictionary *item =  list[ctr];
        NSArray *serviceCategoryArray ;
        if (queryText.length > 0) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"Service CONTAINS[cd] %@",queryText];
            serviceCategoryArray = [[ReadData arrayFromDictionary:item forKey:KEY_LIST_ARRAY] filteredArrayUsingPredicate:predicate];
        }
        else{
            serviceCategoryArray = [ReadData arrayFromDictionary:item forKey:KEY_LIST_ARRAY] ;
        }
        
        
        [[self getCategoryListVC:ctr] loadData: serviceCategoryArray andIsRateCard:isStoresRateCard];
    }
}


#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //showgenderselector_services
    if([segue.identifier isEqualToString:@"showgenderselector_services"]){
        genderselectorVC = (GenderSelectorViewController *)segue.destinationViewController;
        genderselectorVC.delegate = self;
    }
    else if([segue.identifier isEqualToString:@"showcartsummary_services"]){
        cartsummaryVC = (CartSummaryViewController *)segue.destinationViewController;
        cartsummaryVC.delegate = self;
    }
    else if([segue.identifier isEqualToString:@"showlocationdate_services"]){
    }
    else if([segue.identifier isEqualToString:@"embed_search_services"]){
        searchbarVC = (MySearchBarViewController *)segue.destinationViewController;
        searchbarVC.delegate= self;
    }
    
}

#pragma mark - API

-(void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName
{
    apiCtr --;
    if(![ReadData isValidResponseData:response])
    {
        {
            NSString *message = [ReadData stringValueFromDictionary:response forKey:RESPONSEKEY_ERRORMESSAGE];
            [appD showAPIErrorWithMessage:message andTitle:ALERT_TITLE_ERROR];
            [self.view setUserInteractionEnabled:YES];
            if(apiCtr <= 0) [HUD hide:YES];
        }
        return;
    }
    
    if ([apiName isEqualToString:API_GET_SERVICES_CATEGORIES]) {
        [self processResponse_Categories:response];
    }
    else if ([apiName isEqualToString:API_GET_STORE_RATE_CARD]) {
        [self processResponse_Services:response];
    }
    if(apiCtr <= 0){
        [self generateServicesCard];
    }
}

@end
