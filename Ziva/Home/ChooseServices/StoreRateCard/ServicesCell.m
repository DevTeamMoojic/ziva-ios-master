//
//  ServicesCell.m
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "ServicesCell.h"

@implementation ServicesCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [super awakeFromNib];
    
    // Fix : Autoresizing issues for iOS 7 using Xcode 6
    self.contentView.frame = self.bounds;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //    Fix : For right aligned view with autoresizing
    UIView *backV = [[self.contentView subviews] objectAtIndex:0];
    backV.frame = [UpdateFrame setSizeForView:backV usingSize:self.bounds.size];    
}


- (void) configureDataForCell : (NSDictionary *) item
{
    self.titleL.text = [ReadData stringValueFromDictionary:item forKey:KEY_SERVICE_TITLE];
    self.priceL.text = [ReadData amountInRsFromDictionary:item forKey:KEY_SERVICE_PRICE];

    self.priceActualL.hidden = YES;
    self.priceDiscountL.hidden = YES;
    self.DiscountCouponL.hidden = YES;
    self.DiscountCouponCashBackL.hidden = YES;

    /*
    // For strikeline in cernter on text .
    NSAttributedString * title =
    [[NSAttributedString alloc] initWithString:[ReadData amountInRsFromDictionary:item forKey:KEY_SERVICE_PRICE]
                                    attributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle)}];
    [self.priceActualL setAttributedText:title];
    
    // discount price.
    //self.priceDiscountL.text = [ReadData amountInRsFromDictionary:item forKey:KEY_SERVICE_DISCOUNT_PRICE];
    self.priceDiscountL.text = [ReadData amountInRsFromDictionary:item forKey:KEY_SERVICE_PRICE];

    
    // discount coupan.
    //self.DiscountCouponL.text = [ReadData amountInRsFromDictionary:item forKey:KEY_SERVICE_DISCOUNTCOUPAN_PRICE];
    
    
    NSLog(@"%@",[ReadData stringValueFromDictionary:item forKey:KEY_SERVICE_PRICE]);
    if ([[ReadData stringValueFromDictionary:item forKey:KEY_SERVICE_PRICE] isEqual:[NSNull null]]) {
        
        self.priceActualL.hidden = NO;
        self.priceDiscountL.hidden = NO;
        self.DiscountCouponL.hidden = NO;
        self.DiscountCouponCashBackL.hidden = NO;

    }else{
        //self.priceActualL.hidden = YES;
        //self.priceDiscountL.hidden = YES;
        //self.DiscountCouponL.hidden = YES;
        //self.DiscountCouponCashBackL.hidden = YES;

        self.priceActualL.hidden = NO;
        self.priceDiscountL.hidden = NO;
        self.DiscountCouponL.hidden = NO;
        self.DiscountCouponCashBackL.hidden = NO;
    }

   */
    
    NSArray *cashbacks = [ReadData arrayFromDictionary:item forKey:KEY_CASHBACKS];
    
    self.showOffersB.hidden = ([cashbacks count] < 2);
    self.selectServicesB.hidden = !self.showOffersB.hidden;
    
    self.selectServicesB.selected = NO;
    
}

@end
