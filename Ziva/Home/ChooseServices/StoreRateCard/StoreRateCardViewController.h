//
//  StoreRateCardViewController.h
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoreRateCardViewController : UIViewController

- (void) loadForSalonStore : (NSDictionary *)dict Gender:(NSString *) genderId  andDeliveryType : (NSString *) dlvryType;

- (void) addItemToCart : (NSDictionary *) item;
- (void) removeItemFromCart : (NSDictionary *) item;

-(void) updateContentSize : (CGPoint) contentOffset;

@end
