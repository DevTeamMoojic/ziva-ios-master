//
//  ChooseServicesViewController.m
//  Ziva
//
//  Created by Bharat on 30/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "GenderSelectorViewController.h"
#import "ServicesListingViewController.h"
#import "StoresListingViewController.h"
#import "StoreRateCardViewController.h"
#import "ChooseServicesViewController.h"
#import "HomeViewController.h"


@interface ChooseServicesViewController ()<APIDelegate,UICollectionViewDelegateFlowLayout,UISearchBarDelegate,GenderSelectorDelegate>
{
    NSMutableArray *storesArray;
    NSMutableArray *keywordsArray;
    NSMutableArray *servicesArray;
    
    __weak IBOutlet UIScrollView *mainscrollView;
    
    __weak IBOutlet UIView *optionsV;
    
    __weak IBOutlet UIView *toggleCV;
    __weak IBOutlet UIButton *searchbySalonsB;
    __weak IBOutlet UIButton *searchbyServicesB;
    
    __weak IBOutlet UIView *searchBarCV;
    __weak IBOutlet UISearchBar  *searchBar;
    
    __weak IBOutlet UIView *listingCV;
    
    __weak IBOutlet UIView *storesListCV;
    __weak StoresListingViewController *storesListVC;
    
    __weak IBOutlet UIView *servicesListCV;
    __weak ServicesListingViewController *servicesListVC;
    
    __weak IBOutlet UIView *genderselectorCV;
    __weak GenderSelectorViewController *genderselectorVC;
    
    __weak IBOutlet UIActivityIndicatorView *loadingAIV;
    __weak IBOutlet UILabel *noresultsL;

    
    CGFloat contentHeight;
    
    AppDelegate *appD;
    MyCartController *cartC;
    
    NSString *dlvryType;
    NSString *selLatitude;
    NSString *selLongitude;
    NSString *selGenderId;
    
    BOOL isBackFromOtherView;
    BOOL isShowingServiceDetails;
    
    NSInteger apiCtr ;
}
@end

@implementation ChooseServicesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    [mainscrollView removeGestureRecognizer:mainscrollView.panGestureRecognizer];
    
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    cartC = appD.sessionDelegate.mycartC;

    [self setupLayout];
     //storesListCV.frame = CGRectMake(0, self.view.frame.origin.y - 50, self.view.frame.size.height, self.view.frame.size.width);
}
-(void) viewWillAppear:(BOOL)animated{
    genderselectorCV.hidden = YES; // Hide the male female controller when salon button is on.
    
    
    // Frame changed y and hiight.
    CGRect r = [listingCV frame];
    r.origin.y = 210.0;
    [listingCV setFrame:r];
    CGRect h = [listingCV frame];
    h.size.height = 455.0;
    [listingCV setFrame:h];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public Methods

- (void) loadStoresForSalonServices {
    dlvryType = DELIVERY_TYPE_SALON;
    [self reloadList];
}

- (void) loadStoresForHomeServices {
    dlvryType = DELIVERY_TYPE_HOME;
    [self reloadList];
}

- (void) displayStoreRateCard : (NSDictionary *) storesDict
{
    [cartC updateStoreInformation:storesDict];
    [self performSegueWithIdentifier:@"showstoreratecard" sender:storesDict];
}

- (void) displayStoreRateCardUsingService : (NSDictionary *) serviceDict
{
    
}

- (void) getServicesByKeyword : (NSString *) queryText{
    isShowingServiceDetails = YES;
    [UIResponder dismissKeyboard];
    searchBar.text = queryText;
    [self loadServicesByKeyword];
}

#pragma mark - Methods

- (void) setupLayout{
    
    searchbySalonsB.selected = YES;
    searchbyServicesB.selected = NO;
    
    selLatitude = [appD getUserCurrentLocationLatitude];
    selLongitude = [appD getUserCurrentLocationLongitude];
    selGenderId = [appD getLoggedInUserGender];
    [genderselectorVC setGender:selGenderId];
    
    storesArray = [NSMutableArray array];
    keywordsArray = [NSMutableArray array];
    servicesArray = [NSMutableArray array];
    
    toggleCV.layer.borderColor = [UIColor whiteColor].CGColor;
    searchBarCV.layer.borderColor = [UIColor whiteColor].CGColor;
   
    // new code minnarao.
    //mainscrollView.contentOffset = CGPointZero;
    //contentHeight = CGRectGetHeight(self.view.bounds);
    
    storesListCV.hidden = servicesListCV.hidden = YES;
    
    [self setupSearchBar];
    
    searchbySalonsB.backgroundColor = searchbySalonsB.selected?[UIColor whiteColor] : [UIColor clearColor];
    searchbyServicesB.backgroundColor = searchbyServicesB.selected?[UIColor whiteColor] : [UIColor clearColor];
    
    UITextField *txfSearchField = [searchBar valueForKey:@"_searchField"];
    txfSearchField.placeholder = searchbySalonsB.selected? @"Search Salons": @"Search Services";
}

- (void) setupSearchBar
{
    UITextField *txfSearchField = [searchBar valueForKey:@"_searchField"];
    
    txfSearchField.clearButtonMode = UITextFieldViewModeWhileEditing;
    txfSearchField.backgroundColor = [UIColor clearColor];
    txfSearchField.font = [UIFont semiboldFontOfSize:13.0];
    txfSearchField.textColor = [UIColor whiteColor];
    txfSearchField.tintColor = txfSearchField.textColor;
    txfSearchField.placeholder = @"Search Services";
    
    //To remove black border around searchbar
    [searchBar setBackgroundImage:[[UIImage alloc]init]];
    for (UIView * view in [[[searchBar subviews] objectAtIndex:0] subviews])
    {
        if (![view isKindOfClass:[UITextField class]] && ![view isKindOfClass:NSClassFromString(@"UISearchBarBackground")])
        {
            view .alpha = 0;
        }
    }
}

- (void) toggleListing
{
    if(searchbySalonsB.selected){
        genderselectorCV.hidden = YES; // Hide the male female controller.
        
        // Fram change intioal.
        CGRect r = [listingCV frame];
        r.origin.y = 210.0;
        [listingCV setFrame:r];
        
        // set height.
        CGRect h = [listingCV frame];
        h.size.height = 455.0;
        [listingCV setFrame:h];
       
        noresultsL.hidden = ([storesArray count] > 0);
        storesListCV.hidden = ([storesArray count] == 0);
        
       
    }
    else{
        genderselectorCV.hidden = NO;
        
        // Fram change int.
        CGRect r = [listingCV frame];
        r.origin.y = 260.0;
        [listingCV setFrame:r];
        
        // set height.
        CGRect h = [listingCV frame];
        h.size.height = 400.0;
        [listingCV setFrame:h];
        
        noresultsL.hidden = ([keywordsArray count] > 0);
        servicesListCV.hidden = ([keywordsArray count] == 0);
    }
    searchbySalonsB.backgroundColor = searchbySalonsB.selected?[UIColor whiteColor] : [UIColor clearColor];
    searchbyServicesB.backgroundColor = searchbyServicesB.selected?[UIColor whiteColor] : [UIColor clearColor];
    
    UITextField *txfSearchField = [searchBar valueForKey:@"_searchField"];
    txfSearchField.placeholder = searchbySalonsB.selected? @"Search Salons": @"Search Services";
    
    // For when chage the tab the text will be empty.
    searchBar.text = @"";
    [self searchListing : @""];
}


- (void) searchListing : (NSString *) queryText
{
    NSArray *tmpArray;
    if(searchbySalonsB.selected){
        if (queryText.length > 0) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"searchKey CONTAINS[cd] %@",queryText];
            tmpArray =[storesArray filteredArrayUsingPredicate:predicate];
        }
        else{
            tmpArray = storesArray;
        }
        [storesListVC loadData: tmpArray];
        storesListCV.hidden = NO;
        servicesListCV.hidden = YES;
    }
    else{
        isShowingServiceDetails = NO;
        if (queryText.length > 0) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"Name CONTAINS[cd] %@",queryText];
            tmpArray =[keywordsArray filteredArrayUsingPredicate:predicate];
        }
        else{
            tmpArray = keywordsArray;
        }
        [servicesListVC loadKeywords: tmpArray];
        storesListCV.hidden = YES;
        servicesListCV.hidden = NO;
    }
    
    // new code minnarao.
    /*
    contentHeight = CGRectGetHeight(optionsV.frame) + ( searchbySalonsB.selected ? [storesListVC getContentHeight] : [servicesListVC getContentHeight]) ;
    if (contentHeight <= CGRectGetHeight(mainscrollView.bounds))
    {
        contentHeight = CGRectGetHeight(mainscrollView.bounds);
        mainscrollView.scrollEnabled = NO;
    }
    
    [mainscrollView setContentSize:CGSizeMake(CGRectGetWidth(mainscrollView.frame), contentHeight)];
    listingCV.frame = [UpdateFrame setSizeForView:listingCV usingHeight:contentHeight];*/
}

- (void) reloadList
{
    if ([InternetCheck isOnline]){
        apiCtr = 2;
        [self loadNearbyStores];
        [self loadSearchKeywords];
        
        [loadingAIV startAnimating];
        storesListCV.hidden = YES;
        servicesListCV.hidden = YES;
        noresultsL.hidden = YES;
    }
}

- (void) loadNearbyStores
{
    if ([InternetCheck isOnline]){
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject:selLatitude forKey:PARAM_LATITUDE];
        [paramDict setObject:selLongitude forKey:PARAM_LONGITUDE];
        [paramDict setObject: dlvryType forKey:@"Chain"];
        
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_NEARBY_STORES Param:paramDict];
    }
}

- (void) loadSearchKeywords
{
    if ([InternetCheck isOnline]){
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject:selGenderId forKey:KEY_GENDER];
        [paramDict setObject:dlvryType forKey:@"ServiceAvailableAt"];
        [paramDict setObject:@NO forKey:@"IsSearchBlog"];
        [paramDict setObject:@NO forKey:@"IsSearchLook"];
        [paramDict setObject:@NO forKey:@"IsSearchPackage"];
        [paramDict setObject:@NO forKey:@"IsSearchProduct"];
        [paramDict setObject:@NO forKey:@"IsSearchSaloon"];
        [paramDict setObject:@NO forKey:@"IsSearchStylist"];
        [paramDict setObject:@YES forKey:@"IsSearchService"];
        [paramDict setObject: @"" forKey:@"Keyword"];
        
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_SEARCH_KEYWORDS Param:paramDict];
    }
}

- (void) loadServicesByKeyword{
    apiCtr = 2;
    if ([InternetCheck isOnline]){
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject:selGenderId forKey:KEY_GENDER];
        [paramDict setObject:dlvryType forKey:@"ServiceAvailableAt"];
        [paramDict setObject:@NO forKey:@"IsSearchBlog"];
        [paramDict setObject:@NO forKey:@"IsSearchLook"];
        [paramDict setObject:@NO forKey:@"IsSearchPackage"];
        [paramDict setObject:@NO forKey:@"IsSearchProduct"];
        [paramDict setObject:@NO forKey:@"IsSearchSaloon"];
        [paramDict setObject:@NO forKey:@"IsSearchStylist"];
        [paramDict setObject:@YES forKey:@"IsSearchService"];
        [paramDict setObject: searchBar.text forKey:@"Keyword"];
        
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_SEARCH_SERVICES_BY_KEYWORD Param:paramDict];
    }
}

- (void) processResponse_Stores : (NSDictionary *) dict
{
    if([storesArray count] > 0) [storesArray removeAllObjects];
    NSArray *tmpArray  = [ReadData arrayFromDictionary:dict forKey:RESPONSEKEY_DATA];
    NSDictionary *tmpDict;
    for(NSDictionary *item in tmpArray){
        NSMutableArray *searchKeys = [NSMutableArray array];

        tmpDict = [ReadData dictionaryFromDictionary:item forKey:KEY_SALON];
        [searchKeys addObject: [ReadData stringValueFromDictionary:tmpDict forKey:KEY_NAME]];
        tmpDict = [ReadData dictionaryFromDictionary:item forKey: KEY_STORE_LOCATION];
        [searchKeys addObject: [ReadData stringValueFromDictionary:tmpDict forKey:KEY_NAME]];
        
        NSMutableDictionary *obj = [item mutableCopy];
        [obj setObject:[searchKeys componentsJoinedByString: @" "] forKey:@"searchKey"];
        [storesArray addObject:obj];
    }
}

- (void) processResponse_Keywords : (NSDictionary *) dict
{
    if([keywordsArray count] > 0) [keywordsArray removeAllObjects];
    NSDictionary *dataDict  = [ReadData dictionaryFromDictionary:dict forKey:RESPONSEKEY_DATA];
    NSArray *tmpArray = [ReadData arrayFromDictionary:dataDict forKey:@"Services"];
   
    for(NSDictionary *item in tmpArray){
        BOOL isFound = NO;
        NSString *serviceNm = [ReadData stringValueFromDictionary:item forKey:@"ServiceName"];
        for(NSMutableDictionary *obj in keywordsArray){
            if([[ReadData stringValueFromDictionary:obj forKey:KEY_NAME] isEqualToString:serviceNm]){
                NSInteger newCtr = [ReadData integerValueFromDictionary:obj forKey:KEY_COUNT] + 1;
                [obj setObject:[NSNumber numberWithInteger:newCtr] forKey:KEY_COUNT];
                isFound =YES;
                break;
            }
        }
        if(!isFound){
            NSMutableDictionary *obj = [NSMutableDictionary dictionary];
            [obj setValue:serviceNm forKey:KEY_NAME];
            [obj setValue:[NSNumber numberWithInteger:1] forKey:KEY_COUNT];
            [keywordsArray addObject:obj];
        }
    }
}

- (void) processResponse_Services : (NSDictionary *) dict
{
    if([servicesArray count] > 0) [servicesArray removeAllObjects];
    NSDictionary *dataDict  = [ReadData dictionaryFromDictionary:dict forKey:RESPONSEKEY_DATA];
    NSArray *tmpArray = [ReadData arrayFromDictionary:dataDict forKey:@"Services"];
    
    for(NSDictionary *item in tmpArray){
        NSMutableDictionary *obj = [item mutableCopy];
        [servicesArray addObject:obj];
    }
    [servicesListVC loadServices:servicesArray];
}


#pragma mark - Scroll content

- (CGFloat) getContentHeight;
{
    return contentHeight;
}

- (void) scrollContent : (CGPoint) offset
{
    mainscrollView.contentOffset = offset;
}

- (CGPoint) getScrollContent
{
    return mainscrollView.contentOffset;
}

#pragma mark - Gender Selector & Delegate Methods

- (void) valueChanged{
    selGenderId = [genderselectorVC getGender];
    [self reloadList];
}

#pragma mark - Searchbar delegate methods

- (void) searchBarSearchButtonClicked:(UISearchBar *)asearchBar
{
    [searchBar resignFirstResponder];
    [self searchListing:asearchBar.text];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)asearchBar
{
    searchBar.showsCancelButton = NO;
    [searchBar resignFirstResponder];
    searchBar.text = @"";
    [self searchListing:asearchBar.text];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)asearchBar
{
    [searchBar becomeFirstResponder];
    searchBar.showsCancelButton = YES;
}

- (void)searchBar:(UISearchBar *)asearchBar textDidChange:(NSString *)searchText
{
    [self searchListing:asearchBar.text];
}

#pragma mark - Events

- (IBAction) searchbySalonsBPressed:(UIButton *)sender{
    [searchBar resignFirstResponder];

    if(!sender.selected){
        searchbySalonsB.selected = YES;
        searchbyServicesB.selected = NO;
        [self toggleListing];
    }
    
}

- (IBAction) searchbyServicesBPressed:(UIButton *)sender{
    [searchBar resignFirstResponder];

    if(!sender.selected){
        searchbySalonsB.selected = NO;
        searchbyServicesB.selected = YES;
        [self toggleListing];
    }
    
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"showgenderselector_services_home"]){
        genderselectorVC = (GenderSelectorViewController *)segue.destinationViewController;
        genderselectorVC.delegate = self;
    }
    else if([segue.identifier isEqualToString:@"embed_storeslisting"]){
        storesListVC = (StoresListingViewController *) segue.destinationViewController;
    }
    else if([segue.identifier isEqualToString:@"embed_serviceslisting"]){
        servicesListVC = (ServicesListingViewController *) segue.destinationViewController;
    }
    else if([segue.identifier isEqualToString:@"showstoreratecard"]){
        [cartC setLatitude:selLatitude andLongitude:selLongitude];
        StoreRateCardViewController *servicesVC = (StoreRateCardViewController *)segue.destinationViewController;
        [servicesVC loadForSalonStore:(NSDictionary *)sender Gender:selGenderId  andDeliveryType:dlvryType];
    }
}


#pragma mark - API

-(void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName
{
    apiCtr --;
    if(![ReadData isValidResponseData:response])
    {
        {
            NSString *message = [ReadData stringValueFromDictionary:response forKey:RESPONSEKEY_ERRORMESSAGE];
            [appD showAPIErrorWithMessage:message andTitle:ALERT_TITLE_ERROR];
            [self.view setUserInteractionEnabled:YES];
        }
        return;
    }
    
    if ([apiName isEqualToString:API_GET_NEARBY_STORES]) {
        [self processResponse_Stores:response];
    }
    
    if ([apiName isEqualToString:API_SEARCH_KEYWORDS]) {
        [self processResponse_Keywords:response];
    }

    if ([apiName isEqualToString:API_SEARCH_SERVICES_BY_KEYWORD]) {
        [self processResponse_Services:response];
    }
    
    if(apiCtr <= 0){
        [loadingAIV stopAnimating];
        [self toggleListing];
    }
}

//Minnarao New implementaion
#pragma mark - Events

- (IBAction)backBPressed:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
