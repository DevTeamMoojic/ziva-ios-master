//
//  StoresListingViewController.h
//  Ziva
//
//  Created by Bharat on 15/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoresListingViewController : UIViewController

- (void) loadData : (NSArray *) filteredDataList;

- (CGFloat) getContentHeight;

@end
