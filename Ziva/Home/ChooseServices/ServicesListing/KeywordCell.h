//
//  KeywordCell.h
//  Ziva
//
//  Created by Bharat on 16/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KeywordCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *mainV;

@property (weak, nonatomic) IBOutlet UILabel *titleL;

- (void) configureDataForCell : (NSDictionary *) item;

@end
