//
//  StylistsViewController.m
//  Ziva
//
//  Created by Bharat on 30/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "StylistCell.h"
#import "StylistsViewController.h"

@interface StylistsViewController ()<APIDelegate>
{
    NSArray *list;
    
    __weak IBOutlet UIScrollView *mainscrollView;
    __weak IBOutlet UITableView *listTblV;
    __weak IBOutlet UILabel *noresultsL;
    
    
    CGFloat contentHeight;
    CGFloat maxWidth;
    
    AppDelegate *appD;
    
}
@end

@implementation StylistsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [mainscrollView removeGestureRecognizer:mainscrollView.panGestureRecognizer];
    [self resetLayout];
    
    [self setupLayout];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public Methods

- (void) updateLayout {
    [self setupLayout];
}

#pragma mark - Methods

-(void) resetLayout
{
    mainscrollView.contentOffset = CGPointZero;
    contentHeight = CGRectGetHeight(self.view.bounds);
}

- (void) setupLayout
{
    maxWidth = CGRectGetWidth(self.view.bounds) - 35 - 75;
    [self reloadList];
}

- (void) reloadList
{
    if ([InternetCheck isOnline]){
        
        NSMutableArray *replacementArray = [NSMutableArray array];
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject:@"PAGE_ID" forKey:@"key"];
        [paramDict setObject:@"0" forKey:@"value"];
        
        [replacementArray addObject:paramDict];
        
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_STYLISTS Param:Nil replacementStrings:replacementArray];
    }
}

-(CGFloat) calculateHeightOfList
{
    CGFloat height = 0;
    for(NSInteger ctr = 0; ctr < [list count]; ctr++){
        height += [self calculateheightForRow:ctr];
    }
    return height;
}


- (void) processResponse : (NSDictionary *) dict
{
    NSArray *tmpArray = [ReadData arrayFromDictionary:dict forKey:RESPONSEKEY_DATA];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:KEY_NAME ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    list = [tmpArray sortedArrayUsingDescriptors:sortDescriptors];

    [listTblV reloadData];
    [listTblV setContentOffset:CGPointZero animated:NO];
    
    listTblV.hidden = ([list count] == 0);
    noresultsL.hidden = !listTblV.hidden;
    
    contentHeight = [self calculateHeightOfList];
    
    if (contentHeight <= CGRectGetHeight(mainscrollView.bounds))
    {
        contentHeight = CGRectGetHeight(mainscrollView.bounds);
        mainscrollView.scrollEnabled = NO;
    }
    
    [mainscrollView setContentSize:CGSizeMake(CGRectGetWidth(mainscrollView.frame), contentHeight)];
    
    listTblV.frame = [UpdateFrame setSizeForView:listTblV usingHeight:contentHeight];
}


#pragma mark - Scroll content

- (CGFloat) getContentHeight;
{
    return contentHeight;
}

- (void) scrollContent : (CGPoint) offset
{
    mainscrollView.contentOffset = offset;
}

- (CGPoint) getScrollContent
{
    return mainscrollView.contentOffset;
}

#pragma mark - Table view data source

- (NSString *) getStartingPriceDescription : (NSDictionary *) item{
    NSString *str = @"";
    str = [ReadData amountInRsFromDictionary:item forKey:KEY_STYLIST_STARTING_PRICE];
    str = [str stringByAppendingString:@" (Starting Price)"];
    if([[ReadData stringValueFromDictionary:item forKey:KEY_STYLIST_SERVICES] length] > 0 )
    {
        str = [str stringByAppendingString : @" for "];
        str = [str stringByAppendingString : [ReadData stringValueFromDictionary:item forKey:KEY_STYLIST_SERVICES]];
    }
    return str;
}

-(CGFloat) calculateheightForRow:(NSInteger)row{
    NSDictionary *item = list[row];
    CGFloat height = 15;
    height += 15;
    height += 13;
    height += 2;
    height += [ResizeToFitContent getHeightForText:[self getStartingPriceDescription:item] usingFontType:FONT_SEMIBOLD FontOfSize:11 andMaxWidth:maxWidth];
    height += 13;
    height += 15;
    return height;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [list count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self calculateheightForRow:indexPath.row];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    StylistCell *cell = (StylistCell *)[tableView dequeueReusableCellWithIdentifier:@"stylistcell" forIndexPath:indexPath];
    NSDictionary *item = list[indexPath.row];
    cell.lblMaxWidth = maxWidth;
    [cell configureDataForCell :item];
    return cell;
}

#pragma mark - API

-(void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName
{
    if(![ReadData isValidResponseData:response])
    {
        {
            NSString *message = [ReadData stringValueFromDictionary:response forKey:RESPONSEKEY_ERRORMESSAGE];
            [appD showAPIErrorWithMessage:message andTitle:ALERT_TITLE_ERROR];
            [self.view setUserInteractionEnabled:YES];
        }
        return;
    }
    
    if ([apiName isEqualToString:API_GET_STYLISTS]) {
        [self processResponse:response];
    }
}

@end
