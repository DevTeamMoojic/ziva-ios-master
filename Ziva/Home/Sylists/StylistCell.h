//
//  StylistCell.h
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StylistCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *mainV;
@property (weak, nonatomic) IBOutlet UIView *imageCV;


@property (weak, nonatomic) IBOutlet UIImageView *mainImgV;
@property (weak, nonatomic) IBOutlet UIImageView *placeholderImgV;

@property (weak, nonatomic) IBOutlet UIView *contentCV;
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *subtitleL;
@property (weak, nonatomic) IBOutlet UILabel *genderL;
@property (weak, nonatomic) IBOutlet UILabel *startingpriceL;
@property (weak, nonatomic) IBOutlet UILabel *locationL;

@property (nonatomic) CGFloat lblMaxWidth;

- (void) configureDataForCell : (NSDictionary *) item;

@end
