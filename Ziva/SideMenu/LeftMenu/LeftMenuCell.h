//
//  LeftMenuCell.h
//  Ziva
//
//  Created by  Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.

#import <UIKit/UIKit.h>

@interface LeftMenuCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *backV;
@property (weak, nonatomic) IBOutlet UIView *seperatorV;
@property (weak, nonatomic) IBOutlet UILabel *titleL;

@property (weak, nonatomic) IBOutlet UIButton *menuB;
@property (weak, nonatomic) IBOutlet UIButton *selectRowB;

- (void) configureDataForCell : (NSString *) menuoption;
- (void) toogleSelectMenuOption : (BOOL) selected;

@end
