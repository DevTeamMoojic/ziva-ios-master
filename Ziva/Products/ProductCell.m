//
//  ProductCell.m
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "ProductCell.h"

@implementation ProductCell

- (void)awakeFromNib {
    // Initialization code
    
    [super awakeFromNib];
    
    // Fix : Autoresizing issues for iOS 7 using Xcode 6
    self.contentView.frame = self.bounds;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //    Fix : For right aligned view with autoresizing
    UIView *backV = [[self.contentView subviews] objectAtIndex:0];
    backV.frame = [UpdateFrame setSizeForView:backV usingSize:self.bounds.size];
}

- (void) configureDataForCell : (NSDictionary *) item
{
    self.mainImgV.image = Nil;
    self.mainImgV.alpha = 0;
    self.placeholderImgV.alpha=1;
    
    self.titleL.text = [ReadData stringValueFromDictionary:item forKey:KEY_PRODUCT_TITLE];
    self.shortDescL.text = [ReadData stringValueFromDictionary:item forKey:KEY_PRODUCT_SUBTITLE];
    
    CGFloat tmpHeight = [ResizeToFitContent getHeightForNLines:2 ForText:self.shortDescL.text usingFontType:FONT_BOLD FontOfSize:10 andMaxWidth:CGRectGetWidth(self.titleL.frame)];
    self.shortDescL.frame = [UpdateFrame setSizeForView:self.shortDescL usingHeight:tmpHeight];
    
    NSString *originalPrice = [ReadData amountInRsFromDictionary:item forKey:KEY_PRODUCT_ORIGINALPRICE];
    NSString *zivaPrice = [ReadData amountInRsFromDictionary:item forKey:KEY_PRODUCT_PRICE];
    self.offerpriceL.text = zivaPrice;
    NSAttributedString * title =   [[NSAttributedString alloc] initWithString:originalPrice
                                    attributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle)}];
    [self.originalpriceL setAttributedText:title];
    self.originalpriceL.hidden = [originalPrice isEqualToString:zivaPrice];
    
    NSArray *cashbacks = [ReadData arrayFromDictionary:item forKey:KEY_CASHBACKS];
    CGFloat positionY = (CGRectGetHeight(self.priceInfoV.frame) - CGRectGetHeight(self.offerpriceL.frame)) *0.5;
    if([cashbacks count] > 0){
        self.cashbackL.hidden = NO;
        NSDictionary *dict = [cashbacks firstObject];
        self.cashbackL.text = [ReadData cashbackInRsFromDictionary:dict forKey:KEY_CASHBACKS_COST];
        positionY = (CGRectGetHeight(self.priceInfoV.frame) - 2*CGRectGetHeight(self.offerpriceL.frame)) / 3;
        
        self.cashbackL.frame = [UpdateFrame setPositionForView:self.cashbackL usingPositionY: positionY * 2 + CGRectGetHeight(self.offerpriceL.frame)];
    }
    
    self.offerpriceL.frame = [UpdateFrame setPositionForView:self.offerpriceL usingPositionY: positionY];
    self.originalpriceL.frame = [UpdateFrame setPositionForView:self.originalpriceL usingPositionY: positionY];
    
    // ImageView
    NSString *url = [ReadData stringValueFromDictionary:item forKey:KEY_IMAGE_URL];
    if (![url isEqualToString:@""])
    {
        NSURL *imageURL = [NSURL URLWithString:url];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:imageURL];
        [self.mainImgV setImageWithURLRequest:urlRequest
                             placeholderImage:Nil
                                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             [self.mainImgV setImage:image];
             [UIView animateWithDuration:IMAGE_VIEW_TOGGLE
                                   delay:0 options:UIViewAnimationOptionCurveLinear
                              animations:^(void)
              {
                  
                  [self.placeholderImgV setAlpha:0 ];
                  [self.mainImgV setAlpha:1 ];
              }
                              completion:^(BOOL finished)
              {
              }];
             
             
             
         }
                                      failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
         {
             
         }];
    }
}

@end
