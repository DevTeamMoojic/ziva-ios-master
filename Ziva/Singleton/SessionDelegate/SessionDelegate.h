//
//  SessionDelegate.h
//  GuestList
//
//  Created by Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

@class MyCartController;
@class MyTrackingController;

#import <Foundation/Foundation.h>

@interface SessionDelegate : NSObject

@property (nonatomic, strong) MyCartController *mycartC;
@property (nonatomic, strong) MyTrackingController *mytrackingC;

- (void) loadDataForLoggedInUser ;

@end
