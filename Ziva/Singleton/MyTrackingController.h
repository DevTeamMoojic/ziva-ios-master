//
//  MyTrackingController.h
//  Ziva
//
//  Created by Bharat on 13/10/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyTrackingController : NSObject

- (void) initTrackingData;

- (void) trackLookById : (NSString *) recordId;

- (NSArray *) fetchRecentlyViewedLooks;

- (NSArray *) fetchPopularLooks;


@end
