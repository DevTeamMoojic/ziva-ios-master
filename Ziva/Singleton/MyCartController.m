//
//  MyCartController.m
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "MyCartController.h"

#define API_RESPONSE_ERROR @"error"
#define BILL_GENERATOR_URL @"http://api.zivadiva.com/payment/v2/citrus/billgenerator/"

@interface MyCartController ()<APIDelegate>
{
    AppDelegate *appD;
    
    NSMutableArray *items;
    
    NSInteger cartItems;
    CGFloat cartValue;
    
    
    NSArray *lstCoupons;
    NSArray *lstStores;
    
    NSDictionary *requestDict;
    NSDictionary *responseDict;
    NSDictionary *paymentSummaryDict;
    NSDictionary *orderInfoDict;
    
    NSMutableArray *tmpSlots;
    NSMutableArray *delegates;
    
    BOOL availabilityStatus;
    
    NSString *orderId;
}

@end

@implementation MyCartController

- (id)init
{
    if (self = [super init])
    {
        [self initData];
        delegates = [NSMutableArray array];
        items = [NSMutableArray array];
        tmpSlots = [NSMutableArray array];
    }
    return self;
}

#pragma mark - Private Methods

- (void)initData
{
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    cartItems = 0;
    cartValue = 0.0f;
    self.isZenotiEnabled = NO;
    self.isCommonServices = NO;
}

-(void) addItem:(NSString *) recordId withTitle :(NSString *) itemDesc andAmount :(CGFloat) amount{
    BOOL isFound = NO;
    for(NSMutableDictionary *item in items){
        if([[ReadData recordIdFromDictionary:item forKey:KEY_ID] isEqualToString:recordId]){
            NSInteger newQty = [ReadData integerValueFromDictionary:item forKey:KEY_QUANTITY] + 1;
            CGFloat newAmount = [ReadData floatValueFromDictionary:item forKey:KEY_AMOUNT] +  amount;
            [item setObject:[NSNumber numberWithInteger:newQty] forKey:KEY_QUANTITY];
            [item setObject:[NSNumber numberWithFloat:newAmount] forKey:KEY_AMOUNT];
            isFound =YES;
            break;
        }
    }
    if(!isFound){
        NSMutableDictionary *obj = [NSMutableDictionary dictionary];
        [obj setValue:recordId forKey:KEY_ID];
        [obj setValue:itemDesc forKey:KEY_NAME];
        [obj setValue:[NSNumber numberWithFloat:amount] forKey:KEY_RATE_PERUNIT];
        [obj setValue:[NSNumber numberWithFloat:amount] forKey:KEY_AMOUNT];
        [obj setValue:[NSNumber numberWithInteger:1] forKey:KEY_QUANTITY];
        [items addObject:obj];
    }
    cartItems+=1;
    cartValue+=amount;
}

-(void) lessItem:(NSString *) recordId  andAmount :(CGFloat) amount{
    BOOL isFound = NO;
    NSMutableDictionary *obj ;
    for(NSMutableDictionary *item in items){
        if([[ReadData recordIdFromDictionary:item forKey:KEY_ID] isEqualToString:recordId]){
            NSInteger newQty = [ReadData integerValueFromDictionary:item forKey:KEY_QUANTITY] - 1;
            CGFloat newAmount = [ReadData floatValueFromDictionary:item forKey:KEY_AMOUNT] -  amount;
            [item setObject:[NSNumber numberWithInteger:newQty] forKey:KEY_QUANTITY];
            [item setObject:[NSNumber numberWithFloat:newAmount] forKey:KEY_AMOUNT];
            obj = item;
            isFound =YES;
            break;
        }
    }
    if(isFound){
        if([ReadData integerValueFromDictionary:obj forKey:KEY_QUANTITY] == 0){
            [items removeObject:obj];
        }
    }
    cartItems-=1;
    cartValue-=amount;
}

#pragma mark - Delegate handling

- (void)addDelegate:(id<MyCartDelegate>)delegateObject {
    BOOL isFound = NO;
    for(id<MyCartDelegate> item in delegates){
        if([item isEqual:delegateObject]){
            isFound = YES;
            break;
        }
    }
    
    if(!isFound){
        [delegates addObject:delegateObject];
    }
}

- (void)removeDelegate:(id<MyCartDelegate>)delegateObject {
    BOOL isFound = NO;
    for(id<MyCartDelegate> item in delegates){
        if([item isEqual:delegateObject]){
            isFound = YES;
            break;
        }
    }
    
    if(isFound){
        [delegates removeObject:delegateObject];
    }
}

- (void) sendDelegateEvent : (NSString *) eventName
{
    if([delegates count] == 0) return;
    [delegates enumerateObjectsUsingBlock:^(id<MyCartDelegate> item,
                                            NSUInteger idx,
                                            BOOL *stop) {
        if([eventName isEqualToString:API_RESPONSE_ERROR]){
            if([item respondsToSelector:@selector(onResponseError)]){
                [item onResponseError];
            }
        }
        else if([eventName isEqualToString:API_GET_STORES]){
            if([item respondsToSelector:@selector(onStoreDetailCompleted)]){
                [item onStoreDetailCompleted];
            }
        }
        else if([eventName isEqualToString:API_GET_COUPONS]){
            if([item respondsToSelector:@selector(onCouponsListCompleted)]){
                [item onCouponsListCompleted];
            }
        }
        else if([eventName isEqualToString:API_REQUEST_BOOKING]){
            if([item respondsToSelector:@selector(onRequestBookingGenerated)]){
                [item onRequestBookingGenerated];
            }
        }
        else if([eventName isEqualToString:API_ZENOTI_AVAILABILITY]){
            if([item respondsToSelector:@selector(onAvailabilityGenerated)]){
                [item onAvailabilityGenerated];
            }
        }
        else if([eventName isEqualToString:API_SLOT_UPDATE]){
            if([item respondsToSelector:@selector(onUpdateSlot)]){
                [item onUpdateSlot];
            }
        }
        else if([eventName isEqualToString:API_ZENOTI_SLOT_UPDATE]){
            if([item respondsToSelector:@selector(onUpdateZenotiSlot)]){
                [item onUpdateZenotiSlot];
            }
        }
        else if([eventName isEqualToString:API_PAYMENT_SUMMARY]){
            if([item respondsToSelector:@selector(onPaymentSummaryGenerated)]){
                [item onPaymentSummaryGenerated];
            }
        }
        else if([eventName isEqualToString:API_CREATE_ORDER]){
            if([item respondsToSelector:@selector(onOrderGenerated)]){
                [item onOrderGenerated];
            }
        }
        else if([eventName isEqualToString:API_DETAIL_VIEW_ORDER]){
            if([item respondsToSelector:@selector(onOrderDetailCompleted)]){
                [item onOrderDetailCompleted];
            }
        }
    }];
}


#pragma mark - Location

- (void) setLatitude:(NSString *) lat andLongitude : (NSString *) lng{
    self.latitude = lat;
    self.longitude = lng;
    
    [self getStores];
}

#pragma mark - Services

- (void) initForServicesWithDeliveryType : (NSString *) type{
    self.cartType = CART_TYPE_SERVICES;
    self.deliveryType = type;
    self.isCommonServices = NO;
    self.isCashPaymentAllowed = YES;
    self.requestId = @"";
    self.responseId = @"";
    [self makeEmpty];
}

- (void) initForCommonServices{
    self.isCommonServices = YES;
    self.isZenotiEnabled = NO;
    self.storeId = @"";
    self.storeAddress = @"";
    self.storeName = @"";
    self.requestId = @"";
    self.responseId = @"";
    [self makeEmpty];
}

- (void) addServices :(NSDictionary *) serviceItem{
    NSString *recordId = [ReadData recordIdFromDictionary:serviceItem forKey:KEY_ID];
    NSString *itemDesc = [ReadData stringValueFromDictionary:serviceItem forKey:KEY_SERVICE_TITLE];
    CGFloat amount = [ReadData floatValueFromDictionary:serviceItem forKey:KEY_SERVICE_PRICE];
    [self addItem:recordId withTitle:itemDesc andAmount:amount];
}

- (void) lessServices :(NSDictionary *) serviceItem{
    NSString *recordId = [ReadData recordIdFromDictionary:serviceItem forKey:KEY_ID];
    CGFloat amount = [ReadData floatValueFromDictionary:serviceItem forKey:KEY_SERVICE_PRICE];
    [self lessItem:recordId andAmount:amount];
}

#pragma mark - Looks

- (void) initForLooksWithDeliveryType : (NSString *) type{
    self.cartType = CART_TYPE_LOOKS;
    self.deliveryType = type;
    self.isCommonServices = NO;
    self.isZenotiEnabled = NO;
    self.storeId = @"";
    self.storeAddress = @"";
    self.storeName = @"";
    self.requestId = @"";
    self.responseId = @"";
    [self makeEmpty];
}

- (void) addLooks :(NSDictionary *) lookItem{
    NSString *recordId = [ReadData recordIdFromDictionary:lookItem forKey:KEY_ID];
    NSString *itemDesc = [ReadData stringValueFromDictionary:lookItem forKey:KEY_LOOK_HEADER];
    CGFloat amount = [ReadData floatValueFromDictionary:lookItem forKey:KEY_LOOK_SILVERPRICE];
    [self addItem:recordId withTitle:itemDesc andAmount:amount];
}

- (void) lessLooks :(NSDictionary *) lookItem{
    NSString *recordId = [ReadData recordIdFromDictionary:lookItem forKey:KEY_ID];
    CGFloat amount = [ReadData floatValueFromDictionary:lookItem forKey:KEY_LOOK_SILVERPRICE];
    [self lessItem:recordId andAmount:amount];
}


#pragma mark - Products

- (void) initForProducts{
    self.cartType = CART_TYPE_PRODUCTS;
    self.requestId = @"";
    self.responseId = @"";
    [self makeEmpty];
}

-(void) addProducts :(NSDictionary *) productItem{
    NSString *recordId = [ReadData recordIdFromDictionary:productItem forKey:KEY_ID];
    NSString *itemDesc = [ReadData stringValueFromDictionary:productItem forKey:KEY_PRODUCT_TITLE];
    CGFloat amount = [ReadData floatValueFromDictionary:productItem forKey:KEY_PRODUCT_PRICE];
    [self addItem:recordId withTitle:itemDesc andAmount:amount];
}

- (void) lessProducts :(NSDictionary *) productItem{
    NSString *recordId = [ReadData recordIdFromDictionary:productItem forKey:KEY_ID];
    CGFloat amount = [ReadData floatValueFromDictionary:productItem forKey:KEY_PRODUCT_PRICE];
    [self lessItem:recordId andAmount:amount];
}

#pragma mark - Cart Summary

- (NSString *) display_CartTypeInfo{
    NSString *infotext = @"";
    if([self.cartType isEqualToString:CART_TYPE_SERVICES]) {
        infotext = [NSString stringWithFormat:@"Total services added - %ld",(long)cartItems];
    }
    else if([self.cartType isEqualToString:CART_TYPE_SERVICES]) {
        infotext = [NSString stringWithFormat:@"Total products added - %ld",(long)cartItems];
    }
    return infotext;
}

- (NSString *) display_SummaryInfo{
    NSString *infotext = @"0 items";
    if(cartItems > 0){
        infotext = [NSString stringWithFormat:@"%ld item",(long)cartItems];
        if(cartItems > 1){
            infotext =[infotext stringByAppendingString:@"s"];
        }
        infotext =[infotext stringByAppendingFormat:@" - %@", [ReadData getAmountFormatted:cartValue]];

    }
    return infotext;
}

- (NSString *) display_ItemsCount{
    NSString *infotext = @"0 items";
    for(NSDictionary *item in items){
        cartItems+= [ReadData integerValueFromDictionary:item forKey:KEY_QUANTITY] ;
    }
    if(cartItems > 0){
        infotext = [NSString stringWithFormat:@"%ld item",(long)cartItems];
        if(cartItems > 1){
            infotext =[infotext stringByAppendingString:@"s"];
        }
    }
    return infotext;
}

- (NSString *) display_TotalValue{
    return [ReadData getAmountFormatted:cartValue];
}

- (CGFloat) totalValue{
    return  cartValue;
}

- (NSMutableArray *) cartItems{
    return items;
}

- (void) makeEmpty{
    if([items count] > 0){
        [items removeAllObjects];
    }
    cartItems = 0;
    cartValue = 0.0f;
    requestDict = Nil;
    responseDict = Nil;
    paymentSummaryDict = Nil;
}

- (NSMutableArray *) getLineItemIds
{
    NSMutableArray *lineItemIds = [NSMutableArray array];
    for(NSDictionary *item in items){
        [lineItemIds addObject:[ReadData recordIdFromDictionary:item forKey:KEY_ID]];
    }
    return lineItemIds;
}

- (NSArray *) getERPServiceIds
{
    return [ReadData arrayFromDictionary:requestDict forKey:KEY_ERP_SERVICESIDS];
}

- (NSMutableArray *) getProductsArray
{
    NSMutableArray *lineItems = [NSMutableArray array];
    for(NSDictionary *item in items){
        NSMutableDictionary *obj = [NSMutableDictionary dictionary];
        [obj setObject:[ReadData recordIdFromDictionary:item forKey:KEY_ID] forKey:KEY_ID];
        [obj setObject:[NSNumber numberWithInteger:[ReadData integerValueFromDictionary:item forKey:KEY_QUANTITY]] forKey:KEY_QUANTITY];
        [lineItems addObject:obj];
    }
    return lineItems;
}

#pragma mark - Manage Cart Line Item

- (NSInteger) getQuantityInCart : (NSString *) recordId{
    NSInteger qty = 0;
    for(NSMutableDictionary *item in items){
        if([[ReadData stringValueFromDictionary:item forKey:KEY_ID] isEqualToString:recordId]){
            qty = [ReadData integerValueFromDictionary:item forKey:KEY_QUANTITY];
            break;
        }
    }
    return qty;
}

- (BOOL) isAddedToCart : (NSString *) recordId{
    BOOL isFound = NO;
    for(NSMutableDictionary *item in items){
        if([[ReadData stringValueFromDictionary:item forKey:KEY_ID] isEqualToString:recordId]){
            isFound =YES;
            break;
        }
    }
    return isFound;
}

- (void) removeFromCart : (NSString *) recordId{
    for(NSMutableDictionary *item in items){
        if([[ReadData stringValueFromDictionary:item forKey:KEY_ID] isEqualToString:recordId]){
            cartItems = cartItems - [ReadData integerValueFromDictionary:item forKey:KEY_QUANTITY];
            cartValue = cartValue - [ReadData floatValueFromDictionary:item forKey:KEY_AMOUNT];
            [items removeObject:item];
            break;
        }
    }
}

#pragma mark - Helper Methods

- (NSString *) getOrderType
{
    NSString *val = ORDER_TYPE_SERVICES;
    if([self.cartType isEqualToString:CART_TYPE_PRODUCTS]){
        val = ORDER_TYPE_PRODUCTS;
    }
    else if([self.cartType isEqualToString:CART_TYPE_PACKAGES]){
        val = ORDER_TYPE_PACKAGES;
    }
    return val;
}

- (NSString *) getZivaBillURL
{
    return [NSString stringWithFormat:@"%@%@", BILL_GENERATOR_URL,orderId];
}

#pragma mark - Update Information

- (void) updateStoreInformation : (NSDictionary *) infoDict
{
    self.storeId = [ReadData recordIdFromDictionary:infoDict forKey:KEY_ID];
    self.storeName = [ReadData stringValueFromDictionary:infoDict forKey:KEY_NAME];
    NSDictionary *location = [ReadData dictionaryFromDictionary:infoDict forKey:KEY_STORE_LOCATION];
    self.storeAddress = [ReadData stringValueFromDictionary:location forKey:KEY_NAME];
    self.isZenotiEnabled = [ReadData boolValueFromDictionary:infoDict forKey:KEY_STORE_ZENOTIENABLED];
}

#pragma mark - Fetch Information

- (NSArray *) fetchAvailibilitySlots{
    return tmpSlots;
}

- (BOOL) fetchAvailibilityStatus{
    return availabilityStatus;
}

- (NSArray *) fetchCoupons{
    return lstCoupons;
}

- (NSDictionary *) fetchRequestBookingInfo{
    return requestDict;
}

- (NSDictionary *) fetchPaymentSummaryInfo{
    return paymentSummaryDict;
}

- (NSDictionary *) fetchOrderInfo{
    return orderInfoDict;
}

- (NSDictionary *) fetchStoreInfo : (NSString *) storeId
{
    NSDictionary *storeDict;
    for(NSDictionary *item in lstStores){
        if([[ReadData recordIdFromDictionary:item forKey:KEY_ID] isEqualToString:storeId]){
            storeDict = item;
            break;
        }
    }
    return storeDict;
}

#pragma mark - API Calls

- (void) getStores{
    NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
    [paramDict setObject:self.latitude forKey:PARAM_LATITUDE];
    [paramDict setObject:self.longitude forKey:PARAM_LONGITUDE];
    
    [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_NEARBY_STORES Param:paramDict];
}

- (void) getCoupons{
    NSMutableArray *replacementArray = [NSMutableArray array];
    NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
    [paramDict setObject:@"CUSTOMER_ID" forKey:@"key"];
    [paramDict setObject:[appD getLoggedInUserId] forKey:@"value"];
    [replacementArray addObject:paramDict];
    
    
    [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_COUPONS Param:nil replacementStrings:replacementArray];
}

- (void) generateRequestBookingId{
    
    availabilityStatus = NO;
    if([tmpSlots count] > 0) [tmpSlots removeAllObjects];
    NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
    [paramDict setObject: self.latitude forKey: @"Latitude"];
    [paramDict setObject: self.longitude forKey: @"Longitude"];
    [paramDict setObject: BOOK_TYPE_LATER forKey: @"BookingType"];
    [paramDict setObject: self.deliveryType forKey: @"DeliveryType"];
    [paramDict setObject: [appD getLoggedInUserId] forKey: KEY_CUSTOMER_ID];
    [paramDict setObject: @"0" forKey: @"PackType"];
    [paramDict setObject: @"" forKey: @"OtherSeriveId"];
    [paramDict setObject: @"" forKey: @"OfferId"];
    
    if([self.cartType isEqualToString:CART_TYPE_SERVICES])
    {
        if(!self.isZenotiEnabled){
            [paramDict setObject:  [NSString stringWithFormat:@"%@T%@",self.bookDate,self.bookTimeSlot]  forKey:@"BookDate"];
        }
        else{
            [paramDict setObject:  [NSString stringWithFormat:@"%@",self.bookDate]  forKey:@"BookDate"];
        }
        [paramDict setObject: REQUEST_TYPE_SERVICES forKey: @"RequestType"];
        if(![appD isEmptyString:self.storeId]){
            [paramDict setObject: self.storeId forKey: @"StoreId"];
        }        
        [paramDict setObject: [self getLineItemIds] forKey: @"OtherServiceIds"];
    }
    else if([self.cartType isEqualToString:CART_TYPE_LOOKS]){
        [paramDict setObject:  [NSString stringWithFormat:@"%@T%@",self.bookDate,self.bookTimeSlot]  forKey:@"BookDate"];
        [paramDict setObject: REQUEST_TYPE_LOOKS forKey: @"RequestType"];
        [paramDict setObject:[ReadData recordIdFromDictionary:[items firstObject] forKey:KEY_ID] forKey: @"LookId"];
    }
    
    [[[APIHelper alloc] initWithDelegate:self] callAPI:API_REQUEST_BOOKING Param:paramDict];
}

- (void) getZenotiAvailability : (NSString *) storeId
{
    NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
    [paramDict setObject: @"1" forKey:@"Type"]; //0 - Multiple Store; 1 - Single Store
    [paramDict setObject: self.requestId forKey: KEY_REQUEST_ID];
    [paramDict setObject: storeId forKey: @"StoreId"];
    [paramDict setObject:  self.bookDate forKey:@"BookDate"];
    //[paramDict setObject: [self getERPServiceIds] forKey: @"ServiceIds"];
    
    [[[APIHelper alloc] initWithDelegate:self] callAPI:API_ZENOTI_AVAILABILITY Param:paramDict];
}

- (void) updateZenotiSlot
{
    NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
    [paramDict setObject:  self.responseId forKey:KEY_RESPONSE_ID];
    [paramDict setObject:  @"1" forKey:@"Slot"];
    [paramDict setObject: [NSString stringWithFormat:@"%@T%@",self.bookDate,self.bookTimeSlot]  forKey:@"ZenotiBookDate"];
    
    [[[APIHelper alloc] initWithDelegate:self] callAPI:API_ZENOTI_SLOT_UPDATE Param:paramDict];
}

- (void) updateSlot : (BOOL) isZenoti
{
    NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
    [paramDict setObject:  self.responseId forKey:KEY_RESPONSE_ID];
    [paramDict setObject:  @"1" forKey:@"Slot"];
    NSString *keyColumn = (isZenoti) ? @"ZenotiBookDate" : @"BookDate";
    [paramDict setObject: [NSString stringWithFormat:@"%@T%@",self.bookDate,self.bookTimeSlot]  forKey: keyColumn];
    
    [[[APIHelper alloc] initWithDelegate:self] callAPI:API_SLOT_UPDATE Param:paramDict];
}

- (void) getPaymentSummary
{
    NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
    [paramDict setObject: [appD getLoggedInUserId] forKey:KEY_CUSTOMER_ID];
    [paramDict setObject: [self getOrderType] forKey: KEY_ORDER_TYPE];
    if([self.cartType isEqualToString:CART_TYPE_PRODUCTS]){
        [paramDict setObject: [self getProductsArray] forKey: @"Products"];
    }
    else{
        [paramDict setObject: self.responseId forKey: KEY_RESPONSE_ID];
    }
    [paramDict setObject:  (self.isCreditsApplied ? @YES:@NO) forKey:@"ApplyCredits"];
    [paramDict setObject: self.promoCode forKey: @"PromoCode"];
    
    [[[APIHelper alloc] initWithDelegate:self] callAPI:API_PAYMENT_SUMMARY Param:paramDict];
}

- (void) generateOrderId{
    NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
    [paramDict setObject: OS_TYPE_IPHONE forKey: PARAM_PLATFORM];
    [paramDict setObject: [appD getLoggedInUserId] forKey: KEY_CUSTOMER_ID];
    [paramDict setObject: [self getOrderType] forKey: KEY_ORDER_TYPE];
    [paramDict setObject: self.responseId forKey: KEY_RESPONSE_ID];
    [paramDict setObject:  (self.isCreditsApplied ? @YES:@NO) forKey:@"ApplyCredits"];
    [paramDict setObject: self.promoCode forKey: @"PromoCode"];
    [paramDict setObject: ([self.modeOfPayment isEqualToString:PAYMENT_MODE_CASH]?@YES:@NO) forKey: @"IsCOD"];
    
    [[[APIHelper alloc] initWithDelegate:self] callAPI:API_CREATE_ORDER Param:paramDict];
}

- (void) getOrderInfo{
    NSMutableArray *replacementArray = [NSMutableArray array];
    NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
    [paramDict setObject:@"RECORD_ID" forKey:@"key"];
    [paramDict setObject:orderId forKey:@"value"];
    
    [replacementArray addObject:paramDict];
    
    [[[APIHelper alloc] initWithDelegate:self] callAPI:API_DETAIL_VIEW_ORDER Param:nil replacementStrings:replacementArray];    
}

#pragma mark - Process Response API Calls

- (void) processResponse_Stores : (NSArray *) arr
{
    lstStores = arr;
    [self sendDelegateEvent:API_GET_STORES];
}

- (void) processResponse_Coupons : (NSArray *) arr
{
    lstCoupons = arr;
    [self sendDelegateEvent:API_GET_COUPONS];
}

- (void) processResponse_RequestBooking : (NSDictionary *) dict
{
    requestDict = dict;
    self.requestId = [ReadData stringValueFromDictionary:requestDict forKey:KEY_REQUEST_ID];
    [self sendDelegateEvent:API_REQUEST_BOOKING];
}

- (void) processResponse_ZenotiAvailability : (NSDictionary *) dict
{
    if([tmpSlots count] > 0) [tmpSlots removeAllObjects];
    NSString *jsonString = [ReadData stringValueFromDictionary:dict forKey:RESPONSEKEY_DATA];
    if (![jsonString isEqual:[NSNull null]]){
        if (!([jsonString length] == 0)){
            NSData *jsondata = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
            id json = [NSJSONSerialization JSONObjectWithData:jsondata options:NSJSONReadingMutableContainers error:nil];
            
            if ([json isKindOfClass:[NSDictionary class]])
            {
                self.responseId = [ReadData stringValueFromDictionary:json forKey:KEY_RESPONSE_ID];
                
                NSArray *slots = [ReadData arrayFromDictionary:json forKey:@"Slots"];
                if([slots count] > 0 ){
                    NSArray *openSlots = [ReadData arrayFromDictionary:[slots firstObject] forKey:@"OpenSlots"];
                    tmpSlots = [self processResponse_Slots: openSlots ];
                }
            }
        }
    }
    [self sendDelegateEvent:API_ZENOTI_AVAILABILITY];
}

- (NSString *) convertMinsIn24Hr : (NSInteger) utcMinutes
{
    NSInteger minutesOfDay = 330 + utcMinutes;
    NSInteger hrs = minutesOfDay/60 ;
    NSInteger mins = minutesOfDay % 60;
    return [NSString stringWithFormat:@"%02ld:%02ld:00", (long)hrs, (long)mins];
}

- (NSMutableArray *) processResponse_Slots : (NSArray *) slots{
    
    NSMutableArray *tmp = [NSMutableArray array];
    for(NSDictionary *item in slots){
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        NSInteger utcStart  = [ReadData integerValueFromDictionary:item forKey:@"S"];
        NSInteger utcEnd = [ReadData integerValueFromDictionary:item forKey:@"E"];
        [dict setObject:[self convertMinsIn24Hr:utcStart] forKey:KEY_SLOT_ID];
        [dict setObject:[self convertMinsIn24Hr:utcStart] forKey:KEY_SLOT_START_TIME];
        [dict setObject: [self convertMinsIn24Hr:utcEnd] forKey:KEY_SLOT_END_TIME];
        [tmp addObject:dict];
    }
    
    return tmp;
}

-(void) processResponse_UpdateSlot : (NSDictionary *) dict{
    availabilityStatus = [ReadData boolValueFromDictionary:dict forKey:RESPONSEKEY_DATA];
    [self sendDelegateEvent:API_SLOT_UPDATE];
}

-(void) processResponse_UpdateZenotiSlot : (NSDictionary *) dict{
    availabilityStatus = [ReadData boolValueFromDictionary:dict forKey:RESPONSEKEY_DATA];
    [self sendDelegateEvent:API_ZENOTI_SLOT_UPDATE];
}

- (void) processResponse_PaymentSummary : (NSDictionary *) dict
{
    paymentSummaryDict = dict;
    [self sendDelegateEvent:API_PAYMENT_SUMMARY];
}

- (void) processResponse_CreateOrder : (NSString *) recordId
{
    orderId = recordId;
    [self sendDelegateEvent:API_CREATE_ORDER];
}

- (void) processResponse_OrderDetail : (NSDictionary *) dict
{
    orderInfoDict = dict;
    [self sendDelegateEvent:API_DETAIL_VIEW_ORDER];
}


#pragma mark - API delegates

-(void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName
{
    if(![ReadData isValidResponseData:response])
    {
        {
            [self sendDelegateEvent:API_RESPONSE_ERROR];
            NSString *message = [ReadData stringValueFromDictionary:response forKey:RESPONSEKEY_ERRORMESSAGE];
            [appD showAPIErrorWithMessage:message andTitle:ALERT_TITLE_ERROR];
        }
        return;
    }

    if([apiName isEqualToString:API_GET_NEARBY_STORES]){
        [self processResponse_Stores:[ReadData arrayFromDictionary:response forKey:RESPONSEKEY_DATA]];
    }

    if([apiName isEqualToString:API_GET_COUPONS]){
        [self processResponse_Coupons:[ReadData arrayFromDictionary:response forKey:RESPONSEKEY_DATA]];
    }

    if([apiName isEqualToString:API_REQUEST_BOOKING]){
        [self processResponse_RequestBooking: [ReadData dictionaryFromDictionary:response forKey:RESPONSEKEY_DATA]];
    }

    if([apiName isEqualToString:API_ZENOTI_AVAILABILITY]){
        [self processResponse_ZenotiAvailability:response];
    }

    if([apiName isEqualToString:API_SLOT_UPDATE]){
        [self processResponse_UpdateSlot:response];
    }
    
    if([apiName isEqualToString:API_ZENOTI_SLOT_UPDATE]){
        [self processResponse_UpdateZenotiSlot:response];
    }
    
    if([apiName isEqualToString:API_PAYMENT_SUMMARY]){
        [self processResponse_PaymentSummary:[ReadData dictionaryFromDictionary:response forKey:RESPONSEKEY_DATA]];
        self.responseId = [ReadData stringValueFromDictionary:response forKey:@"responseid"];
    }
    
    if([apiName isEqualToString:API_CREATE_ORDER]){
        [self processResponse_CreateOrder:[ReadData stringValueFromDictionary:response forKey:RESPONSEKEY_DATA]];
    }

    if([apiName isEqualToString:API_DETAIL_VIEW_ORDER]){
        [self processResponse_OrderDetail:[ReadData dictionaryFromDictionary:response forKey:RESPONSEKEY_DATA]];
    }
}

@end
