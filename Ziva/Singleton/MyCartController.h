//
//  MyCartController.h
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MyCartDelegate <NSObject>

- (void) onResponseError;

@optional

- (void) onAvailabilityGenerated ;

- (void) onUpdateZenotiSlot;

- (void) onUpdateSlot;

- (void) onCouponsListCompleted;

- (void) onStoreDetailCompleted;

- (void) onPaymentSummaryGenerated;

- (void) onRequestBookingGenerated;

- (void) onOrderGenerated;

- (void) onOrderDetailCompleted;

@end


@interface MyCartController : NSObject

@property (nonatomic,copy) NSString *cartType;

@property (nonatomic,copy) NSString *deliveryType;

@property (nonatomic,copy) NSString *requestId;

@property (nonatomic,copy) NSString *responseId;

@property (nonatomic,copy) NSString *storeId;

@property (nonatomic,copy) NSString *storeName;

@property (nonatomic,copy) NSString *storeAddress;

@property (nonatomic,assign) BOOL isZenotiEnabled;

@property (nonatomic,assign) BOOL isCommonServices;


@property (nonatomic,copy) NSString *latitude;

@property (nonatomic,copy) NSString *longitude;

@property (nonatomic,copy) NSString *bookDate;

@property (nonatomic,copy) NSString *bookTimeSlot;

@property (nonatomic,assign) BOOL isCashPaymentAllowed;

@property (nonatomic,copy) NSString *promoCode;

@property (nonatomic,assign) BOOL isCreditsApplied;

@property (nonatomic,assign) CGFloat payableAmount;

@property (nonatomic,copy) NSString *modeOfPayment;

- (void)addDelegate:(id<MyCartDelegate>)delegateObject ;
- (void)removeDelegate:(id<MyCartDelegate>)delegateObject ;

- (void) setLatitude:(NSString *) lat andLongitude : (NSString *) lng;

- (void) initForServicesWithDeliveryType : (NSString *) type;
- (void) addServices :(NSDictionary *) serviceItem;
- (void) lessServices :(NSDictionary *) serviceItem;

- (void) initForLooksWithDeliveryType : (NSString *) type;
- (void) addLooks :(NSDictionary *) lookItem;
- (void) lessLooks :(NSDictionary *) lookItem;

- (void) initForProducts;
- (void) addProducts :(NSDictionary *) productItem;
- (void) lessProducts :(NSDictionary *) productItem;

- (NSString *) display_SummaryInfo;
- (NSString *) display_CartTypeInfo;
- (NSString *) display_ItemsCount;
- (NSString *) display_TotalValue;

- (CGFloat) totalValue;

- (NSMutableArray *) cartItems;
- (void) makeEmpty;

- (NSInteger) getQuantityInCart : (NSString *) recordId;
- (BOOL) isAddedToCart : (NSString *) recordId;
- (void) removeFromCart : (NSString *) recordId;

- (NSDictionary *) fetchRequestBookingInfo;
- (NSArray *) fetchAvailibilitySlots;
- (BOOL) fetchAvailibilityStatus;
- (NSDictionary *) fetchPaymentSummaryInfo;
- (NSDictionary *) fetchOrderInfo;

- (NSArray *) fetchCoupons;
- (NSDictionary *) fetchStoreInfo : (NSString *) storeId;

- (void) updateStoreInformation : (NSDictionary *) infoDict ;

- (NSString *) getZivaBillURL ;

// APIs

- (void) getStores;

- (void) getCoupons;

- (void) generateRequestBookingId ;

- (void) getZenotiAvailability : (NSString *) storeId ;

- (void) updateSlot : (BOOL) isZenoti;

- (void) updateZenotiSlot;

- (void) getPaymentSummary;

- (void) generateOrderId;

- (void) getOrderInfo;

@end
