//
//  MakeHTMLString.h
//  Ziva
//
//  Created by  Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.

#import <Foundation/Foundation.h>

@interface MakeHTMLString : NSObject

+ (NSAttributedString *)getHTMLString : (NSString *) html FontFamily :(NSString *) family FontSize: (CGFloat) size;

+ (NSAttributedString *) getHTMLString : (NSString *) html ;

+ (NSAttributedString *) getHTMLString : (NSString *) html FontSize: (CGFloat) size;

+ (NSAttributedString *)getCenteredHTMLString : (NSString *) html FontFamily :(NSString *) family FontSize: (CGFloat) size;

@end
