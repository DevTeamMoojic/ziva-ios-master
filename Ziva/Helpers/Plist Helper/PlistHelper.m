//  PlistHelper.m
//  
//
//  Created by  Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.

#import "PlistHelper.h"

@interface PlistHelper ()
{
    NSString *plistFile;
    NSArray  *paths;
    NSString *documentsPath;
    NSString *plistPath;
    
    NSMutableDictionary *plistData;
}

@end

@implementation PlistHelper

- (id)init
{
    if (self = [super init])
    {
        paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsPath = [paths objectAtIndex:0];
    }
    return self;
}

#pragma mark - Mutable Plists
- (NSMutableDictionary *)getMutableDictionaryForPlist:(NSString *)plistName
{
    plistFile = [NSString stringWithFormat:@"%@.plist", plistName];
    plistPath = [documentsPath stringByAppendingPathComponent:plistFile];
    
    // check to see if plist exists in documents
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath])
    {
        // if not in documents, get property list from main bundle
        plistPath = [[NSBundle mainBundle] pathForResource:plistName ofType:@"plist"];
    }
    
    // read property list into memory as an NSData object
    NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
    NSError *error;
    NSPropertyListFormat format;
    // convert static property list into dictionary object
    plistData = (NSMutableDictionary *)[NSPropertyListSerialization propertyListWithData:plistXML
                                                                                 options:NSPropertyListMutableContainersAndLeaves
                                                                                  format:&format
                                                                                   error:&error];
    return [NSMutableDictionary dictionaryWithDictionary:plistData];
}

// Get DATA
- (NSMutableDictionary *)getData:(NSString *)plistName
{
    plistFile = [NSString stringWithFormat:@"%@.plist", plistName];
    plistPath = [documentsPath stringByAppendingPathComponent:plistFile];
    
    // check to see if plist exists in documents
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath])
    {
        // if not in documents, return nil data
        return nil;
    }
    
    // read property list into memory as an NSData object
    NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
    NSError *error;
    NSPropertyListFormat format;
    // convert static property list into dictionary object
    plistData = (NSMutableDictionary *)[NSPropertyListSerialization propertyListWithData:plistXML
                                                                                 options:NSPropertyListMutableContainersAndLeaves
                                                                                  format:&format
                                                                                   error:&error];
    return plistData;
}

- (NSMutableDictionary *)getDataFromFileNameWithExtension:(NSString *)fileName
{
    plistPath = [documentsPath stringByAppendingPathComponent:fileName];
    
    // check to see if plist exists in documents
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath])
    {
        // if not in documents, return nil data
        return nil;
    }
    
    // read property list into memory as an NSData object
    NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
    NSError *error;
    NSPropertyListFormat format;
    // convert static property list into dictionary object
    plistData = (NSMutableDictionary *)[NSPropertyListSerialization propertyListWithData:plistXML
                                                                                 options:NSPropertyListMutableContainersAndLeaves
                                                                                  format:&format
                                                                                   error:&error];
    return plistData;
}


- (NSMutableArray *)getFileList:(NSString *)filePattern
{
    NSMutableArray *fileList = [[NSMutableArray alloc] init];
    NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsPath error:Nil];
    for (NSString *fName in dirContents) {
        if ([fName hasPrefix:filePattern] ) {
            
            [fileList addObject:fName];
        }
    }
    
    return fileList;
}
// Generic Save
- (void)saveDictionary:(NSMutableDictionary *)plist
                      toPlist:(NSString *)plistName
{
    plistFile = [NSString stringWithFormat:@"%@.plist", plistName];
    plistPath = [documentsPath stringByAppendingPathComponent:plistFile];
    
    BOOL writeSuccess = [plist writeToFile:plistPath atomically:YES];
    if (!writeSuccess) {
        NSLog(@"%@",@"An error occurred while saving plist" );
    }
}

// Save DATA
- (void)saveData:(NSMutableDictionary *)data toPlist:(NSString *)plistName
{
    if (data)
    {
        plistFile = [NSString stringWithFormat:@"%@.plist", plistName];
        plistPath = [documentsPath stringByAppendingPathComponent:plistFile];
        
        [self cleanDictionary:data];
        
        // CODE - USE NSDATA
        NSError *error;
        NSData *nsdata = [NSPropertyListSerialization dataWithPropertyList:data
                                                                    format:NSPropertyListBinaryFormat_v1_0
                                                                   options:0
                                                                     error:&error];
        [nsdata writeToFile:plistPath options:0 error:&error];
        
//        BOOL writeSuccess = [data writeToFile:plistPath atomically:YES];
//        if (IS_DEBUG)
//            NSLog(@"%d", writeSuccess);
    }
}

#pragma mark - Immutable Plists
- (NSDictionary *)getDictionaryForPlist:(NSString *)plistName
{
    plistFile = [NSString stringWithFormat:@"%@.plist", plistName];
    plistPath = [documentsPath stringByAppendingPathComponent:plistFile];
    
    // check to see if plist exists in documents
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath])
    {
        // if not in documents, get property list from main bundle
        plistPath = [[NSBundle mainBundle] pathForResource:plistName ofType:@"plist"];
    }
    
    // read property list into memory as an NSData object
    NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
    if(!plistXML) return Nil;
    
    NSError *error;
    NSPropertyListFormat format;
    // convert static property list into dictionary object
    NSDictionary *plist = [NSPropertyListSerialization propertyListWithData:plistXML
                                                                    options:NSPropertyListImmutable
                                                                     format:&format
                                                                      error:&error];
    return [NSDictionary dictionaryWithDictionary:plist];
}

// Get API
- (NSDictionary *)getAPI:(NSString *)apiName
{
    NSDictionary *plist = [self getDictionaryForPlist:@"APIs"];
    return [plist objectForKey:apiName];
}

// Delete All Data in Documents
- (void)deleteAllData
{
    NSError *error;
    NSArray *allDataFiles = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsPath error:&error];
    
    for (NSString *dataFileName in allDataFiles)
        [self deleteFile:dataFileName];
}

- (void)deleteFile:(NSString *)fileName
{
    NSString *path = [documentsPath stringByAppendingPathComponent:fileName];
    
    NSError *error;
    if(![[NSFileManager defaultManager] removeItemAtPath:path error:&error])
    {
        //TODO: Handle/Log error
    }
}

- (void)deletePlist:(NSString *)plistName
{
    plistFile = [NSString stringWithFormat:@"%@.plist", plistName];
    plistPath = [documentsPath stringByAppendingPathComponent:plistFile];
    
    
    [[NSFileManager defaultManager] removeItemAtPath:plistPath error:NULL];
}

- (void)cleanDictionary:(NSMutableDictionary *)dictionary
{
    // Get all keys
    NSArray *keys = [dictionary allKeys];
    // Check each object
    for (id key in keys)
    {
        id obj = [dictionary objectForKey:key];
        if (obj == [NSNull null])
        {
            [dictionary setObject:@"" forKey:key];
            //NSLog(@"Removed Null");
        }
        else if ([obj isKindOfClass:[NSMutableDictionary class]])
            [self cleanDictionary:obj];
        else if ([obj isKindOfClass:[NSMutableArray class]])
            [self cleanArray:obj];
        else if ([obj isKindOfClass:[NSString class]])
        {
            [self cleanString:obj];
            [dictionary setObject:obj forKey:key];
        }
    }
}

- (void)cleanArray:(NSMutableArray *)array
{
    for (id obj in array)
    {
        if ([obj isKindOfClass:[NSMutableDictionary class]])
            [self cleanDictionary:obj];
    }
}

- (void)cleanString:(NSString *)string
{
    string = [string stringByReplacingOccurrencesOfString:@"&amp;" withString:@" & "];
    string = [string stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
    string = [string stringByReplacingOccurrencesOfString:@"&euro;" withString:@"€"];
}


- (void)saveImage:(UIImage *)image WithName:(NSString *)imageName
{
    // File Name and Path
    NSString *fileName = [NSString stringWithFormat:@"%@.png", imageName];
    NSString *imagePath = [documentsPath stringByAppendingPathComponent:fileName];
    // Data
    NSData *imageData = UIImagePNGRepresentation(image);
    // Store
    [imageData writeToFile:imagePath atomically:YES];
}

- (UIImage *)getImage:(NSString *)imageName
{
    UIImage *image = nil;
    NSString *fileName = [NSString stringWithFormat:@"%@.png", imageName];
    NSString *imagePath = [documentsPath stringByAppendingPathComponent:fileName];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath])
    {
        NSData *imageData = [NSData dataWithContentsOfFile:imagePath];
        image = [UIImage imageWithData:imageData];
    }
    
    return image;
}

@end
