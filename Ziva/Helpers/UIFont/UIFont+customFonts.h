//  UIFont+customFonts.h
//  Ziva
//
//  Created by  Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.

#import <UIKit/UIKit.h>

@interface UIFont (customFonts)

+ (UIFont *) regularFontOfSize: (CGFloat) size ;
+ (UIFont *) blackFontOfSize: (CGFloat) size ;
+ (UIFont *) boldFontOfSize: (CGFloat) size;
+ (UIFont *) hairlineFontOfSize: (CGFloat) size;
+ (UIFont *) lightFontOfSize: (CGFloat) size ;
+ (UIFont *) semiboldFontOfSize: (CGFloat) size;
+ (UIFont *) heavyFontOfSize: (CGFloat) size;
+ (UIFont *) mediumFontOfSize: (CGFloat) size ;

+ (UIFont *) italicFontOfSize: (CGFloat) size ;
+ (UIFont *) blackItalicFontOfSize: (CGFloat) size;
+ (UIFont *) boldItalicFontOfSize: (CGFloat) size;
+ (UIFont *) hairlineItalicFontOfSize: (CGFloat) size;
+ (UIFont *) lightItalicFontOfSize: (CGFloat) size;
+ (UIFont *) semiboldItalicFontOfSize: (CGFloat) size;
+ (UIFont *) heavyItalicFontOfSize: (CGFloat) size;
+ (UIFont *) mediumItalicFontOfSize: (CGFloat) size;

+ (UIFont *) getFontType : (NSString *) type andSize : (CGFloat) size;

@end
