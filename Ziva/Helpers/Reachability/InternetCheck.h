//
//  InternetCheck.h
//  Ziva
//
//  Created by  Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.

#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface InternetCheck : NSObject

+ (BOOL)isOnline;

@end
