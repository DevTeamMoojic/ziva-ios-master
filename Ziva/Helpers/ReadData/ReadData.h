//
//  ReadData.h
//  Ziva
//
//  Created by Bharat on 03/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReadData : NSObject

// Generic Helper Methods

+ (NSString *)stringValueFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName;

+ (NSInteger)integerValueFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName;

+ (CGFloat)floatValueFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName;

+ (BOOL)boolValueFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName;

+ (NSArray *)arrayFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName;

+ (NSDictionary *)dictionaryFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName;

+ (BOOL)IsnulldictionaryFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName;

+ (BOOL)IsnullarrayFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName;


// Helper Methods
+ (BOOL) isValidResponseData :(NSDictionary *)dict ;

+ (NSString *)recordIdFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName;

+ (NSString *)getGenderDescription:(NSString *) genderId;

+ (NSString *)genderIdFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName;

+ (NSString *)ratingsInPointsFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName;

+ (NSString *)amountInRsFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName;

+ (NSString *)cashbackInRsFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName;

+ (NSString *)distanceInKmFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName;

+ (NSString *)getAmountFormatted:(CGFloat) amount;

+ (NSString *)getCashbackFormatted:(CGFloat) amount;

@end
