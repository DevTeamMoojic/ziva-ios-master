//
//  ReadData.m
//  Ziva
//
//  Created by Bharat on 03/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "ReadData.h"

@implementation ReadData

#pragma mark - Generic Helper Methods


+ (NSString *)stringValueFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName
{
    NSString *val = @"";
    if([dict objectForKey:keyName]){
        if(![[dict objectForKey:keyName] isEqual:[NSNull null]]){
            if([[dict objectForKey:keyName] isKindOfClass:[NSString class]]){
                val = [dict objectForKey:keyName];
            }
            else{
            val = [[dict objectForKey:keyName] stringValue];
            }        
        }
    }
    return val;
}

+ (NSInteger)integerValueFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName
{
    NSInteger val = 0;
    if([dict objectForKey:keyName]){
        if(![[dict objectForKey:keyName] isEqual:[NSNull null]]){
            val = [[dict objectForKey:keyName] integerValue];
        }
    }
    return val;
}

+ (CGFloat)floatValueFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName
{
    CGFloat val = 0;
    if([dict objectForKey:keyName]){
        if(![[dict objectForKey:keyName] isEqual:[NSNull null]]){
            val = [[dict objectForKey:keyName] floatValue];
        }
    }
    return val;
}

+ (BOOL)boolValueFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName
{
    BOOL val = NO;
    if([dict objectForKey:keyName]){
        if(![[dict objectForKey:keyName] isEqual:[NSNull null]]){
            val = [[dict objectForKey:keyName] boolValue];
        }
    }
    return val;
}

+ (NSArray *)arrayFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName{
    NSArray *val ;
    BOOL isValid = NO;
    if([dict objectForKey:keyName]){
        if([[dict objectForKey:keyName] isKindOfClass:[NSArray class]])
        {
            if(![[dict objectForKey:keyName] isEqual:[NSNull null]]){
                isValid = YES;
                val =[dict objectForKey:keyName];
            }
        }
    }
    if(!isValid) val = [NSArray array];
    return val;
}

+ (NSDictionary *)dictionaryFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName{
    NSDictionary *val ;
    BOOL isValid = NO;
    if([dict objectForKey:keyName]){
        if([[dict objectForKey:keyName] isKindOfClass:[NSDictionary class]])
        {
            if(![[dict objectForKey:keyName] isEqual:[NSNull null]]){
                isValid = YES;
                val =[dict objectForKey:keyName];
            }
        }
    }
    if(!isValid) val = [NSDictionary dictionary];
    return val;
}

+ (BOOL)IsnulldictionaryFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName
{
    BOOL isNull = YES;
    if([dict objectForKey:keyName]){
        if([[dict objectForKey:keyName] isKindOfClass:[NSDictionary class]])
        {
            if(![[dict objectForKey:keyName] isEqual:[NSNull null]]){ isNull = NO;}
        }
    }
    
    return isNull;
}

+ (BOOL)IsnullarrayFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName
{
    BOOL isNull = YES;
    if([dict objectForKey:keyName]){
        if([[dict objectForKey:keyName] isKindOfClass:[NSArray class]])
        {
            if(![[dict objectForKey:keyName] isEqual:[NSNull null]]){ isNull = NO;}
        }
    }
    
    return isNull;
}

#pragma mark - Helper Methods

+ (BOOL) isValidResponseData :(NSDictionary *)dict
{
    NSString *isSuccess = [ReadData stringValueFromDictionary:dict forKey:RESPONSEKEY_STATUS];
    return ([isSuccess caseInsensitiveCompare:STATUS_SUCCESS] == NSOrderedSame);
}

+ (NSString *)recordIdFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName
{
    NSInteger val = 0;
    if([dict objectForKey:keyName]){
        if(![[dict objectForKey:keyName] isEqual:[NSNull null]]){
            val = [[dict objectForKey:keyName] integerValue];
        }
    }
    return [NSString stringWithFormat:@"%ld", (long)val];
}

+ (NSString *)getGenderDescription:(NSString *) genderId;
{
    NSString *val = @"";
    if([genderId isEqualToString:GENDER_MALE]){
        val = @"Male";
    }
    else if([genderId isEqualToString:GENDER_FEMALE]){
        val = @"Female";
    }
    else if([genderId isEqualToString:GENDER_UNISEX]){
        val = @"Unisex";
    }
    return val;
}

+ (NSString *)genderIdFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName
{
    NSInteger val = 0;
    if([dict objectForKey:keyName]){
        if(![[dict objectForKey:keyName] isEqual:[NSNull null]]){
            val = [[dict objectForKey:keyName] integerValue];
        }
    }
    return [NSString stringWithFormat:@"%ld", (long)val];
}

+ (NSString *)ratingsInPointsFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName
{
    CGFloat val = 0;
    if([dict objectForKey:keyName]){
        if(![[dict objectForKey:keyName] isEqual:[NSNull null]]){
            val = [[dict objectForKey:keyName] floatValue];
        }
    }
    return [NSString stringWithFormat:@"%.f", floor(val)];
}

+ (NSString *)amountInRsFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName
{
    CGFloat val = 0;
    if([dict objectForKey:keyName]){
        if(![[dict objectForKey:keyName] isEqual:[NSNull null]]){
            val = [[dict objectForKey:keyName] floatValue];
        }
    }
    return [self getAmountFormatted:val];
}

+ (NSString *)cashbackInRsFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName
{
    CGFloat val = 0;
    if([dict objectForKey:keyName]){
        if(![[dict objectForKey:keyName] isEqual:[NSNull null]]){
            val = [[dict objectForKey:keyName] floatValue];
        }
    }
    return [self getCashbackFormatted:val];
}

+ (NSString *)distanceInKmFromDictionary:(NSDictionary *)dict forKey : (NSString *) keyName
{
    CGFloat val = 0;
    if([dict objectForKey:keyName]){
        if(![[dict objectForKey:keyName] isEqual:[NSNull null]]){
            val = [[dict objectForKey:keyName] floatValue];
        }
    }
    return [NSString stringWithFormat:@"%.1f km", ceil(val *0.001)];
}

+ (NSString *)getAmountFormatted:(CGFloat) amount{
    if( amount == floorf(amount)){
        return [NSString stringWithFormat:@"₹ %.f/-", floorf(amount)];
    }
    else{
        return [NSString stringWithFormat:@"₹ %.02f/-", amount];
    }
}

+ (NSString *)getCashbackFormatted:(CGFloat) amount{
    if( amount == floorf(amount)){
        return [NSString stringWithFormat:@"₹ %.f cashback", floorf(amount)];
    }
    else{
        return [NSString stringWithFormat:@"₹ %.02f cashback", amount];
    }
}

@end
