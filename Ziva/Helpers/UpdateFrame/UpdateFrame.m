//
//  UpdateFrame.m
//  Ziva
//
//  Created by Bharat on 22/09/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "UpdateFrame.h"

@implementation UpdateFrame

+ (CGRect)setPositionForView:(UIView *) sourceView usingPosition : (CGPoint) transformedPoint{
    CGRect frame = sourceView.frame;
    frame.origin = transformedPoint;
    return frame;
}

+ (CGRect)setPositionForView:(UIView *) sourceView usingPositionX : (CGFloat) valueX andPositionY : (CGFloat) valueY{
    CGRect frame = sourceView.frame;
    frame.origin.x = valueX;
    frame.origin.y = valueY;
    return frame;
}

+ (CGRect)setPositionForView:(UIView *) sourceView usingPositionX : (CGFloat) valueX
{
    CGRect frame = sourceView.frame;
    frame.origin.x = valueX;
    return frame;
}

+ (CGRect)setPositionForView:(UIView *) sourceView usingPositionY : (CGFloat) valueY
{
    CGRect frame = sourceView.frame;
    frame.origin.y = valueY;
    return frame;
}

+ (CGRect)setPositionForView:(UIView *) sourceView usingOffsetX : (CGFloat) valueX andOffsetY : (CGFloat) valueY
{
    return CGRectOffset(sourceView.frame, valueX, valueY);
}

+ (CGRect)setPositionForView:(UIView *) sourceView usingOffsetX : (CGFloat) valueX
{
    return CGRectOffset(sourceView.frame, valueX, 0);
}

+ (CGRect)setPositionForView:(UIView *) sourceView usingOffsetY : (CGFloat) valueY
{
    return CGRectOffset(sourceView.frame, 0, valueY);
}


+ (CGRect)setSizeForView:(UIView *) sourceView usingSize : (CGSize) transformedSize{
    CGRect frame = sourceView.frame;
    frame.size = transformedSize;
    return frame;
}

+ (CGRect)setSizeForView:(UIView *) sourceView usingWidth : (CGFloat) transformedWidth andHeight : (CGFloat) transformedHeight
{
    CGRect frame = sourceView.frame;
    frame.size.height = transformedHeight;
    frame.size.width = transformedWidth;
    return frame;
}

+ (CGRect)setSizeForView:(UIView *) sourceView usingWidth : (CGFloat) transformedWidth {
    CGRect frame = sourceView.frame;
    frame.size.width = transformedWidth;
    return frame;
}

+ (CGRect)setSizeForView:(UIView *) sourceView usingHeight : (CGFloat) transformedHeight{
    CGRect frame = sourceView.frame;
    frame.size.height = transformedHeight;
    return frame;

}



@end
