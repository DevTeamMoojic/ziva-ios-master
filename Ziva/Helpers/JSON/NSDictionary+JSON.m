//  NSDictionary+JSON.m
//  JSON
//
//  Created by  Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.

#import "NSDictionary+JSON.h"

@implementation NSDictionary (JSON)

+ (NSDictionary* )dictionaryWithContentsOfJSONURLString:(NSString *)urlAddress
{
    NSData *data    = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlAddress]];
    __autoreleasing NSError* error  = nil;
    id result       = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    if (error != nil) return nil;
    
    return result;
}

- (BOOL)isConvertibleToJSON
{
    return [NSJSONSerialization isValidJSONObject:self];
}

- (NSData *)toJSON
{
    NSError* error = nil;
    id result = [NSJSONSerialization dataWithJSONObject:self options:kNilOptions error:&error];
    
    if (error != nil) return nil;
    
    return result;
}

@end
