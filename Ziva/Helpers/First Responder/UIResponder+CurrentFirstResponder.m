//  UIResponder+CurrentFirstResponder.m
//  
//
//  Created by  Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.

#import "UIResponder+CurrentFirstResponder.h"

static __weak id currentFirstResponder;

@implementation UIResponder (CurrentFirstResponder)

+ (id)currentFirstResponder {
    currentFirstResponder = nil;
    [[UIApplication sharedApplication] sendAction:@selector(findFirstResponder:) to:nil from:nil forEvent:nil];
    return currentFirstResponder;
}

- (void)findFirstResponder:(id)sender {
    currentFirstResponder = self;
}

+ (void)dismissKeyboard
{
    [[self currentFirstResponder] resignFirstResponder];
}

@end