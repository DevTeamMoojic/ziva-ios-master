//  UIResponder+CurrentFirstResponder.h
//  
//
//  Created by  Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.

#import <UIKit/UIKit.h>

@interface UIResponder (CurrentFirstResponder)

+ (id)currentFirstResponder;
+ (void)dismissKeyboard;

@end
