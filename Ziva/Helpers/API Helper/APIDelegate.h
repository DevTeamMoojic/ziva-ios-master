//  APIDelegate.h
//
//  Created by  Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.


#import <Foundation/Foundation.h>

@protocol APIDelegate <NSObject>

- (void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName;

@end
