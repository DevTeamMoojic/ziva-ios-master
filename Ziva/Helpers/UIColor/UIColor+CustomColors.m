//  UIColor+CustomColors.m
//  Ziva
//
//  Created by  Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.

#import "UIColor+CustomColors.h"

@implementation UIColor (CustomColors)

+ (UIColor *) brandPurpleDisplayColor
{
    return  [UIColor colorWithRed:69.0/255 green:78.0/255 blue:109.0/255 alpha:1];
}


+ (UIColor *) brandOrangeDisplayColor
{
    return  [UIColor colorWithRed:255.0/255 green:181.0/255 blue:16.0/255 alpha:1];
}

+ (UIColor *) greyBackgroundDisplayColor
{
    return  [UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1];
}

+ (UIColor *) greyTextDisplayColor
{
    return  [UIColor colorWithRed:197.0/255 green:197.0/255 blue:197.0/255 alpha:1];
}

+ (UIColor *) searchTextDisplayColor
{
    return  [UIColor colorWithRed:87.0/255 green:87.0/255 blue:87.0/255 alpha:1];
}

+ (UIColor *) borderDisplayColor
{
    return [UIColor colorWithRed:160.0/255 green:167.0/255 blue:192.0/255 alpha:1.0];
}

+ (UIColor *) lightborderDisplayColor
{
    return [UIColor colorWithRed:240.0/255 green:240.0/255 blue:245.0/255 alpha:1.0];
}

@end
