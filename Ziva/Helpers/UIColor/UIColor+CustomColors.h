//  UIColor+CustomColors.h
//  Ziva
//
//  Created by  Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.

#import <UIKit/UIKit.h>

@interface UIColor (CustomColors)

+ (UIColor *) brandPurpleDisplayColor;

+ (UIColor *) brandOrangeDisplayColor;

+ (UIColor *) greyBackgroundDisplayColor;

+ (UIColor *) greyTextDisplayColor;

+ (UIColor *) searchTextDisplayColor;

+ (UIColor *) borderDisplayColor;

+ (UIColor *) lightborderDisplayColor;

@end
