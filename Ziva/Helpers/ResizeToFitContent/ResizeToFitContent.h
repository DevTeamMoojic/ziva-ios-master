//
//  ResizeToFitContent.h
//  WowTables
//
//  Created by Bharat on 03/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ResizeToFitContent : NSObject

+ (CGFloat)getWidthForText:(NSString *) content usingFontType:(NSString *) fontType FontOfSize:(CGFloat) fontSize andMaxWidth:(CGFloat) maxWidth;

+ (CGFloat)getHeightForText:(NSString *) content usingFontType:(NSString *) fontType FontOfSize:(CGFloat) fontSize andMaxWidth:(CGFloat) maxWidth;

+ (CGFloat)getHeightForNLines : (NSInteger) noOfLines ForText:(NSString *) content usingFontType:(NSString *) fontType FontOfSize:(CGFloat) fontSize andMaxWidth:(CGFloat) maxWidth;


+ (CGFloat) getHeightForParallaxImage : (CGRect ) viewBounds;
+ (CGFloat) getHeightForDetailViewImage : (CGRect ) viewBounds;
@end
