//
//  PackageCategoryListViewController.h
//  Ziva
//
//  Created by Bharat on 24/09/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PackageCategoryListViewController : UIViewController

- (void) loadData: (NSArray *) listArray  withCellContentSize : (CGSize) contentsize;

- (CGFloat) getContentHeight;
- (void) scrollContent : (CGPoint) offset;
- (CGPoint) getScrollContent ;

@end
