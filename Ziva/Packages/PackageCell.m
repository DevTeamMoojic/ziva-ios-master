//
//  PackageCell.m
//  Ziva
//
//  Created by Bharat on 24/09/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "PackageCell.h"

@implementation PackageCell

- (void)awakeFromNib {
    // Initialization code
    
    [super awakeFromNib];
    
    // Fix : Autoresizing issues for iOS 7 using Xcode 6
    self.contentView.frame = self.bounds;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //    Fix : For right aligned view with autoresizing
    UIView *backV = [[self.contentView subviews] objectAtIndex:0];
    backV.frame = [UpdateFrame setSizeForView:backV usingSize:self.bounds.size];
}


- (void) configureDataForCell : (NSDictionary *) item
{
    self.mainImgV.image = Nil;
    self.mainImgCV.alpha = 0;
    self.placeholderImgV.alpha=1;
    
    self.titleL.text = [ReadData stringValueFromDictionary:item forKey:KEY_PACKAGE_TITLE];
    self.otherInfoL.text = [ReadData stringValueFromDictionary:item forKey:KEY_PACKAGE_SUBTITLE];
    
    NSString *startingPrice = [NSString stringWithFormat: @"Starting price from %@",[ReadData amountInRsFromDictionary:item forKey:KEY_PACKAGE_STARTINGPRCE]];
    
    CGFloat tmpWidth = [ResizeToFitContent getWidthForText:startingPrice usingFontType:FONT_SEMIBOLD FontOfSize:13 andMaxWidth:CGRectGetWidth(self.titleL.frame)];
    
    self.pricingCV.frame = [UpdateFrame setSizeForView:self.pricingCV usingWidth:(tmpWidth+10)];
    self.startingPriceL.text = startingPrice;
    
    // ImageView
    NSString *url = [ReadData stringValueFromDictionary:item forKey:KEY_IMAGE_URL];
    if (![url isEqualToString:@""])
    {
        NSURL *imageURL = [NSURL URLWithString:url];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:imageURL];
        [self.mainImgV setImageWithURLRequest:urlRequest
                             placeholderImage:Nil
                                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             [self.mainImgV setImage:image];
             [UIView animateWithDuration:IMAGE_VIEW_TOGGLE
                                   delay:0 options:UIViewAnimationOptionCurveLinear
                              animations:^(void)
              {
                  
                  [self.placeholderImgV setAlpha:0 ];
                  [self.mainImgCV setAlpha:1 ];
              }
                              completion:^(BOOL finished)
              {
              }];
             
         }
                                      failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
         {
             
         }];
    }
}

@end
