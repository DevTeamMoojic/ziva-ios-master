//
//  PackagesViewController.m
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//
#import "GenderSelectorViewController.h"
#import "PackageCategoryListViewController.h"
#import "PackagesViewController.h"

#define MARGINS 20
#define PAGER_HEIGHT 40

@interface PackagesViewController ()<APIDelegate,MBProgressHUDDelegate,GenderSelectorDelegate>
{
    MBProgressHUD *HUD;
    
    NSArray *categoriesArray;
    NSArray *packagesArray;

    
    NSMutableArray *list;
    NSMutableArray *viewcontrollers;
    
    __weak IBOutlet UIView *genderV;
    __weak IBOutlet UIImageView *genderImgV;
    __weak IBOutlet UILabel *genderL;
    
    __weak IBOutlet UIView *optionsV;    
    __weak IBOutlet UILabel *screenTitleL;
    
    __weak IBOutlet UIView *containerView;
    __weak IBOutlet UIView *topImageV;
    __weak IBOutlet UIImageView *mainImgV;
    
    __weak IBOutlet UIScrollView *scrollviewOnTop;
    __weak IBOutlet UIScrollView *tabscrollView;
    
    __weak IBOutlet UIScrollView *tabcontentscrollView;
    
    __weak IBOutlet UIView *tabSelectorV;
    
    __weak IBOutlet UIButton *dummyButton;
    
    __weak IBOutlet UIView *genderselectorCV;
    __weak GenderSelectorViewController *genderselectorVC;
    
    __weak IBOutlet UIView *backV;
    
    NSString *selectedGenderId;
    
    CGRect cachedImageViewSize;
    CGFloat yscrollOffset;
    
    BOOL isBackFromOtherView;
    BOOL isContentSizeUpdated;
    
    BOOL isUpdating;
    BOOL isUpdating_MenuOptions;
    
    NSUInteger pages;
    NSUInteger pageIndex;
    NSInteger pageWidth;
    NSInteger pageHeight;
    
    CGFloat offset_menuOptions_x;
    
    NSInteger apiCtr;
    
    AppDelegate *appD;
}
@end

@implementation PackagesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self setupLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!isBackFromOtherView)
        cachedImageViewSize = mainImgV.frame;
    else
        isBackFromOtherView = NO;
}

#pragma mark - Public Methods

- (void) loadPackages{
    if ([InternetCheck isOnline])
    {
        [self hudShowWithMessage:@"Loading"];
        apiCtr = 2;
        
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_PACKAGE_CAETEGORIES Param:nil];
        
        NSMutableArray *replacementArray = [NSMutableArray array];
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];        
        paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject:@"GENDER_ID" forKey:@"key"];
        [paramDict setObject:selectedGenderId forKey:@"value"];
        [replacementArray addObject:paramDict];
        
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_PACKAGES Param:nil replacementStrings:replacementArray];
    }
}

#pragma mark - Methods

- (CGFloat) calculateImageHeight
{
    CGFloat designScreenHeight = 667;
    CGFloat designHeight = 230;
    
    CGFloat translatedHeight = ceil(designHeight * CGRectGetHeight(self.view.frame)/designScreenHeight);
    
    return (int)translatedHeight;
}

- (CGSize) getCellContentSize
{
    CGSize cellSz = tabcontentscrollView.frame.size;
    cellSz.width-=(2*MARGINS);
    cellSz.height-=(PAGER_HEIGHT + MARGINS);
    return cellSz;
}

- (PackageCategoryListViewController *) getCategoryListVC :(NSInteger) index{
    PackageCategoryListViewController *obj  = (PackageCategoryListViewController *)[viewcontrollers objectAtIndex:index];
    return obj;
}

- (void) setupLayout
{
    list = [NSMutableArray array];
    viewcontrollers = [NSMutableArray array];
    
    selectedGenderId = [appD getLoggedInUserGender];
    genderL.text = [ReadData getGenderDescription:selectedGenderId];
    [genderselectorVC setGender:selectedGenderId];
    
    pageWidth = CGRectGetWidth(self.view.bounds);
    
    [self.view addGestureRecognizer:scrollviewOnTop.panGestureRecognizer];
    
    //Position elements
    {
        
        topImageV.frame = [UpdateFrame setSizeForView:topImageV usingHeight:[ResizeToFitContent getHeightForParallaxImage:self.view.frame]];
        
        yscrollOffset = CGRectGetHeight(topImageV.frame) - IMAGE_SCROLL_OFFSET_Y - CGRectGetHeight(optionsV.frame);
        mainImgV.frame = topImageV.frame ;
        
        containerView.frame = [UpdateFrame setSizeForView:containerView usingHeight:(CGRectGetHeight(self.view.bounds) + yscrollOffset)];
        
        tabscrollView.frame = [UpdateFrame setPositionForView:tabscrollView usingPositionY:CGRectGetMaxY(topImageV.frame)];
        
        tabcontentscrollView.frame= [UpdateFrame setPositionForView:tabcontentscrollView usingPositionY: CGRectGetMaxY(tabscrollView.frame)];
        tabcontentscrollView.frame = [UpdateFrame setSizeForView:tabcontentscrollView usingHeight: CGRectGetHeight(containerView.frame) - CGRectGetMaxY(tabscrollView.frame)];
    }
    
    CGPoint scrollviewOrigin = scrollviewOnTop.frame.origin;
    scrollviewOnTop.scrollIndicatorInsets = UIEdgeInsetsMake(-scrollviewOrigin.y, 0, scrollviewOrigin.y, scrollviewOrigin.x);
    
    [self loadPackages];
}

- (void) processResponse_Categories : (NSDictionary *)dict
{
    categoriesArray = [ReadData arrayFromDictionary:dict forKey:RESPONSEKEY_DATA];
}

- (void) processResponse_Packages : (NSDictionary *)dict
{
    packagesArray = [ReadData arrayFromDictionary:dict forKey:RESPONSEKEY_DATA];
}

- (void) generatePackagesByCategories{
    if([list count] > 0 ) [list removeAllObjects];
    for(NSDictionary *category in categoriesArray){
        NSString *categoryId = [ReadData recordIdFromDictionary:category forKey:KEY_ID];
        BOOL isFound = NO;
        NSMutableArray *tmpArray = [NSMutableArray array];
        for(NSDictionary *package in packagesArray){
            if([[ReadData recordIdFromDictionary:package forKey:KEY_PACKAGE_CATEGORY_ID] isEqualToString:categoryId]){
                isFound = YES;
                [tmpArray addObject:package];
            }
        }
        if(isFound){
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setObject:categoryId forKey:KEY_ID];
            [dict setObject:[ReadData stringValueFromDictionary:category forKey:KEY_NAME] forKey:KEY_NAME];
            [dict setObject:tmpArray forKey:KEY_LIST_ARRAY];
            [list addObject:dict];
        }
    }
    [self loadsubmenusScrollView];
}

#pragma mark - Setup Tab Containers

- (NSInteger) calculateWidthForSubMenu : (NSString *) title
{
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, CGRectGetHeight(tabscrollView.bounds))];
    lbl.font = [UIFont semiboldFontOfSize:13];
    lbl.text = title;
    [lbl sizeToFit];
    return (CGRectGetWidth(lbl.frame) + 50);
}

-(void)loadsubmenusScrollView
{
    for (UIView* v in tabscrollView.subviews)
    {
        if(v.tag >= TAG_START_INDEX)
            [v removeFromSuperview];
    }
    
    CGFloat offsetX = 0;
    NSInteger ctr = TAG_START_INDEX;
    pages = [list count];
    for(NSDictionary *item in list)
    {
        NSData *tempArchive = [NSKeyedArchiver archivedDataWithRootObject:dummyButton];
        UIButton *categoryB = (UIButton *)[NSKeyedUnarchiver unarchiveObjectWithData:tempArchive];
        NSString *heading = [ReadData stringValueFromDictionary:item forKey:KEY_NAME];
        [categoryB setTitle:heading forState:UIControlStateNormal];
        categoryB.tag = ctr;
        categoryB.hidden = NO;
        [categoryB addTarget:self action:@selector(optionBPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        categoryB.frame = [UpdateFrame setPositionForView:categoryB usingPositionX:offsetX andPositionY:0];
        categoryB.frame = [UpdateFrame setSizeForView:categoryB usingWidth:[self calculateWidthForSubMenu:heading]];
        
        offsetX+=CGRectGetWidth(categoryB.frame);
        ctr++;
        
        [tabscrollView addSubview:categoryB];
    }
    tabSelectorV.hidden = (pages == 0);
    [tabscrollView bringSubviewToFront:tabSelectorV];
    [tabscrollView setContentSize:CGSizeMake(offsetX, CGRectGetHeight(tabscrollView.frame))];
    [tabscrollView setContentOffset:CGPointZero];
    
    offset_menuOptions_x = tabscrollView.contentSize.width - CGRectGetWidth(self.view.bounds);
    if (offset_menuOptions_x > 0) {
        tabscrollView.scrollEnabled = YES;
    }
    else{
        tabscrollView.scrollEnabled = NO;
        offset_menuOptions_x = 0;
    }
    
    [self loadcontentScrollView];
    [HUD hide:YES];
}

- (void) loadcontentScrollView
{
    if([viewcontrollers count] > 0)[viewcontrollers removeAllObjects];
    
    [tabcontentscrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    //Submenus tab content scroll view
    isUpdating = YES;
    pageWidth = CGRectGetWidth(tabcontentscrollView.frame);
    tabcontentscrollView.contentSize =
    CGSizeMake(pageWidth * pages , CGRectGetHeight(tabcontentscrollView.frame));
    [tabcontentscrollView setContentOffset:CGPointZero animated:NO];
    
    
    UIViewController *controller;
    for (NSInteger ctr = 0; ctr < pages; ctr++)
    {
        controller = [self.storyboard instantiateViewControllerWithIdentifier:@"packagecategorylistingVC"];
        
        // add the controller's view to the scroll view
        if (controller.view.superview == nil)
        {
            controller.view.frame = [UpdateFrame setPositionForView:tabcontentscrollView usingPositionX:pageWidth * ctr andPositionY:0];
            
            [self addChildViewController:controller];
            [tabcontentscrollView addSubview:controller.view];
            [controller didMoveToParentViewController:self];
        }
        
        [viewcontrollers addObject:controller];
        
        NSDictionary *item =  list[ctr];
        [[self getCategoryListVC:ctr] loadData:[item objectForKey:KEY_LIST_ARRAY]  withCellContentSize:[self getCellContentSize]];
    }
    isUpdating = NO;
    pageIndex = 0;
    if(pages > 0){
        [self tabChanged:pageIndex];
    }
}

- (void) tabChanged:(NSInteger)index
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize : index];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        CGPoint offset = [self getScrollContentOffset:index];
        offset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:offset animated:NO];
    }
    isContentSizeUpdated = NO;
    
    [self setSelectedOption:index];
}

-(void)setSelectedOption:(NSInteger)index
{
    UIButton *button = (UIButton *)[tabscrollView viewWithTag:TAG_START_INDEX+index];
    CGFloat sliderPosition = CGRectGetMinX(button.frame);
    CGFloat width = CGRectGetWidth(button.frame);
    
    
    [UIView animateWithDuration:TIME_VIEW_TOGGLE
                          delay:0 options:UIViewAnimationOptionCurveLinear
                     animations:^(void)
     {
         
         tabSelectorV.frame = [UpdateFrame setPositionForView:tabSelectorV usingPositionX:sliderPosition];
         tabSelectorV.frame = [UpdateFrame setSizeForView:tabSelectorV usingWidth:width];
         
     }
                     completion:^(BOOL finished)
     {
         for (int i = 0; i < pages; i++)
         {
             UIButton *btn = (UIButton *)[tabscrollView viewWithTag:i + TAG_START_INDEX];
             if (i != index  )
             {
                 if (btn.selected) btn.selected = NO;
             }
             else
                 btn.selected = YES;
         }
     }];
}

#pragma mark - Translate View

- (void)translateView:(UIView *)view andGreyView:(BOOL)showGreyView
{
    // Shrink View
    view.frame = [UpdateFrame setSizeForView:view usingHeight:0];
    
    // Ready Grey View
    if (showGreyView)
    {
        backV.alpha = 0;
        [self.view bringSubviewToFront:backV];
    }
    
    // Ready View
    view.alpha = 0;
    [self.view bringSubviewToFront:view];
    
    // DONT CHANGE THE ORDER
    
    // 1 - Dismiss keyboard
    [UIResponder dismissKeyboard];
    
    // 2 - Disable User Interaction
    [self.view setUserInteractionEnabled:NO];
    
    // 3 - Animate
    [UIView animateWithDuration:TIME_VIEW_TOGGLE
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         // Animate Grow and Fade In
         view.frame = [UpdateFrame setSizeForView:view usingHeight:TRANSFORMED_HEIGHT];
         view.alpha = 1;
         
         if (showGreyView)
             backV.alpha = 1;
     }
                     completion:^(BOOL finished)
     {
         // Enable User Interaction
         [self.view setUserInteractionEnabled:YES];
     }];
}

- (void)resetTranslateView:(UIView *)view andGreyView:(BOOL)hideGreyView
{
    // DONT CHANGE THE ORDER
    
    // 1 - Dismiss keyboard
    [UIResponder dismissKeyboard];
    
    
    // 2 - Disable User Interaction
    [self.view setUserInteractionEnabled:NO];
    
    // 3 - Animate
    [UIView animateWithDuration:TIME_VIEW_TOGGLE
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         // Animate Shrink & Fade Out
         view.frame = [UpdateFrame setSizeForView:view usingHeight:0];;
         view.alpha = 0;
         
         if(hideGreyView)
             backV.alpha = 0;
     }
                     completion:^(BOOL finished)
     {
         
         // Send Views to Back
         [self.view sendSubviewToBack:view];
         
         if(hideGreyView)
             [self.view sendSubviewToBack:backV];
         
         // Enable User Interaction
         [self.view setUserInteractionEnabled:YES];
     }];
}


#pragma mark - Gender Selector & Delegate Methods

- (void) valueChanged{
    selectedGenderId = [genderselectorVC getGender];
    genderL.text = [ReadData getGenderDescription:selectedGenderId];
    [self resetTranslateView:genderselectorCV andGreyView:YES];
    [self loadPackages];
}

#pragma mark - HUD & Delegate methods

- (void)hudShowWithMessage:(NSString *)message
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    HUD.dimBackground = YES;
    HUD.labelText = message;
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
    
    [HUD show:YES];
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
    HUD = nil;
}

#pragma mark - Expand on Scroll animation

- (CGSize) setContentSize :(NSInteger)index
{
    CGFloat height = CGRectGetHeight(mainImgV.frame) + CGRectGetHeight(tabscrollView.frame) ;
    height += [[self getCategoryListVC:index] getContentHeight];
    return CGSizeMake(pageWidth, height);
}

- (void) setScrollContentOffset: (CGPoint) offset forPage:(NSInteger)index
{
    [[self getCategoryListVC:index] scrollContent:offset];
}

- (CGPoint) getScrollContentOffset :(NSInteger)index
{
    CGPoint offset;
    [[self getCategoryListVC:index] getScrollContent];
    return offset;
}

- (void) resetOffsetAllTabs
{
    CGPoint offset = CGPointZero;
    for(NSInteger ctr = 0; ctr < [viewcontrollers count]; ctr++){
        [[self getCategoryListVC:ctr] scrollContent:offset];
    }
}

-(void) updateContentSize : (CGPoint) contentOffset;
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize : pageIndex];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        contentOffset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    }
    [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    
    isContentSizeUpdated = NO;
}

#pragma mark - Scroll Events


- (void) repositionMenuOptionsAfterScrolling
{
    NSUInteger page = 0;
    if (tabscrollView.contentOffset.x >= offset_menuOptions_x) {
        page = pages - 1 ;
    }
    else{
        page = 0;
    }
    
    UIButton *btn = (UIButton *)[tabscrollView viewWithTag:TAG_START_INDEX+page];
    [self optionBPressed:btn];
}

- (void) updateMenuOptionsScroll
{
    if (tabscrollView.scrollEnabled) {
        UIButton *btn = (UIButton *)[tabscrollView viewWithTag:TAG_START_INDEX+pageIndex];
        isUpdating_MenuOptions = YES;
        if (btn.frame.origin.x > offset_menuOptions_x) {
            [tabscrollView setContentOffset:CGPointMake(offset_menuOptions_x, 0) animated:YES];
        }
        else{
            [tabscrollView setContentOffset:CGPointZero animated:YES];
        }
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == scrollviewOnTop)
    {
        {
            if (isContentSizeUpdated)  return;
            
            CGRect scrolledBoundsForContainerView = containerView.bounds;
            if (scrollView.contentOffset.y <= yscrollOffset)
            {
                
                scrolledBoundsForContainerView.origin.y = scrollView.contentOffset.y ;
                containerView.bounds = scrolledBoundsForContainerView;
                
                //Reset offset for all tabs
                if (scrollView.contentOffset.y <= 0) [self resetOffsetAllTabs];
                
                
                CGFloat y = -scrollView.contentOffset.y;
                CGFloat alphaLevel = 1;
                CGFloat BLUR_MAX_Y = IMAGE_SCROLL_OFFSET_Y + CGRectGetHeight(topImageV.frame) - CGRectGetMinY(optionsV.frame);
                if (fabs(y) < BLUR_MAX_Y)
                {
                    alphaLevel = (BLUR_MAX_Y - fabs(y))/(BLUR_MAX_Y);
                }
                else
                {
                    alphaLevel = 0;
                }
                
                //[screenTitleL setAlpha:alphaLevel];
                if (y > 0)
                {
                    mainImgV.frame = CGRectInset(cachedImageViewSize, 0, -y/2);
                    mainImgV.center = CGPointMake(mainImgV.center.x, mainImgV.center.y + y/2);
                }
                else
                {
                    mainImgV.frame = [UpdateFrame setPositionForView:mainImgV usingPositionY:y];
                }
                return;
            }
            
            scrolledBoundsForContainerView.origin.y = yscrollOffset ;
            containerView.bounds = scrolledBoundsForContainerView;
            
            [self setScrollContentOffset:CGPointMake(0, scrollView.contentOffset.y - yscrollOffset) forPage:pageIndex ];
        }
    }
    else if(scrollView == tabcontentscrollView)
    {
        if (isUpdating) return;
        NSUInteger page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        if (pageIndex != page)
        {
            pageIndex = page;
            [self tabChanged:pageIndex];
            [self updateMenuOptionsScroll];
        }
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if(scrollView == tabcontentscrollView)
    {
        if (isUpdating) isUpdating = NO;
    }
    else if(scrollView == tabscrollView)
    {
        if (isUpdating_MenuOptions) isUpdating_MenuOptions = NO;
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate) {
        if(scrollView == tabscrollView)
        {
            [self repositionMenuOptionsAfterScrolling];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(scrollView == tabscrollView)
    {
        [self repositionMenuOptionsAfterScrolling];
    }
}

#pragma mark - Events

- (IBAction)backBPressed:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)showGenderListBPressed:(UIButton *)sender{
    [self translateView:genderselectorCV andGreyView:YES];
}

- (IBAction)dismissListBPressed:(UIButton *)sender{
    [self resetTranslateView:genderselectorCV andGreyView:YES];
}

- (IBAction)optionBPressed: (UIButton *)sender
{
    NSUInteger page = sender.tag - TAG_START_INDEX;
    if (pageIndex != page)
    {
        isUpdating = YES;
        pageIndex = page;
        [tabcontentscrollView setContentOffset:CGPointMake(pageIndex*pageWidth, 0) animated:YES];
        [self tabChanged:pageIndex];
    }
    [self updateMenuOptionsScroll];
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"showgenderselector_packages"]){
        genderselectorVC = (GenderSelectorViewController *)segue.destinationViewController;
        genderselectorVC.delegate = self;
    }
}

#pragma mark - API

-(void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName
{
    apiCtr --;
    if(![ReadData isValidResponseData:response])
    {
        {
            NSString *message = [ReadData stringValueFromDictionary:response forKey:RESPONSEKEY_ERRORMESSAGE];
            [appD showAPIErrorWithMessage:message andTitle:ALERT_TITLE_ERROR];
            [self.view setUserInteractionEnabled:YES];
            if(apiCtr <= 0) [HUD hide:YES];
        }
        return;
    }
    
    if ([apiName isEqualToString:API_GET_PACKAGE_CAETEGORIES]) {
        [self processResponse_Categories:response];
    }
    if ([apiName isEqualToString:API_GET_PACKAGES]) {
        [self processResponse_Packages: response];
    }
    if(apiCtr <= 0){
        [self generatePackagesByCategories];
    }
}

@end
